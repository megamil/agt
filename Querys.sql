use megamil_agpt;

CREATE TABLE usuarios (   

id_usuario int NOT NULL auto_increment,
usuario character varying(20) NOT NULL,
email character varying(40),
senha character varying(32) NOT NULL,
telefone character varying(11),
nome character varying(40),
ativo boolean,
  CONSTRAINT primary_usuarios_key PRIMARY KEY (id_usuario),
  CONSTRAINT unique_email_key UNIQUE (email),
  CONSTRAINT unique_nome_key UNIQUE (usuario)

);

Create table grupos (  

id_grupo int NOT NULL auto_increment,
nome_grupo character varying(15) NOT NULL,
descricao_grupo text,
  CONSTRAINT primary_grupos_key PRIMARY KEY (id_grupo),
  CONSTRAINT unique_nome_grupo_key UNIQUE (nome_grupo)

);

Create table aplicacoes (  

id_aplicacao int NOT NULL auto_increment,
aplicacao character varying(30) NOT NULL,
titulo_aplicacao character varying(50),
descricao_aplicacao character varying(500),
  CONSTRAINT primary_aplicacoes_key PRIMARY KEY (id_aplicacao),
  CONSTRAINT unique_nome_aplicacao_key UNIQUE (aplicacao)

);

CREATE TABLE grupo_aplicacoes (   

id_grupo_aplicacao int NOT NULL auto_increment,
fk_grupo int,
fk_aplicacao int,

  CONSTRAINT primary_aplicacoes_pkey PRIMARY KEY (id_grupo_aplicacao),
  CONSTRAINT grupo_aplicacoes_fk_aplicacao_fkey FOREIGN KEY (fk_aplicacao)
      REFERENCES aplicacoes (id_aplicacao) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT grupo_aplicacoes_fk_grupo_fkey FOREIGN KEY (fk_grupo)
      REFERENCES grupos (id_grupo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE TABLE grupo_usuarios (   

id_grupo_usuarios int NOT NULL auto_increment,
fk_grupo int,
fk_usuario int,
  CONSTRAINT primary_usuarios_pkey PRIMARY KEY (id_grupo_usuarios),

  CONSTRAINT grupo_usuarios_fk_grupo_fkey FOREIGN KEY (fk_grupo)
      REFERENCES grupos (id_grupo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT grupo_usuarios_fk_usuario_fkey FOREIGN KEY (fk_usuario)
      REFERENCES usuarios (id_usuario) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
/*=========================================*/
/*Para inserir um usuário use: (Esta senha È 123 em padão MD5).*/
Insert into usuarios (id_usuario,usuario,email,senha,nome,telefone,ativo) values (1,'Administrador','email@admin.com','202cb962ac59075b964b07152d234b70','Usuario administrativo','11962782329',TRUE);
Insert into usuarios (id_usuario,usuario,email,senha,nome,telefone,ativo) values (2,'delivery','email3@admin.com','202cb962ac59075b964b07152d234b70','Usuario delivery','11962782329',TRUE);

/*Criando Grupo Administrador:*/
Insert into grupos (nome_grupo,descricao_grupo) values ('Administrador', 'Acesso Total');
Insert into grupos (nome_grupo,descricao_grupo) values ('Promotores', 'Acesso limitado para promotores');
Insert into grupos (nome_grupo,descricao_grupo) values ('Delivery', 'Acesso limitado para delivery');

/*Inserindo as aplicações iniciais:*/
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (1,'view_proprios_usuarios', 'Editar conta' ,'Edição de usuário.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (2,'view_listar_grupos', 'Grupos' ,'Exibir grupos existentes.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (3,'view_novos_usuarios', 'Novo Usuário' , 'Criar novo usuário.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (4,'view_listar_usuarios', 'Usuários' ,'Exibir usuários existentes.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (5,'view_editar_usuarios', 'Editar Usuários' ,'Alterar dados dos usuários e atribuir grupos.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (6,'view_editar_grupos', 'Editar Grupo' , 'Alterar dados dos grupos e atribuir aplicações a eles.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (7,'view_novos_grupos', 'Novo Grupo' ,'Criar novo grupo.');
/*Dando permissão de acesso ao grupo Administrador:*/

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,1);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (3,1);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,2);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,3);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,4);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,5);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,6);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,7);

Insert into grupo_usuarios(fk_grupo, fk_usuario) values (1,1);
Insert into grupo_usuarios(fk_grupo, fk_usuario) values (3,2);
/*FIM DOS SQL*/


############################################################
####################### SQL DO SISTEMA #####################
############################################################

/*Produtos*/
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (8,'view_listar_produtos', 'Listar Produtos' ,'Listar Produtos.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (9,'view_novos_produtos', 'Novo Produto' ,'Cadastrar Produto.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (10,'view_editar_produtos', 'Editar Produto' ,'Editar Produto.');

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,8);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,9);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,10);

/*Clientes*/
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (11,'view_listar_clientes', 'Listar Clientes' ,'Listar Clientes.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (12,'view_novos_clientes', 'Novo Cliente' ,'Cadastrar Cliente.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (13,'view_editar_clientes', 'Editar Cliente' ,'Editar Cliente.');

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,11);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,12);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,13);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (2,11);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (2,12);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (2,13);

/*Pedidos*/
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (14,'view_listar_pedidos', 'Listar Pedidos' ,'Listar Pedidos.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (15,'view_filtro_pedidos', 'Filtrar Clientes' ,'Filtrar Clientes.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (16,'view_novos_pedidos', 'Novo Pedido' ,'Cadastrar Pedido.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (17,'view_editar_pedidos', 'Editar Pedido' ,'Editar Pedido.');

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,14);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (3,14);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,15);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,16);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,17);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (3,17);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (2,17);

/*Entregador*/
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (18,'view_listar_entregadores', 'Listar Entregadores' ,'Listar Entregadores.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (19,'view_novos_entregadores', 'Novo Entregador' ,'Cadastrar Entregador.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (20,'view_editar_entregadores', 'Editar Entregador' ,'Editar Entregador.');

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,18);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,19);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,20);

/*Promotores*/
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (21,'view_listar_promotores', 'Listar Promotores' ,'Listar Promotores.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (22,'view_novos_promotores', 'Novo Promotor' ,'Cadastrar Promotor.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (23,'view_editar_promotores', 'Editar Promotor' ,'Editar Promotor.');

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,21);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,22);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,23);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (2,21);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (2,22);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (2,23);


Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (24,'view_master_relatorios', 'Todos relatórios' ,'Todos relatórios, do master');

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (25,'view_pedidospg_relatorios', 'Todos Pedidos de pagamento' ,'Todos Pedidos de pagamento');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (26,'view_vendas_relatorios', 'Todas vendas' ,'Detalhes de Todas vendas');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (27,'view_trocascomissao_relatorios', 'Todas trocas de comissão' ,'Detalhes de Todas as trocas de comissão.');


Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,24);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,25);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,26);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,27);

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (40,'view_promotor_relatorios', 'Todos relatórios' ,'Todos relatórios, do promotor');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (41,'view_ganhos_p_relatorios', 'Ganhos por vendas' ,'Todos relatórios, dos ganhos por vendas');


Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (2,40);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (2,41);


Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (50,'view_saque_pagamentos', 'Lista e solicita saque' ,'CRUD para saques');

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (2,50);

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (51,'view_listar_pedidos_cliente', 'Lista pedidos por cliente' ,'Lista pedidos por cliente');
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,51);

create table produtos (

  id_produto int NOT NULL auto_increment,
  nome_produto character varying(15) NOT NULL,
  preco_produto double,
  quantidade_produto int,
  codigo_produto character varying(15) NOT NULL,
  url_produto character varying(150) NOT NULL,
  descricao_produto character varying(50),

  CONSTRAINT primary_id_produto PRIMARY KEY (id_produto)

);

create table estados (

  id_estado int NOT NULL auto_increment,
  codigo_ibge int,
  sigla character varying(2),
  nome character varying(30),

  CONSTRAINT primary_id_estado PRIMARY KEY (id_estado)

);

 Insert Into estados (codigo_ibge,sigla,nome) Values(12,'AC','Acre');  
 Insert Into estados (codigo_ibge,sigla,nome) Values(27,'AL','Alagoas');  
 Insert Into estados (codigo_ibge,sigla,nome) Values(13,'AM','Amazonas');
 Insert Into estados (codigo_ibge,sigla,nome) Values(16,'AP','Amapá');
 Insert Into estados (codigo_ibge,sigla,nome) Values(29,'BA','Bahia');
 Insert Into estados (codigo_ibge,sigla,nome) Values(23,'CE','Ceará');
 Insert Into estados (codigo_ibge,sigla,nome) Values(53,'DF','Distrito Federal');
 Insert Into estados (codigo_ibge,sigla,nome) Values(32,'ES','Espírito Santo');
 Insert Into estados (codigo_ibge,sigla,nome) Values(52,'GO','Goiás');
 Insert Into estados (codigo_ibge,sigla,nome) Values(21,'MA','Maranhão');
 Insert Into estados (codigo_ibge,sigla,nome) Values(31,'MG','Minas Gerais');
 Insert Into estados (codigo_ibge,sigla,nome) Values(50,'MS','Mato Grosso do Sul');
 Insert Into estados (codigo_ibge,sigla,nome) Values(51,'MT','Mato Grosso');
 Insert Into estados (codigo_ibge,sigla,nome) Values(15,'PA','Pará');
 Insert Into estados (codigo_ibge,sigla,nome) Values(25,'PB','Paraíba');
 Insert Into estados (codigo_ibge,sigla,nome) Values(26,'PE','Pernambuco');
 Insert Into estados (codigo_ibge,sigla,nome) Values(22,'PI','Piauí');
 Insert Into estados (codigo_ibge,sigla,nome) Values(41,'PR','Paraná');
 Insert Into estados (codigo_ibge,sigla,nome) Values(33,'RJ','Rio de Janeiro');
 Insert Into estados (codigo_ibge,sigla,nome) Values(24,'RN','Rio Grande do Norte');
 Insert Into estados (codigo_ibge,sigla,nome) Values(11,'RO','Rondônia');
 Insert Into estados (codigo_ibge,sigla,nome) Values(14,'RR','Roraima');
 Insert Into estados (codigo_ibge,sigla,nome) Values(43,'RS','Rio Grande do Sul');
 Insert Into estados (codigo_ibge,sigla,nome) Values(42,'SC','Santa Catarina');
 Insert Into estados (codigo_ibge,sigla,nome) Values(28,'SE','Sergipe');
 Insert Into estados (codigo_ibge,sigla,nome) Values(35,'SP','São Paulo');
 Insert Into estados (codigo_ibge,sigla,nome) Values(17,'TO','Tocantins');

create table status (

  id_status int NOT NULL auto_increment,
  nome_status character varying(50),

  CONSTRAINT primary_id_status PRIMARY KEY (id_status)

);

insert into status (id_status,nome_status) values (1,'Ativo');
insert into status (id_status,nome_status) values (2,'Inativo');
insert into status (id_status,nome_status) values (3,'Aguardando');
insert into status (id_status,nome_status) values (4,'Cancelado');
insert into status (id_status,nome_status) values (5,'Concluído');


create table bancos (

  id_banco int NOT NULL auto_increment,
  banco character varying(50),

  CONSTRAINT primary_id_banco PRIMARY KEY (id_banco)

);

insert into bancos (id_banco,banco) values (1,'CAIXA ECONÔMICA FEDERAL');
insert into bancos (id_banco,banco) values (2,'BANCO DO BRASIL');
insert into bancos (id_banco,banco) values (3,'BRADESCO');
insert into bancos (id_banco,banco) values (4,'ITAÚ');
insert into bancos (id_banco,banco) values (5,'SANTANDER');
insert into bancos (id_banco,banco) values (6,'MERCANTIL DO BRASIL');

create table promotores (

  id_promotor int NOT NULL auto_increment,
  nome_promotor character varying(30),
  cpf_promotor character varying(11),
  rg_promotor character varying(11),
  tel_promotor character varying(10),
  cel_promotor character varying(11),
  fk_promotor int,
  status_promotor int,
  login_promotor character varying(50),
  senha_promotor character varying(50),
  email_promotor character varying(100),
  data_cadastro_promotor timestamp DEFAULT CURRENT_TIMESTAMP,

  rua_promotor character varying(40),
  numero_promotor character varying(20),
  cep_promotor character varying(8),
  bairro_promotor character varying(30),
  cidade_promotor character varying(30),
  estado_promotor int,
  complemento_promotor character varying(100),

  fk_banco int,
  agencia_promotor character varying(10),
  conta_promotor character varying(15),

  obs_promotor character varying(100),


  CONSTRAINT primary_id_promotor PRIMARY KEY (id_promotor),
  
  CONSTRAINT fk_estado_promotor FOREIGN KEY (estado_promotor)
      REFERENCES estados (id_estado),

  CONSTRAINT fk_banco_promotor FOREIGN KEY (fk_banco)
      REFERENCES bancos (id_banco),

  CONSTRAINT fk_status_promotor FOREIGN KEY (status_promotor)
      REFERENCES status (id_status)

);

create table clientes (

  id_cliente int NOT NULL auto_increment,
  nome_cliente character varying(30),
  cpf_cliente character varying(11),
  rg_cliente character varying(11),
  tel_cliente character varying(10),
  cel_cliente character varying(11),
  fk_promotor_cliente int,
  data_cadastro_cliente timestamp DEFAULT CURRENT_TIMESTAMP,

  data_nascimento_cliente date,
  tel2_cliente character varying(11),
  tel3_cliente character varying(11),

  rua_cliente character varying(40),
  numero_cliente character varying(20),
  cep_cliente character varying(8),
  bairro_cliente character varying(30),
  cidade_cliente character varying(30),
  estado_cliente int,
  complemento_cliente character varying(100),

  obs_cliente character varying(100),


  CONSTRAINT primary_id_cliente PRIMARY KEY (id_cliente),
  CONSTRAINT fk_estado_cliente FOREIGN KEY (estado_cliente)
      REFERENCES estados (id_estado),
  CONSTRAINT fk_promotor_c FOREIGN KEY (fk_promotor_cliente)
      REFERENCES promotores (id_promotor)

);

create table entregador(

  id_entregador int NOT NULL auto_increment,
  nome_entregador character varying(40),
  status_entregador int,

  CONSTRAINT primary_id_entregador PRIMARY KEY (id_entregador),

  CONSTRAINT fk_status_entregador FOREIGN KEY (status_entregador)
      REFERENCES status (id_status)

);

create table pedidos(

  id_pedido int NOT NULL auto_increment,
  fk_cliente int,
  fk_usuario_criou int, 
  fk_usuario_atendeu int,
  fk_usuario_entregador int,
  taxa_entrega double,
  data_pedido timestamp DEFAULT CURRENT_TIMESTAMP,
  data_atendimento timestamp,
  status_pedido int,

  CONSTRAINT primary_id_pedido PRIMARY KEY (id_pedido),

  CONSTRAINT fk_status FOREIGN KEY (status_pedido)
      REFERENCES status (id_status),

  CONSTRAINT fk_cliente_pedido FOREIGN KEY (fk_cliente)
      REFERENCES clientes (id_cliente),

  CONSTRAINT fk_usuario_criou_pedido FOREIGN KEY (fk_usuario_criou)
      REFERENCES usuarios (id_usuario),

  CONSTRAINT fk_usuario_atendeu_pedido FOREIGN KEY (fk_usuario_atendeu)
      REFERENCES usuarios (id_usuario),

  CONSTRAINT fk_usuario_entregador_pedido FOREIGN KEY (fk_usuario_entregador)
      REFERENCES entregador (id_entregador)

);

create table pedidos_x_produtos(

  id_pedidos_x_produtos int NOT NULL auto_increment,
  fk_produto int,
  fk_pedido int,
  quantidade double,
  preco double,

  CONSTRAINT primary_pedidos_x_produtos PRIMARY KEY (id_pedidos_x_produtos),
  CONSTRAINT fk_produto_pedido_pp FOREIGN KEY (fk_produto)
      REFERENCES produtos (id_produto),
  CONSTRAINT fk_pedido_pp FOREIGN KEY (fk_pedido)
      REFERENCES pedidos (id_pedido)

);

/*
  Armazena as comissões que são pagas aos promotores, sobre outros promotores.
  Junto com todas alterações realizadas.
*/

create table hist_comissao (

    id_hist_comissao int NOT NULL auto_increment,
    comissao double,
    nivel int,
    fk_usuario int,
    data_comissao timestamp DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT primary_hist_comissao PRIMARY KEY (id_hist_comissao),
    CONSTRAINT fk_usuario_comissao FOREIGN KEY (fk_usuario)
      REFERENCES usuarios (id_usuario)

);


create table pagamentos(

  id_pagamento int NOT NULL auto_increment,
  fk_promotor int,
  status_pagamento int,
  data_pagamento timestamp DEFAULT CURRENT_TIMESTAMP,
  valor double,

  CONSTRAINT primary_id_pagamento PRIMARY KEY (id_pagamento),
  CONSTRAINT fk_promotor_pgs FOREIGN KEY (fk_promotor)
      REFERENCES promotores (id_promotor),
  CONSTRAINT fk_status_pgs FOREIGN KEY (status_pagamento)
      REFERENCES status (id_status)

);


/*
  Tabela que associa cada venda a um promotor e seu nível, junto do valor da comissão na época, 
  assim é só fazer um somatório simples para sabel seu saldo
*/

create table tbl_comissao (

  id_tbl_comissao int NOT NULL auto_increment,
  fk_pedido int,
  fk_promotor int,
  nivel int,
  comissao_atual double,

  CONSTRAINT primary_id_tbl_comissao PRIMARY KEY (id_tbl_comissao),
  CONSTRAINT fk_promotor_tc FOREIGN KEY (fk_promotor)
      REFERENCES promotores (id_promotor),
  CONSTRAINT fk_pedido_tc FOREIGN KEY (fk_pedido)
      REFERENCES pedidos (id_pedido)

);


/*usuários*/
Insert into usuarios (id_usuario,usuario,email,senha,nome,telefone,ativo) values (10,'WARLES','WARLES@email.com','202cb962ac59075b964b07152d234b70','WARLES LOPES DA SILVA','1122821048',TRUE);
Insert into usuarios (id_usuario,usuario,email,senha,nome,telefone,ativo) values (11,'GIGI','GIGI@email.com','202cb962ac59075b964b07152d234b70','GERLANE MENDES','1125585510',TRUE);
Insert into usuarios (id_usuario,usuario,email,senha,nome,telefone,ativo) values (12,'EULINA','EULINA@email.com','202cb962ac59075b964b07152d234b70','EULINA GONÇALVES','1129640585',TRUE);

Insert into grupo_usuarios(fk_grupo, fk_usuario) values (1,10);
Insert into grupo_usuarios(fk_grupo, fk_usuario) values (2,11);
Insert into grupo_usuarios(fk_grupo, fk_usuario) values (2,12);
