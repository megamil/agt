<?php defined('BASEPATH') OR exit('No direct script access allowed');

class controller_clientes extends CI_Controller {


	public function lista_filtro() {

		$nome = $this->input->post('nome');
		$rua = $this->input->post('rua');
		$cpf = str_replace('.', '', $this->input->post('cpf'));
		$cpf = str_replace('-', '', $cpf);

		$tel = str_replace('(', '', $this->input->post('tel'));
		$tel = str_replace(')', '', $tel);
		$tel = str_replace('-', '', $tel);

		$dados = $this->model_clientes->listar_filtro($nome,$rua,$cpf,$tel);

		foreach ($dados as $cliente) {

			    echo '<tr>';

					echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/clientes-view_editar_clientes/'.$cliente->id_cliente, 'Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Editar.', 'alt' => 'Editar.')).'</td>';

					echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/pedidos-view_listar_pedidos_clientes/'.$cliente->id_cliente, 'Compras', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect alerta', 'title' => 'Compras.', 'alt' => 'Compras.')).'</td>';

					echo '<td width="50%"  class="mdl-data-table__cell--non-numeric">'.$cliente->nome_cliente.'</td>';
					echo '<td width="10%">'.$cliente->cpf_cliente.'</td>';
					echo '<td width="20%" class="mdl-data-table__cell--non-numeric">'.$cliente->bairro_cliente.'</td>';
					echo '<td width="10%">'.$cliente->cep_cliente.'</td>';

				echo '</tr>';

		}


	}

	public function novo_cliente() {

		$this->form_validation->set_rules('nome_cliente','Nome','required');
		$this->form_validation->set_rules('cpf_cliente','CPF','is_unique[clientes.cpf_cliente]');
		$this->form_validation->set_rules('tel_cliente','Telefone','is_unique[clientes.tel_cliente]');
		$this->form_validation->set_rules('cel_cliente','Celular','is_unique[clientes.cel_cliente]');

		if ($this->input->post('data_nascimento_cliente') != '') {
			$data = explode('/',$this->input->post('data_nascimento_cliente'));
			$data_nascimento_cliente = $data[2].'-'.$data[1].'-'.$data[0];
		} else {
			$data_nascimento_cliente = null;
		}	

		$campos = array (

			'nome_cliente' => $this->input->post('nome_cliente'),
			'cpf_cliente' => $this->input->post('cpf_cliente'),
			'rg_cliente' => $this->input->post('rg_cliente'),
			'tel_cliente' => $this->input->post('tel_cliente'),
			'cel_cliente' => $this->input->post('cel_cliente'),
			'rua_cliente' => $this->input->post('rua_cliente'),
			'numero_cliente' => $this->input->post('numero_cliente'),
			'cep_cliente' => $this->input->post('cep_cliente'),
			'bairro_cliente' => $this->input->post('bairro_cliente'),
			'cidade_cliente' => $this->input->post('cidade_cliente'),
			'estado_cliente' => $this->input->post('estado_cliente'),

			'tel2_cliente' => $this->input->post('tel2_cliente'),
			'tel3_cliente' => $this->input->post('tel3_cliente'),
			'data_nascimento_cliente' => $data_nascimento_cliente,

			'complemento_cliente' => $this->input->post('complemento_cliente'),
			'obs_cliente' => $this->input->post('obs_cliente')

		);

		if($this->session->userdata('id_grupo') == 2) { //grupo promotor

			$campos['fk_promotor_cliente'] = $this->session->userdata('id_usuario');

		}

		if($this->form_validation->run()) {

			$cliente = $this->model_clientes->cliente_novo($campos);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Cliente Criado');
			$this->session->set_flashdata('mensagem','Cliente: '.$this->input->post('nome').' adicionado com sucesso!');

			if ($this->input->post('pedido') == 'pedido') {

				$id = $this->model_pedidos->gerarPedido(array('fk_cliente' =>$cliente, 'status_pedido' => 2));

				redirect('main/redirecionar/pedidos-view_novos_pedidos/'.$id);
				
			} else {

				redirect('main/redirecionar/clientes-view_editar_clientes/'.$cliente);
			}

			


		} else {

			$this->session->set_flashdata($campos);

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao criar cliente.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/clientes-view_novos_clientes/');

		}
		
	}


	public function editar_cliente(){

		$this->form_validation->set_rules('nome_cliente','Nome','required');
		
		if ($this->input->post('cpfAtual') != $this->input->post('cpf_cliente')) {
			$this->form_validation->set_rules('cpf_cliente','CPF','is_unique[clientes.cpf_cliente]');
		}

		if ($this->input->post('telefoneAtual') != $this->input->post('tel_cliente')) {
			$this->form_validation->set_rules('tel_cliente','Telefone','is_unique[clientes.tel_cliente]');
		}

		if ($this->input->post('celularAtual') != $this->input->post('cel_cliente')) {
			$this->form_validation->set_rules('cel_cliente','Celular','is_unique[clientes.cel_cliente]');
		}

		if ($this->input->post('data_nascimento_cliente') != '') {
			$data = explode('/',$this->input->post('data_nascimento_cliente'));
			$data_nascimento_cliente = $data[2].'-'.$data[1].'-'.$data[0];
		} else {
			$data_nascimento_cliente = null;
		}

		$campos = array (

			'id_cliente' => $this->input->post('id_cliente'),
			'nome_cliente' => $this->input->post('nome_cliente'),
			'cpf_cliente' => $this->input->post('cpf_cliente'),
			'rg_cliente' => $this->input->post('rg_cliente'),
			'tel_cliente' => $this->input->post('tel_cliente'),
			'cel_cliente' => $this->input->post('cel_cliente'),
			'rua_cliente' => $this->input->post('rua_cliente'),
			'numero_cliente' => $this->input->post('numero_cliente'),
			'cep_cliente' => $this->input->post('cep_cliente'),
			'bairro_cliente' => $this->input->post('bairro_cliente'),
			'cidade_cliente' => $this->input->post('cidade_cliente'),
			'estado_cliente' => $this->input->post('estado_cliente'),

			'tel2_cliente' => $this->input->post('tel2_cliente'),
			'tel3_cliente' => $this->input->post('tel3_cliente'),
			'data_nascimento_cliente' => $data_nascimento_cliente,

			'complemento_cliente' => $this->input->post('complemento_cliente'),
			'obs_cliente' => $this->input->post('obs_cliente')

		);

		if($this->form_validation->run()) {

			$this->model_clientes->update($campos);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Cliente Editado');
			$this->session->set_flashdata('mensagem','Cliente: '.$this->input->post('nome').' editado com sucesso!');

			redirect('main/redirecionar/clientes-view_editar_clientes/'.$this->input->post('id_cliente'));


		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao editar cliente.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/clientes-view_editar_clientes/'.$this->input->post('id_cliente'));

		}
		
	}

}

?>