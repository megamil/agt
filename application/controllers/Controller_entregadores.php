<?php defined('BASEPATH') OR exit('No direct script access allowed');

class controller_entregadores extends CI_Controller {


	public function lista_filtro() {

		$nome = $this->input->post('nome');
		$status = $this->input->post('status');

		$dados = $this->model_entregadores->listar_filtro($nome,$status);

		foreach ($dados as $entregadores) {

			echo '<tr>';

				echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/entregadores-view_editar_entregadores/'.$entregadores->id_entregador, 'Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Editar.', 'alt' => 'Editar.')).'</td>';

				echo '<td width="50%" class="mdl-data-table__cell--non-numeric">'.$entregadores->nome_entregador.'</td>';
				if($entregadores->status_entregador == 1){
					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">Ativo</td>';	
				} else {
					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">Inativo</td>';	
				}
				
			echo '</tr>';

		}


	}


	public function novo_entregador(){

		$this->form_validation->set_rules('nome_entregador','Nome','required');

		$campos = array (

			'nome_entregador' => $this->input->post('nome_entregador'),
			'status_entregador' => $this->input->post('status_entregador')

		);


		if($this->form_validation->run()) {

			$id = $this->model_entregadores->entregador_novo($campos);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Entregador Criado');
			$this->session->set_flashdata('mensagem','Entregador: '.$this->input->post('nome_entregador').' adicionado com sucesso!');

			redirect('main/redirecionar/entregadores-view_editar_entregadores/'.$id);


		} else {

			$this->session->set_flashdata($campos);

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao criar entregador.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/entregadores-view_novos_entregadores/');

		}

	}

	public function editar_entregador(){

		$this->form_validation->set_rules('nome_entregador','Nome','required');

		$campos = array (

			'id_entregador' => $this->input->post('id_entregador'),
			'nome_entregador' => $this->input->post('nome_entregador'),
			'status_entregador' => $this->input->post('status_entregador')

		);


		if($this->form_validation->run()) {

			$this->model_entregadores->update($campos);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Entregador Editado');
			$this->session->set_flashdata('mensagem','Entregador: '.$this->input->post('nome_entregador').' alterado com sucesso!');

			redirect('main/redirecionar/entregadores-view_editar_entregadores/'.$this->input->post('id_entregador'));


		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao criar entregador.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/entregadores-view_editar_entregadores/'.$this->input->post('id_entregador'));

		}

	}

}