<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_pagamentos extends CI_Controller {

		public function abrir_pedido() {

			if($this->model_pagamentos->novo_pedido($this->uri->segment(3))) {

				$this->session->set_flashdata('tipo','sucesso');
				$this->session->set_flashdata('titulo','Novo pedido aberto.');
				$this->session->set_flashdata('mensagem','Obrigado, agora é só aguardar a confirmação do pagamento');

				redirect('main/redirecionar/saques-view_saque_pagamentos/');

			} else {

				$this->session->set_flashdata('tipo','erro');
				$this->session->set_flashdata('titulo','Falha ao solicitar..');
				$this->session->set_flashdata('mensagem','Falha ao solicitar pagamento, por favor tente novamente.');

				redirect('main/redirecionar/saques-view_saque_pagamentos/');

			}

		}

		public function confirmar_pagamento(){

			$this->model_pagamentos->confirmar($this->uri->segment(3));

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Pagamento confirmado.');
			$this->session->set_flashdata('mensagem','Pagamento confirmado.');

			redirect('main/redirecionar/relatorios-view_pedidospg_relatorios/');

		}

		public function cancelar_pagamento(){

			$this->model_pagamentos->cancelar($this->uri->segment(3));

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Pagamento cancelado.');
			$this->session->set_flashdata('mensagem','Pagamento cancelado');

			redirect('main/redirecionar/relatorios-view_pedidospg_relatorios/');


		}

		public function finalizarPagamento(){

			$this->model_pagamentos->finalizar($this->input->post('id_pagamento'),$this->input->post('pago'));

			if($this->input->post('pago') == 8){

				$this->session->set_flashdata('tipo','sucesso');
				$this->session->set_flashdata('titulo','Pagamento alterado.');
				$this->session->set_flashdata('mensagem','Pagamento alterado para depositado.');

			}


			redirect('main/redirecionar/relatorios-view_dtspagamentos_relatorios/'.$this->input->post('id_pagamento'));

		}

	}