<?php defined('BASEPATH') OR exit('No direct script access allowed');

class controller_pedidos extends CI_Controller {

	public function buscaRapida(){

		$quantidade = $this->model_pedidos->existePedido($this->input->post('id_pedido'));

		if($quantidade->row()->quantidade == 1){

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Pedido localizado');
			$this->session->set_flashdata('mensagem','Pedido localizado!');

			redirect('main/redirecionar/pedidos-view_editar_pedidos/'.$this->input->post('id_pedido'));

		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Pedido '.$this->input->post('id_pedido').' não localizado');
			$this->session->set_flashdata('mensagem','Pedido '.$this->input->post('id_pedido').' não localizado!');

			redirect('main/redirecionar/pedidos-view_listar_pedidos/');

		}

	}

	public function lista_filtro() {

		$status = $this->input->post('status');
		$de = $this->input->post('de');
		$ate = $this->input->post('ate');
		$cpf = str_replace('.', '', $this->input->post('cpf'));
		$cpf = str_replace('-', '', $cpf);

		$dados = $this->model_pedidos->listar_filtro($status,$cpf,$de,$ate);

		foreach ($dados as $pedido) {
			    	echo '<tr>';

					echo '<td width="10%">'.anchor('main/redirecionar/pedidos-view_editar_pedidos/'.$pedido->id_pedido, 'Atender', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Atender.', 'alt' => 'Atender.')).'</td>';

					switch ($pedido->status_pedido) {
			    		case '5': //Concluído
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #00B848;" class="corStatus" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;

						case '4': //Cancelado
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #E01329;" class="corStatus" status="'.$pedido->status_pedido.'" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    		
			    		case '3': //Aguardando
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #F4CC22;" class="corStatus" status="'.$pedido->status_pedido.'" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;

			    		default:
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #E7E6E5;" class="corStatus" status="'.$pedido->status_pedido.'" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    	}

					echo '<td width="50%"  class="mdl-data-table__cell--non-numeric">'.$pedido->nome_cliente.'</td>';
					echo '<td width="10%">'.$pedido->data.'</td>';
					echo '<td width="10%">'.$pedido->cep_cliente.'</td>';
					echo '<td width="10%">R$ '.$pedido->total.'</td>';

					echo '</tr>';
				}


	}

//Usado pelo meio antigo de criar pedidos.

public function lista_filtro_cliente() {

		$nome = $this->input->post('nome');
		$cpf = str_replace('.', '', $this->input->post('cpf'));
		$cpf = str_replace('-', '', $cpf);

		$tel = str_replace('(', '', $this->input->post('tel'));
		$tel = str_replace(')', '', $tel);
		$tel = str_replace('-', '', $tel);

		$dados = $this->model_clientes->listar_filtro($nome,$cpf,$tel);

		foreach ($dados as $cliente) {

			    echo '<tr>';

					echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('controller_pedidos/novoPedido/'.$cliente->id_cliente, 'Criar Pedido', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Criar Pedido.', 'alt' => 'Criar Pedido.')).'</td>';

					echo '<td width="50%"  class="mdl-data-table__cell--non-numeric">'.$cliente->nome_cliente.'</td>';
					echo '<td width="10%">'.$cliente->cpf_cliente.'</td>';
					echo '<td width="20%" class="mdl-data-table__cell--non-numeric">'.$cliente->bairro_cliente.'</td>';
					echo '<td width="10%">'.$cliente->cep_cliente.'</td>';

				echo '</tr>';

		}


	}

	public function buscar_cliente() {

		$cpf = str_replace('.', '', $this->input->post('cpf'));
		$cpf = str_replace('-', '', $cpf);

		$rua = $this->input->post('rua');

		$tel = str_replace('(', '', $this->input->post('tel'));
		$tel = str_replace(')', '', $tel);
		$tel = str_replace('-', '', $tel);

		if ($tel == 11) {
			$tel = '';
		}

		$cliente = $this->model_clientes->filtro_cliente($cpf,$tel,$rua);

		if($cliente == 0){

			redirect('main/redirecionar/pedidos-view_filtro_pedidos?avisoCliente=1');

		} else {

			$id = $this->model_pedidos->gerarPedido(array('fk_cliente' =>$cliente, 'status_pedido' => 2));

			redirect('main/redirecionar/pedidos-view_novos_pedidos/'.$id);

		}


	}

	//Reserva um pedido para o cliente, começa inativo
	public function novoPedido() {

		$cliente = $this->uri->segment(3);

		$id = $this->model_pedidos->gerarPedido(array('fk_cliente' =>$cliente, 'status_pedido' => 2));

		redirect('main/redirecionar/pedidos-view_novos_pedidos/'.$id);


	}

	public function addProduto () {

		$pedido = $this->input->post('pedido');
		$produto = $this->input->post('produto');
		$quantidade = $this->input->post('quantidade');

		$campos['taxa_entrega'] = $this->input->post('taxa_entrega');
		$campos['fk_entregador'] = $this->input->post('fk_entregador');

		$this->session->set_flashdata($campos);

		$adicionar = $this->model_pedidos->add($pedido,$produto,$quantidade);

		if($adicionar['status']){
			
			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Produto adicionado');
			$this->session->set_flashdata('mensagem','Produto adicionado com sucesso, quantidade removida do estoque!');
			echo json_encode(array('retorno'=>true));

		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Falha ao adicionar produto');
			$this->session->set_flashdata('mensagem',$adicionar['aviso']);
			echo json_encode(array('retorno'=>false));

		}

	}

	public function removerProduto () {

		$pedido = $this->input->post('pedido');
		$produto = $this->input->post('produto');

		$campos['taxa_entrega'] = $this->input->post('taxa_entrega');
		$campos['fk_entregador'] = $this->input->post('fk_entregador');

		$this->session->set_flashdata($campos);

		$this->model_pedidos->remover($pedido,$produto);

		$this->session->set_flashdata('tipo','erro');
		$this->session->set_flashdata('titulo','Produto removido');
		$this->session->set_flashdata('mensagem','Produto removido com sucesso, quantidade retornou para estoque!');


		echo json_encode(array('retorno'=>true));

	}

	public function cancelarPedido() {

		$id_pedido = $this->input->post('pedido');

		$this->model_pedidos->cancelando($id_pedido);

		$this->session->set_flashdata('tipo','erro');
		$this->session->set_flashdata('titulo','Pedido cancelado');
		$this->session->set_flashdata('mensagem','Pedido '.$id_pedido.' cancelado com sucesso!');


		echo json_encode(array('retorno'=>true));


	}


	//Atualiza o pedido, deixa como aguardando e daqui em diante é possível alterar somente pela tela de edição.
	public function confirmar_pedido(){

		$id_pedido = $this->input->post('id_pedido');
		$taxa_entrega = $this->input->post('taxa_entrega');
		
		if($this->input->post('fk_entregador') == 0){

			$dados = array('taxa_entrega' =>$taxa_entrega, 'status_pedido' => 3);

		} else {

			$fk_entregador = $this->input->post('fk_entregador');
			$dados = array('taxa_entrega' =>$taxa_entrega,'fk_usuario_entregador' => $fk_entregador, 'status_pedido' => 3);
		}
				

		$this->model_pedidos->confirmarPedido($dados,$id_pedido);

		$this->session->set_flashdata('tipo','sucesso');
		$this->session->set_flashdata('titulo','Pedido aguardando atendimento');
		$this->session->set_flashdata('mensagem','Pedido aguardando atendimento!');

		redirect('main/redirecionar/pedidos-view_editar_pedidos/'.$id_pedido);

	}

	//Encerra de fato o pedido
	public function finalizarPedido(){

		$id_pedido = $this->input->post('pedido');
		$taxa_entrega = $this->input->post('taxa_entrega');
		
		if($this->input->post('fk_entregador') == 0){

			$fk_entregador = 'null';

			$dados = array('taxa_entrega' =>$taxa_entrega, 'status_pedido' => 5, 'data_atendimento' => 'current_timestamp');
			
		} else {

			$fk_entregador = $this->input->post('fk_entregador');
			$dados = array('taxa_entrega' =>$taxa_entrega,'fk_usuario_entregador' =>$fk_entregador, 'status_pedido' => 5, 'data_atendimento' => 'current_timestamp');

		}

		$this->model_pedidos->finalizaPedido($dados,$id_pedido);

		$this->session->set_flashdata('tipo','sucesso');
		$this->session->set_flashdata('titulo','Pedido finalizado');
		$this->session->set_flashdata('mensagem','Pedido finalizado!');

		echo json_encode(array('retorno'=>true));

	}

}

?>