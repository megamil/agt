<?php defined('BASEPATH') OR exit('No direct script access allowed');

class controller_produtos extends CI_Controller {


	public function lista_filtro() {

		$nome = $this->input->post('nome');
		$preco = str_replace(',', '.', $this->input->post('preco'));

		$dados = $this->model_produtos->listar_filtro($nome,$preco);

		foreach ($dados as $produto) {

			echo '<tr>';

				echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/produtos-view_editar_produtos/'.$produto->id_produto, 'Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Editar.', 'alt' => 'Editar.')).'</td>';

				echo '<td width="20%" class="mdl-data-table__cell--non-numeric"><a href="'.base_url().'produtos/'.$produto->url_produto.'" target="_blank" title="Click para abrir"><img src="'.base_url().'produtos/'.$produto->url_produto.'" width="120px"></a></td>';

				echo '<td width="50%"  class="mdl-data-table__cell--non-numeric">'.$produto->nome_produto.'</td>';
				echo '<td width="10%">R$ '.str_replace('.', ',',$produto->preco_produto).'</td>';
				echo '<td width="10%">'.$produto->quantidade_produto.'</td>';

			echo '</tr>';

		}


	}

	public function criar_produto() {

		$this->form_validation->set_rules('nome','Nome','required');
		$this->form_validation->set_rules('preco','Preço','required');
		$this->form_validation->set_rules('quantidade','Quantidade','required|numeric');
		$this->form_validation->set_rules('codigo','Código','required|is_unique[produtos.codigo_produto]');
		

		$campos = array (

			'nome_produto' => $this->input->post('nome'),
			'preco_produto' => str_replace(',', '.', $this->input->post('preco')),
			'quantidade_produto' => $this->input->post('quantidade'),
			'codigo_produto' => $this->input->post('codigo'),
			'descricao_produto' => $this->input->post('descricao')
			

		);

		if($this->form_validation->run()) {

			$id = $this->model_produtos->novo_produto($campos);

			$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].base_url().'produtos';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name'] = 'produto'.$id;
			
			$this->load->library('upload', $config);
		
			//Verifica se houve upload e que realmente foi enviado um arquivo.
			if (!$this->upload->do_upload('imagem') && $this->upload->do_upload('imagem') != '') {

				$this->model_produtos->del($id);

				$this->session->set_flashdata($campos);

				$this->session->set_flashdata('tipo','erro');
				$this->session->set_flashdata('titulo','Erro no upload.');
				$this->session->set_flashdata('mensagem',$this->upload->display_errors());

				redirect('main/redirecionar/produtos-view_novos_produtos/');

			} else {

				//Se realmente tiver uma imagem.
				if($this->upload->do_upload('imagem') != '') {

					$arquivo = $this->upload->data();

					$dados = array (

						'url_produto' => $arquivo['file_name'],
						'id_produto' => $id
						
					);

					$this->model_produtos->update($dados);

				}


				$this->session->set_flashdata('tipo','sucesso');
				$this->session->set_flashdata('titulo','Produto Criado');
				$this->session->set_flashdata('mensagem','Produto: '.$this->input->post('nome').' criado com sucesso!');

				redirect('main/redirecionar/produtos-view_editar_produtos/'.$id);
				
				
			}

		} else {

			$this->session->set_flashdata($campos);

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao criar produto.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/produtos-view_novos_produtos/');

		}
		
	}

	public function editar_produto() {


			if($_FILES['imagem']['name'] == "") { // não trocou a imagem

				$campos = array (

					'id_produto' => $this->input->post('id_produto'),
					'nome_produto' => $this->input->post('nome'),
					'preco_produto' => str_replace(',', '.', $this->input->post('preco')),
					'quantidade_produto' => $this->input->post('quantidade'),
					'codigo_produto' => $this->input->post('codigo'),
					'descricao_produto' => $this->input->post('descricao')

				);

			} else {

				if($this->input->post('imagemAnterior') != ""){

					unlink("produtos/".$this->input->post('imagemAnterior'));

				}

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'produtos';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name'] = 'produto'.$this->input->post('id_produto');
				
				$this->load->library('upload', $config);
			
				if (!$this->upload->do_upload('imagem')) {

					$this->session->set_flashdata('tipo','erro');
					$this->session->set_flashdata('titulo','Erro no upload, imagem anterior deletada.');
					$this->session->set_flashdata('mensagem',$this->upload->display_errors());

					redirect('main/redirecionar/produtos-view_editar_produtos/'.$this->input->post('id_produto'));

				} else {

					$arquivo = $this->upload->data();

					$campos = array (

						'id_produto' => $this->input->post('id_produto'),
						'nome_produto' => $this->input->post('nome'),
						'preco_produto' => str_replace(',', '.', $this->input->post('preco')),
						'quantidade_produto' => $this->input->post('quantidade'),
						'codigo_produto' => $this->input->post('codigo'),
						'descricao_produto' => $this->input->post('descricao'),
						'url_produto' => $arquivo['file_name']

					);

				}

			}

		$this->form_validation->set_rules('nome','Nome','required');
		$this->form_validation->set_rules('preco','Preço','required');
		$this->form_validation->set_rules('quantidade','Quantidade','required|numeric');

		//Garante que o código foi alterado.
		if($this->input->post('codigoAnterior') != $this->input->post('codigo')) {
			$this->form_validation->set_rules('codigo','Código','required|is_unique[produtos.codigo_produto]');	
		}
		
		$this->form_validation->set_rules('descricao','Descrição','required');


		if($this->form_validation->run()) {

			$this->model_produtos->update($campos);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Produto Criado');
			$this->session->set_flashdata('mensagem','Produto: '.$this->input->post('nome').' editado com sucesso!');

			redirect('main/redirecionar/produtos-view_editar_produtos/'.$this->input->post('id_produto'));

		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao editar produto.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/produtos-view_editar_produtos/'.$this->input->post('id_produto'));

		}
		
	}

}

?>