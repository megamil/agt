<?php defined('BASEPATH') OR exit('No direct script access allowed');

class controller_promotores extends CI_Controller {


	public function lista_filtro() {

		$nome = $this->input->post('nome');
		$cpf = str_replace('.', '', $this->input->post('cpf'));
		$cpf = str_replace('-', '', $cpf);

		$dados = $this->model_promotores->listar_filtro($nome,$cpf);

		foreach ($dados as $promotor) {

			    echo '<tr>';

					echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/promotores-view_editar_promotores/'.$promotor->id_promotor, 'Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Editar.', 'alt' => 'Editar.')).'</td>';

					echo '<td width="50%"  class="mdl-data-table__cell--non-numeric">'.$promotor->nome_promotor.'</td>';
					echo '<td width="10%">'.$promotor->cpf_promotor.'</td>';
					echo '<td width="20%" class="mdl-data-table__cell--non-numeric">'.$promotor->bairro_promotor.'</td>';
					echo '<td width="10%">'.$promotor->cep_promotor.'</td>';

				echo '</tr>';

		}


	}

	public function novo_promotor() {

		$this->form_validation->set_rules('nome_promotor','Nome','required');
		$this->form_validation->set_rules('login_promotor','Login','required|is_unique[promotores.login_promotor]');

		$campos = array (

			'nome_promotor' => $this->input->post('nome_promotor'),
			'cpf_promotor' => $this->input->post('cpf_promotor'),
			'rg_promotor' => $this->input->post('rg_promotor'),
			'tel_promotor' => $this->input->post('tel_promotor'),
			'cel_promotor' => $this->input->post('cel_promotor'),
			'rua_promotor' => $this->input->post('rua_promotor'),
			'numero_promotor' => $this->input->post('numero_promotor'),
			'cep_promotor' => $this->input->post('cep_promotor'),
			'bairro_promotor' => $this->input->post('bairro_promotor'),
			'cidade_promotor' => $this->input->post('cidade_promotor'),
			'estado_promotor' => $this->input->post('estado_promotor'),
			'complemento_promotor' => $this->input->post('complemento_promotor'),
			'login_promotor' => $this->input->post('login_promotor'),
			'senha_promotor' => md5($this->input->post('senha_promotor')),
			'email_promotor' => $this->input->post('email_promotor'),

			'fk_banco' => $this->input->post('fk_banco'),
			'agencia_promotor' => $this->input->post('agencia_promotor'),
			'conta_promotor' => $this->input->post('conta_promotor'),

			'status_promotor' => 1,
			'obs_promotor' => $this->input->post('obs_promotor')

		);

		if($this->session->userdata('id_grupo') == 2) { //grupo promotor

			$campos['fk_promotor'] = $this->session->userdata('id_usuario');

		}

		if($this->form_validation->run()) {

			$id = $this->model_promotores->promotor_novo($campos);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','promotor Criado');
			$this->session->set_flashdata('mensagem','promotor: '.$this->input->post('nome').' adicionado com sucesso!');

			redirect('main/redirecionar/promotores-view_editar_promotores/'.$id);


		} else {

			$this->session->set_flashdata($campos);

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao criar promotor.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/promotores-view_novos_promotores/');

		}
		
	}


	public function editar_promotor(){

		$this->form_validation->set_rules('nome_promotor','Nome','required');
		
		if ($this->input->post('loginAtual') != $this->input->post('login_promotor')) {
			$this->form_validation->set_rules('login_promotor','Login','required|is_unique[promotores.login_promotor]');
		}

		if(is_null($this->input->post('status_promotor'))) {
			$status_promotor = 1;	
		} else {
			$status_promotor = $this->input->post('status_promotor');	
		}

		$campos = array (

			'id_promotor' => $this->input->post('id_promotor'),
			'nome_promotor' => $this->input->post('nome_promotor'),
			'cpf_promotor' => $this->input->post('cpf_promotor'),
			'rg_promotor' => $this->input->post('rg_promotor'),
			'tel_promotor' => $this->input->post('tel_promotor'),
			'cel_promotor' => $this->input->post('cel_promotor'),
			'rua_promotor' => $this->input->post('rua_promotor'),
			'numero_promotor' => $this->input->post('numero_promotor'),
			'cep_promotor' => $this->input->post('cep_promotor'),
			'bairro_promotor' => $this->input->post('bairro_promotor'),
			'cidade_promotor' => $this->input->post('cidade_promotor'),
			'estado_promotor' => $this->input->post('estado_promotor'),
			'complemento_promotor' => $this->input->post('complemento_promotor'),
			'login_promotor' => $this->input->post('login_promotor'),
			'status_promotor' => $status_promotor,
			'email_promotor' => $this->input->post('email_promotor'),
			'fk_banco' => $this->input->post('fk_banco'),
			'agencia_promotor' => $this->input->post('agencia_promotor'),
			'conta_promotor' => $this->input->post('conta_promotor'),
			'obs_promotor' => $this->input->post('obs_promotor')

		);

		if($this->input->post('senha_promotor') != '') {

			$campos['senha_promotor'] = md5($this->input->post('senha_promotor'));

		}


		if($this->form_validation->run()) {

			$this->model_promotores->update($campos);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','promotor Editado');
			$this->session->set_flashdata('mensagem','promotor: '.$this->input->post('nome').' editado com sucesso!');

			redirect('main/redirecionar/promotores-view_editar_promotores/'.$this->input->post('id_promotor'));


		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao editar promotor.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/promotores-view_editar_promotores/'.$this->input->post('id_promotor'));

		}
		
	}

}

?>