<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_relatorios extends CI_Controller {

		public function ajax_pedidospg(){

		$status_pagamento = $this->input->post('status_pagamento');
		  $promotor = $this->input->post('promotor');

		  $dados = $this->model_relatorios->filtro_pedidospg($status_pagamento,$promotor);

		 	foreach ($dados as $pg) {
			    	echo '<tr>';

					switch ($pg->id_status) {
			    		case 3:
			    		echo '<td width="10%" align="left">'.anchor('Controller_pagamentos/confirmar_pagamento/'.$pg->id, 'Pagar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Pagar.', 'alt' => 'Pagar.'));

			    		echo ' '.anchor('Controller_pagamentos/cancelar_pagamento/'.$pg->id, 'Cancelar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect erro', 'title' => 'Cancelar.', 'alt' => 'Cancelar.', 'style' => 'background-color: #C1272D')).'</td>';
			    			break;

			    		case 4:
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color:#C1272D">'.$pg->nome_status.'</td>';
			    			break;
			    		
			    		default:
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color:#118049">'.$pg->nome_status.'</td>';
			    			break;
			    	}
						
					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">'.$pg->id.'</td>';

					echo '<td width="40%" class="mdl-data-table__cell--non-numeric">'.$pg->nome.'</td>';

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">'.$pg->data_pg.'</td>';

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">R$ '.$pg->total.'</td>';
					
					echo '</tr>';
			}

		}

		public function excel_pedidospg(){

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'pagamentos.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html ='<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
		<table>
		  <thead>
		    <tr>
		      <th>Status</th>
		      <th>Pagamento</th>
		      <th>Promotor</th>
		      <th>Pedido em:</th>
		      <th>Valor Solicitado</th>
		    </tr>
		  </thead>
		  <tbody>';

		  $status_pagamento = $this->input->post('status_pagamento');
		  $promotor = $this->input->post('promotor');

		  $dados = $this->model_relatorios->filtro_pedidospg($status_pagamento,$promotor);

		 	foreach ($dados as $pg) {
			    	$html .= '<tr>';

					$html .= '	<td>'.$pg->nome_status.'</td>';
						
					$html .= '	<td>'.$pg->id.'</td>';

					$html .= '	<td>'.$pg->nome.'</td>';

					$html .= '	<td>'.$pg->data_pg.'</td>';

					$html .= '	<td>R$ '.$pg->total.'</td>';
					
					$html .= '</tr>';
			}

		  $html .= "</tbody>
		</table>";

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;

		}

		public function ajax_vendas(){

			$nivel = $this->input->post('nivel');
			$promotor = $this->input->post('promotor');
			$cliente = $this->input->post('cliente');
			$pedido = $this->input->post('pedido');
			$de = $this->input->post('de');
			$ate = $this->input->post('ate');

			$dados = $this->model_relatorios->filtro_vendas($nivel,$promotor,$cliente,$pedido,$de,$ate);

			foreach ($dados as $comporpromotor) {
			    
			    echo '<tr>';

				echo '<td width="20%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->nome_cliente.'</td>';

				echo '<td width="20%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->nome_promotor.'</td>';

				echo '<td width="10%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->fk_pedido.'</td>';

				echo '<td width="10%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->data_pedido.'</td>';

				echo '<td width="10%" class="mdl-data-table__cell--non-numeric">R$ '.$comporpromotor->comissao_atual.'</td>';

				echo '<td width="60%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->nivel.'</td>';

				echo '<td width="10%">'.anchor('main/redirecionar/pedidos-view_editar_pedidos/'.$comporpromotor->fk_pedido, 'Detalhes', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Detalhes.', 'alt' => 'Detalhes.')).'</td>';
				
				echo '</tr>';
			}


		}
		

		public function excel_vendas(){

			// Definimos o nome do arquivo que será exportado
			$arquivo = 'vendas.xls';
			
			// Criamos uma tabela HTML com o formato da planilha
			$html ='<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
			<table>
			  <thead>
			    <tr>
			      <th>Cliente</th>
			      <th>Promotor</th>
			      <th>Pedido</th>
			      <th>Data do pedido</th>
			      <th>Comissão</th>
			      <th>Nível</th>
			    </tr>
			  </thead>
			  <tbody>';

				$nivel = $this->input->post('nivel');
				$promotor = $this->input->post('promotor');
				$cliente = $this->input->post('cliente');
				$pedido = $this->input->post('pedido');
				$de = $this->input->post('de');
				$ate = $this->input->post('ate');

				$dados = $this->model_relatorios->filtro_vendas($nivel,$promotor,$cliente,$pedido,$de,$ate);

				foreach ($dados as $comporpromotor) {
				    
				    $html .= '<tr>';

					$html .= '<td>'.$comporpromotor->nome_cliente.'</td>';

					$html .= '<td>'.$comporpromotor->nome_promotor.'</td>';

					$html .= '<td>'.$comporpromotor->fk_pedido.'</td>';

					$html .= '<td>'.$comporpromotor->data_pedido.'</td>';

					$html .= '<td>R$ '.$comporpromotor->comissao_atual.'</td>';

					$html .= '<td>'.$comporpromotor->nivel.'</td>';
					
					$html .= '</tr>';
				}

			  $html .= "</tbody>
			</table>";

			// Configurações header para forçar o download
			header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
			header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
			header ("Content-Transfer-Encoding: binary"); 
			header ("Content-Type: application/vnd.ms-excel"); 
			header ("Expires: 0"); 
			header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
			header ("Content-Description: PHP Generated Data");

			// Envia o conteúdo do arquivo
			chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
			echo $html;
			exit;

		}


	}

?>