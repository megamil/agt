<?php defined('BASEPATH') OR exit('No direct script access allowed');

class controller_seguranca extends CI_Controller {


	public function criar_usuario() {

		if ($this->input->post('ativo') == 'on') {$ativo_flashdata = 'on'; $ativo_banco = 1;} else {$ativo_flashdata = 'off'; $ativo_banco = 0;} //Devido o uso do MDL

		$campos = array (

			'usuario' => $this->input->post('usuario'),
			'email' => $this->input->post('email'),
			'telefone' => $this->input->post('telefone'),
			'nome' => $this->input->post('nome'),

		);

		$this->form_validation->set_rules('usuario','Usuário','required|is_unique[usuarios.usuario]');
		$this->form_validation->set_rules('email','E-mail','required|is_unique[usuarios.email]');
		$this->form_validation->set_rules('telefone','Telefone','required|numeric');
		$this->form_validation->set_rules('nome','Nome','required');
		$this->form_validation->set_rules('senha','Senha','required|matches[confirmacao]');
		$this->form_validation->set_rules('confirmacao','Confirmação de senha','required');

		if($this->form_validation->run()) {

			$campos['ativo'] = $ativo_banco;
			$campos['senha'] = md5($this->input->post('senha'));

			$id = $this->model_usuarios->criar_usuarios($campos,$this->input->post('fk_grupo'));

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Usuário Criado');
			$this->session->set_flashdata('mensagem','Usuário: '.$this->input->post('usuario').' criado com sucesso!, Adicione ele em um ou mais grupos.');

			redirect('main/redirecionar/seguranca-view_editar_usuarios/'.$id);

		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao criar usuário.');
			$this->session->set_flashdata('mensagem',validation_errors());

			$campos['ativo'] = $ativo_flashdata;
			$this->session->set_flashdata($campos);

			redirect('main/redirecionar/seguranca-view_novos_usuarios/');

		}
		
	}

	public function editar_usuario() {

		if($this->input->post('usuario') !=  $this->input->post('usuario_anterior')){

			$this->form_validation->set_rules('usuario','Usuário','required|is_unique[usuarios.usuario]');

		}
		
		if($this->input->post('email') !=  $this->input->post('email_anterior')){

			$this->form_validation->set_rules('email','E-mail','required|is_unique[usuarios.email]');

		}

		$this->form_validation->set_rules('telefone','Telefone','required|numeric');
		$this->form_validation->set_rules('nome','Nome','required');

		if($this->input->post('senha') != '') { //Senha não é obrigatório, mas se for preenchida deve ser igual a confirmação.
			$this->form_validation->set_rules('senha','Senha','required|matches[confirmacao]');
			$this->form_validation->set_rules('confirmacao','Confirmação de senha','required');
		}

		if($this->form_validation->run()) {

			if ($this->input->post('ativo') == 'on') {$ativo = 1;} else {$ativo = 0;} //Devido o uso do MDL

			$campos = array (

				'id_usuario' => $this->input->post('id_usuario'),
				'usuario' => $this->input->post('usuario'),
				'email' => $this->input->post('email'),
				'telefone' => $this->input->post('telefone'),
				'nome' => $this->input->post('nome'),
				'ativo' => $ativo

			);

			if($this->input->post('senha') != '') { //Senha não é obrigatório, mas se for preenchida deve ser igual a confirmação.
				$campos['senha']  = md5($this->input->post('senha'));
			}

			$this->model_usuarios->update_usuarios($campos,$this->input->post('fk_grupo'));

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Editado com sucesso!');
			$this->session->set_flashdata('mensagem','Usuário: '.$this->input->post('usuario').' editado com sucesso.');

			redirect('main/redirecionar/seguranca-view_editar_usuarios/'.$this->input->post('id_usuario'));

		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao editar usuário.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/seguranca-view_editar_usuarios/'.$this->input->post('id_usuario'));
		}

	}

	public function editar_proprio_usuario() {

		if($this->input->post('usuario') !=  $this->input->post('usuario_anterior')){

			$this->form_validation->set_rules('usuario','Usuário','required|is_unique[usuarios.usuario]');
			$this->session->set_userdata('usuario',$this->input->post('usuario'));

		}
		
		if($this->input->post('email') !=  $this->input->post('email_anterior')){

			$this->form_validation->set_rules('email','E-mail','required|is_unique[usuarios.email]');

		}

		$this->form_validation->set_rules('telefone','Telefone','required|numeric');
		$this->form_validation->set_rules('nome','Nome','required');

		if($this->input->post('senha') != '') { //Senha não é obrigatório, mas se for preenchida deve ser igual a confirmação.
			$this->form_validation->set_rules('senha','Senha','required|matches[confirmacao]');
			$this->form_validation->set_rules('confirmacao','Confirmação de senha','required');
		}

		if($this->form_validation->run()) {

			$campos = array (

				'id_usuario' => $this->input->post('id_usuario'),
				'usuario' => $this->input->post('usuario'),
				'email' => $this->input->post('email'),
				'telefone' => $this->input->post('telefone'),
				'nome' => $this->input->post('nome'),

			);

			if($this->input->post('senha') != '') { //Senha não é obrigatório, mas se for preenchida deve ser igual a confirmação.
				$campos['senha']  = md5($this->input->post('senha'));
			}

			/*GRAVA O VALOR DE CONFIGURAÇÃO PARA AS COMISSÕES DOS PROMOTORES, SOBRE OS VENDAS.*/

		if ($this->session->userdata('id_grupo') == 1) {
			$this->model_promotores->atualizar_comissao(str_replace(',', '.',$this->input->post('comissao1')),1);

			$this->model_promotores->atualizar_comissao(str_replace(',', '.',$this->input->post('comissao2')),2);

			$this->model_promotores->atualizar_comissao(str_replace(',', '.',$this->input->post('comissao3')),3);

			$this->model_promotores->atualizar_comissao(str_replace(',', '.',$this->input->post('comissao4')),4);

			$this->model_promotores->atualizar_comissao(str_replace(',', '.',$this->input->post('comissao5')),5);
		}

			$this->model_usuarios->update_usuarios($campos);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Perfil editado!');
			$this->session->set_flashdata('mensagem','Seu perfil foi editado com sucesso.');

			redirect('main/redirecionar/seguranca-view_proprios_usuarios/');

		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao editar usuário.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/seguranca-view_proprios_usuarios/');
		}

	}

	public function add_rem_grupo() {

		$dados = array (

			'fk_grupo' => $this->uri->segment(4),
			'fk_usuario' => $this->uri->segment(5),

		);

		$this->model_usuarios->rem_add_grupo($dados,$this->uri->segment(3));

		$this->session->set_flashdata('titulo','Editado com sucesso!');
		if($this->uri->segment(3) == 'remover'){
			$this->session->set_flashdata('tipo','aviso');
			$this->session->set_flashdata('mensagem','Usuário removido do grupo com sucesso.');
		} else {
			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('mensagem','Usuário adicionado ao grupo com sucesso.');
		}

		redirect('main/redirecionar/seguranca-view_editar_usuarios/'.$this->uri->segment(5));

	}

	public function criar_grupo() {

		$this->form_validation->set_rules('nome_grupo','Nome do Grupo','required|is_unique[grupos.nome_grupo]');
		$this->form_validation->set_rules('descricao_grupo','Descrição do Grupo','required');

		$campos = array (

			'nome_grupo' => $this->input->post('nome_grupo'),
			'descricao_grupo' => $this->input->post('descricao_grupo')

		);

		if($this->form_validation->run()) {

			$id = $this->model_grupos->criar_grupos($campos);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Grupo criado!');
			$this->session->set_flashdata('mensagem','Grupo: '.$this->input->post('nome_grupo').' criado com sucesso!, Adicione as aplicações que seus integrantes devem ter acesso.');

			redirect('main/redirecionar/seguranca-view_editar_grupos/'.$id);


		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao criar grupo.');
			$this->session->set_flashdata('mensagem',validation_errors());

			$this->session->set_flashdata($campos);

			redirect('main/redirecionar/seguranca-view_novos_grupos');
		}

		
	}

	public function editar_grupo() {

		if($this->input->post('email') !=  $this->input->post('email_anterior')){
			
			$this->form_validation->set_rules('nome_grupo','Nome do Grupo','required|is_unique[grupos.nome_grupo]');

		}

		$this->form_validation->set_rules('descricao_grupo','Descrição do Grupo','required');

		$campos = array (

			'id_grupo' => $this->input->post('id_grupo'),
			'nome_grupo' => $this->input->post('nome_grupo'),
			'descricao_grupo' => $this->input->post('descricao_grupo')

		);

		if($this->form_validation->run()) {

			$id = $this->model_grupos->update_grupos($campos);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Grupo editado!');
			$this->session->set_flashdata('mensagem','Grupo: '.$this->input->post('nome_grupo').' Editado com sucesso!.');

			redirect('main/redirecionar/seguranca-view_editar_grupos/'.$this->input->post('id_grupo'));


		} else {

			$this->session->set_flashdata('tipo','erro');
			$this->session->set_flashdata('titulo','Erro ao editar grupo.');
			$this->session->set_flashdata('mensagem',validation_errors());

			redirect('main/redirecionar/seguranca-view_editar_grupos/'.$this->input->post('id_grupo'));
		}
		
	}

	public function add_rem_aplicacao() {

		$dados = array (

			'fk_grupo' => $this->uri->segment(4),
			'fk_aplicacao' => $this->uri->segment(5),

		);

		$this->model_grupos->rem_add_aplicacao($dados,$this->uri->segment(3));

		$this->session->set_flashdata('titulo','Editado com sucesso!');
		if($this->uri->segment(3) == 'remover'){
			$this->session->set_flashdata('tipo','aviso');
			$this->session->set_flashdata('mensagem','Aplicação removida do grupo com sucesso.');
		} else {
			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('mensagem','Aplicação adicionada ao grupo com sucesso.');
		}

		redirect('main/redirecionar/seguranca-view_editar_grupos/'.$this->uri->segment(4));

	}

	public function esqueci_Senha() {

		$caracteres = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,*,-';
		$caractereVetor = explode(',',$caracteres);
		$senha = '';

		while(strlen($senha) < 10) { //Cria uma nova senha com 10 caracteres.
			
			$indice = mt_rand(0, count($caractereVetor));
			$senha .= $caractereVetor[$indice];

		}

		$email = $this->model_seguranca->senha_Email(md5($senha),$this->input->get('user'));

		//Usuário inativo ou desativado
		if($email == ''){

			echo 'Usuário: '.$this->input->get('user').' está desabilitado ou não existe, entre em contato com o administrador!';

		} else { //Senha enviada para o E-mail.

			// Detalhes do Email. 
			$this->email->from('agt@megamil.net', 'Gás para todos'); 
			$this->email->to($email); 
			$this->email->subject('Gás para todos - Troca de senha'); 
			$this->email->message('Sua nova senha é: '.$senha);  

			// Enviar... 
			if ($this->email->send()) { 
				echo 'Nova Senha enviada para o E-mail: "'.$email.'"';
			} else {
				echo $this->email->print_debugger(); 
			}

		}


	}

	public function esqueci_Senha_Promotor() {

		$caracteres = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,*,-';
		$caractereVetor = explode(',',$caracteres);
		$senha = '';

		while(strlen($senha) < 10) { //Cria uma nova senha com 10 caracteres.
			
			$indice = mt_rand(0, count($caractereVetor));
			$senha .= $caractereVetor[$indice];

		}

		$email = $this->model_seguranca->senha_Email_Promotor(md5($senha),$this->input->get('user'));

		//Usuário inativo ou desativado
		if($email == ''){

			echo 'Usuário: '.$this->input->get('user').' está desabilitado ou não existe, entre em contato com o administrador!';

		} else { //Senha enviada para o E-mail.

			// Detalhes do Email. 
			$this->email->from('agt@megamil.net', 'Gás para todos'); 
			$this->email->to($email); 
			$this->email->subject('Gás para todos - Troca de senha'); 
			$this->email->message('Sua nova senha é: '.$senha); 

			// Enviar... 
			if ($this->email->send()) { 
				echo 'Nova Senha enviada para o E-mail: "'.$email.'"';
			} else {
				echo $this->email->print_debugger(); 
			}

		}


	}

}

?>