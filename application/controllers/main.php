<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function master($dados = null) {

		$this->session->set_userdata('promotor',false);

		if ($this->session->userdata('online')) { /*Valida se o usuário já estava logado*/

			$dados = array (

				'tela' => 'Tela Principal',
				'dados' => $this->model_seguranca->inicioMaster(),
				'aviso' => array (

						'tipo' => '', 
						'titulo' =>	'', 
						'mensagem' => '' 
					) //aviso

			);

			$this->load->view('estrutura/header',$dados);
			$this->load->view('inicio');
			$this->load->view('estrutura/footer');

		} else { /*Caso não esteja logado*/
			
			if(is_null($dados)){ // se não existir um aviso

				$dados = array (

					'tela' => 'Nome da Aplicação',
					'aviso' => array (

						'tipo' => '', 
						'titulo' =>	'', 
						'mensagem' => '' 
					) //aviso

				); //dados

			}//if interno

			$this->load->view('index',$dados);

		} //else

	}

	public function index($dados = null){

		if($this->session->userdata('promotor')){

			$this->promotor($dados);

		} else {

			$this->master($dados);

		}

	}

	public function promotor($dados = null) {

		$this->session->set_userdata('promotor',true);

		if ($this->session->userdata('online')) { /*Valida se o usuário já estava logado*/

			$dados = array (

				'tela' => 'Tela Principal',
				'dados' => $this->model_seguranca->inicioPromotor(),
				'aviso' => array (

						'tipo' => '', 
						'titulo' =>	'', 
						'mensagem' => '' 
				) //aviso

			);

			$this->load->view('estrutura/headerPromotor',$dados);
			$this->load->view('inicioPromotor');
			$this->load->view('estrutura/footer');

		} else { /*Caso não esteja logado*/
			
			if(is_null($dados)){ // se não existir um aviso

				$dados = array (

					'tela' => 'Nome da Aplicação',
					'aviso' => array (

						'tipo' => '', 
						'titulo' =>	'', 
						'mensagem' => '' 
					) //aviso

				); //dados

			}//if interno

			$this->load->view('promotor',$dados);

		} //else

	}

	public function login() {

		$login = array ( /*Campos que foram digitados*/

			'usuario' => $this->input->post('user'),
			'senha' => md5($this->input->post('pass'))

			);

		$usuario = $this->model_seguranca->validar($login);

		if($usuario) { /*Valida dados*/

			$secao = array ( /*Inicia a seção*/

				'usuario' => $this->input->post('user'),
				'nome' => $usuario->row()->nome,
				'online' => true,
				'id_usuario' => $usuario->row()->id_usuario,
				'id_grupo' => $usuario->row()->id_grupo

			);

			$this->session->set_userdata($secao);

			$this->index();

		} else { /*Carrega a tela para logar*/

			$dados = array (

				'tela' => 'Nome da Aplicação',
				'aviso' => array (
					'tipo' => 'erro',
					'titulo' =>	'Erro de acesso!',
					'mensagem' => 'Usuário ou senha inválidos!'
				)

			);

			if(!$this->session->userdata('promotor')){

				$this->load->view('index',$dados);

			} else {

				$this->load->view('promotor',$dados);

			}

		}

	}

	public function Sair() {

		$nome = $this->session->userdata('usuario');
		$login = $this->session->userdata('promotor');

		$this->session->sess_destroy();

			$dados = array (

				'tela' => 'Nome da Aplicação',
				'aviso' => array (
					'tipo' => 'sucesso',
					'titulo' =>	'Até logo!',
					'mensagem' => 'Usuário '.$nome.' saiu do sistema com sucesso!.'
				)

			);

			$this->session->set_userdata('promotor',$login);

			if(!$this->session->userdata('promotor')){

				$this->load->view('index',$dados);

			} else {

				$this->load->view('promotor',$dados);

			}


	}

	public function erro() {

		if (!$this->session->userdata('online')) {

			$dados = array (

					'tela' => 'Nome da Aplicação',
					'aviso' => array (

						'tipo' => 'aviso', 
						'titulo' =>	'Offline!', 
						'mensagem' => 'Você precisa estar online para acessar o sistema!' 
					) //aviso

			); //dados

			if(!$this->session->userdata('promotor')){

				$this->load->view('index',$dados);

			} else {

				$this->load->view('promotor',$dados);

			}

		} else {

			$dados = array (

					'tela' => 'Erro 404',
					'aviso' => array (
						'tipo' => 'erro',
						'titulo' =>	'Erro 404',
						'mensagem' => 'Página não encontrada!'
					)
				);

			$this->load->view('estrutura/header',$dados);
			$this->load->view('erros/pagina_nao_encontrada');
			$this->load->view('estrutura/footer');

		}

	}
	public function redirecionar(){

		if (!$this->session->userdata('online')) {

			$dados = array (

					'tela' => 'Nome da Aplicação',
					'aviso' => array (

						'tipo' => 'aviso', 
						'titulo' =>	'Offline!', 
						'mensagem' => 'Você precisa estar online para acessar o sistema!' 
					) //aviso

			); //dados

			$this->index($dados);

		} else {

			//Verifica se for passado algum argumento inicio.
			$continuar = true; //Verifica se deve continuar procurando por argumentos na URL.
			$i = 4; //Começa no argumento 4 da url.
			$where = array(); //Armazena os argumentos.

			while($continuar) { // Enquanto tiver argumento irá armazenando no array.

				if($this->uri->segment($i) != '') { //confirma um argumento na posição i.
					
					$where[]  = $this->uri->segment($i); //Adicionar argumento no array.
					$i++; //Incrementa para procurar outro argumento na proxima passagem pelo if.

				} else {

					$continuar = false; //para o while.

				}

			}
			//Verifica se for passado algum argumento fim.

			$tela = $this->depois('-', $this->uri->segment(3));

			if($this->model_seguranca->acesso($tela) == 1 and $this->session->userdata('online')) { //validar acesso

				$endereco = str_replace("-", "/",$this->uri->segment(3)); /*troca o - por uma identificação de diretório*/

				$funcao = $this->depois('_', $tela); //Pega do nome da view a função correspondente.

				$model = explode('_',$funcao);

				$model = 'model_'.($model[count($model)-1]); //Pega do nome da função o model correspondente.

				if(method_exists($this->$model, $funcao)) {

					$dados_iniciais = $this->$model->$funcao($where);

				} else {

					$dados_iniciais = '';

				}

				$dados = array (

					'tela' => $this->model_seguranca->titulo($tela)->titulo_aplicacao, //Carregar do banco de dados.
					'dados' => $dados_iniciais,
					'aviso' => array (
						'tipo' => $this->session->flashdata('tipo'), /*Escolha entre: erro, aviso e sucesso*/
						'titulo' =>	$this->session->flashdata('titulo'), /*Mensagem que ficará no título do aviso*/
						'mensagem' => $this->session->flashdata('mensagem') /*Mensagem do aviso*/
					)
				);

				if(!$this->session->userdata('promotor')){
					$this->load->view('estrutura/header',$dados);
				} else {
					$this->load->view('estrutura/headerPromotor',$dados);
				}
				$this->load->view($endereco);
				$this->load->view('estrutura/footer');

			} else { // Redirecionar para tela com aviso que não possui permissão de acesso.

				$dados = array (

					'tela' => 'Sem permissão de acesso.', //Carregar do banco de dados.
					'aviso' => array (
						'tipo' => 'aviso',
						'titulo' =>	'Sem permissão de acesso.',
						'mensagem' => 'O Grupo do seu usuário não possuí permissão para acessar está página.'
					)

				);

				if(!$this->session->userdata('promotor')){
					$this->load->view('estrutura/header',$dados);
				} else {
					$this->load->view('estrutura/headerPromotor',$dados);
				}
				
				$this->load->view('erros/sem_permissao');
				$this->load->view('estrutura/footer');

			}

		}

	}

	function depois ($deste, $texto) { // Pegar restante da String.
	    
	        if (!is_bool(strpos($texto, $deste)))
	        return substr($texto, strpos($texto,$deste)+strlen($deste));
	    
	}

}

?>