<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_clientes extends CI_Model {

		public function listar_clientes() {

			try {

				if($this->session->userdata('promotor')){ //Caso o usuário logado seja um promotor
					$sql = 'SELECT id_cliente,
										nome_cliente,
										cpf_cliente,
										bairro_cliente,
										nome_promotor,
										cep_cliente FROM clientes
										left join promotores on id_promotor = fk_promotor_cliente
										where id_promotor = '.$this->session->userdata('id_usuario').' limit 100;';
				} else {
					$sql = 'SELECT id_cliente,
										nome_cliente,
										cpf_cliente,
										bairro_cliente,
										nome_promotor,
										cep_cliente FROM clientes
										left join promotores on id_promotor = fk_promotor_cliente  limit 100 ;';
				}

				return $this->db->query($sql)->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar os clientes: '.$e;
				
			}


		}

		public function listar_pedidos_clientes($where = null) {

			try {

				return array ('pedidos' => $this->db->query('select id_pedido,DATE_FORMAT(data_pedido, \'%d/%m/%Y %H:%i:%s\') data, status_pedido, cep_cliente,
										(select ((sum(preco * quantidade)) + taxa_entrega) 
										from pedidos_x_produtos where fk_pedido = id_pedido ) as total
										from pedidos 
										inner join clientes on id_cliente = fk_cliente
										where status_pedido != 2 and id_cliente = '.$where[0].';')->result(),

							'cliente' => $this->db->query('select nome_cliente from clientes where id_cliente = '.$where[0])->row()->nome_cliente);
				
			} catch (Exception $e) {

				echo 'Falha ao listar os pedidos: '.$e;
				
			}



		}

		public function novos_clientes() {

			try {

				return $this->db->get('estados')->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar os estados: '.$e;
				
			}

		}

		public function editar_clientes($where = null) { //Array

			try {

				return array ('cliente' =>$this->db->query('select c.*, nome_promotor from clientes c left join promotores on id_promotor = fk_promotor_cliente where id_cliente = '.$where[0]),
							  'estados' => $this->db->get('estados')->result()
							);
				
			} catch (Exception $e) {

				echo 'Falha ao carregar grupo: '.$e;
				
			}

		}

		public function cliente_novo($cliente = null){

			try {

				$this->db->insert('clientes',$cliente);
				return $this->db->insert_id(); //Retorna o id do novo cliente.

			} catch (Exception $e) {

				echo 'Falha ao criar cliente: '.$e;

			}

		}

		public function update($cliente = null) {

			try {
				
				$this->db->where('id_cliente', $cliente['id_cliente']);
				return $this->db->update('clientes',$cliente);

			} catch (Exception $e) {

				echo 'Falha ao editar '.$e;
				return false;
				
			}	

		}

		//Filtro para o modo antigo de criar um pedido
		public function listar_filtro($nome = null,$rua = null,$cpf = null,$tel = null){


			$sql = 'SELECT * FROM clientes';

			$where = ' where ';

			if($cpf != '') {
				$where .= "cpf_cliente = '".$cpf."'";
			} 

			if($nome != '') {
				if($where != ' where ') {$where .= ' and ';}
				$where .= "nome_cliente like '%".$nome."%'";
			}

			if($tel != '') {
				if($where != ' where ') {$where .= ' and ';}
				$where .= "tel_cliente like '%".$tel."%' or cel_cliente like '%".$tel."%'
				or tel2_cliente like '%".$tel."%' or tel3_cliente like '%".$tel."%'";
			}

			if($rua != '') {
				if($where != ' where ') {$where .= ' and ';}
				$where .= "rua_cliente like '%".$rua."%'";
			}

			//Caso venha sem nenhum valor.
			if($where == ' where ') {$where = '';}

			try {

				return $this->db->query($sql.$where)->result();

			} catch (Exception $e) {

				echo 'Falha: '.$e;
				
			}


		}

		public function filtro_cliente($cpf = null,$tel = null,$rua = null){

			$sql = 'SELECT id_cliente FROM clientes';

			$where = ' where ';

			if($cpf != '') {
				$where .= "cpf_cliente = '".$cpf."'";
			} 

			if($tel != '') {
				if($where != ' where ') {$where .= ' and ';}
				$where .= "tel_cliente like '%".$tel."%' or cel_cliente like '%".$tel."%'
				or tel2_cliente like '%".$tel."%' or tel3_cliente like '%".$tel."%'";
			}

			if($rua != '') {
				if($where != ' where ') {$where .= ' and ';}
				$where .= "rua_cliente like '%".$rua."%'";
			}

			//Caso venha sem nenhum valor.
			if($where == ' where ') {$where = '';}

			try {

				$resultado = $this->db->query($sql.$where);

				if($resultado->num_rows() == 0){
					return 0;
				} else {
					return $resultado->row()->id_cliente;
				}


			} catch (Exception $e) {

				echo 'Falha: '.$e;
				
			}

		}
		

	}

?>