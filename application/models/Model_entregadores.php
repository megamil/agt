<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_entregadores extends CI_Model {

		public function listar_entregadores() {
			try {

				return $this->db->get('entregador')->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar os entregadores: '.$e;
				
			}
		}

		public function listar_filtro($nome = null, $status = null){

			$sql = 'SELECT * FROM entregador';

			if($status != '' && $nome != '') {

				$sql .= " where status_entregador = ".$status." and nome_entregador like '%".$nome."%';";

			} else if($status != '') {
			
				$sql .= " where status_entregador = ".$status.";";
			
			} else if($nome != '') {

				$sql .= " where nome_entregador like '%".$nome."%';";

			}

			try {

				return $this->db->query($sql)->result();

			} catch (Exception $e) {

				echo 'Falha: '.$e;
				
			}


		}

		public function editar_entregadores($where = null) { //Array

			try {

				return $this->db->get_where('entregador',array ('id_entregador' => $where[0]));
				
			} catch (Exception $e) {

				echo 'Falha ao carregar grupo: '.$e;
				
			}

		}

		public function update($entregador = null) {

			try {
				
				$this->db->where('id_entregador', $entregador['id_entregador']);
				return $this->db->update('entregador',$entregador);

			} catch (Exception $e) {

				echo 'Falha ao editar '.$e;
				return false;
				
			}	

		}

		public function entregador_novo($entregador = null) {

			try {

				$this->db->insert('entregador',$entregador);
				return $this->db->insert_id(); //Retorna o id do novo cliente.
				
			} catch (Exception $e) {
				
				echo 'Falha ao criar entregador: '.$e;

			}
		}

	}