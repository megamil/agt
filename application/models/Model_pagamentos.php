<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_pagamentos extends CI_Model {

		public function novo_pedido($valor = null) {

			$id_usuario = $this->session->userdata('id_usuario');

			$dados['fk_promotor'] = $id_usuario;
			$dados['status_pagamento'] = 3;
			$dados['valor'] = $valor;

			//Pedido criado.
			$this->db->insert('pagamentos',$dados);

			return true;
			
		}

		public function finalizar($id_pagamento = null, $status = null){

			return $this->db->query('update pagamentos set status_pagamento = '.$status.' where id_pagamento = '.$id_pagamento);
			
		}

		public function confirmar($id = null){

			return $this->db->query('update pagamentos set status_pagamento = 5 where id_pagamento = '.$id);

		}

		public function cancelar($id = null){

			return $this->db->query('update pagamentos set status_pagamento = 4 where id_pagamento = '.$id);

		}		

		public function saque_pagamentos(){

			/*valores = Caso não existam pedidos de pagamento (is null) a subtração recebe um 0, caso contrario o valor dos pagamentos*/

			$id = $this->session->userdata('id_usuario');

			return $dados = array('valores' => $this->db->query("select 
												format((sum(comissao_atual) -
												IF(
														(select sum(valor) as saques 
														from pagamentos where fk_promotor = {$id} 
														and (status_pagamento = 3 
														or status_pagamento = 5)) is null
														,'0'
														,(select sum(valor) as saques 
														from pagamentos where fk_promotor = {$id} 
														and (status_pagamento = 3 
														or status_pagamento = 5)))
												        
													),2,'de_DE') as total

												from tbl_comissao where fk_promotor = {$id};"),

							'pagamentos'=> $this->db->query("select 
										id_pagamento as id,
										nome_status,
										id_status,
										DATE_FORMAT(data_pagamento, '%d/%m/%Y %H:%i:%s') as data_pg,
										format(valor,2,'de_DE') as total
										from pagamentos p 
										inner join status on id_status = status_pagamento
										where fk_promotor = {$id}")->result());

		}

	}