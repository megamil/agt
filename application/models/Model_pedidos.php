<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_pedidos extends CI_Model {

		public function existePedido($id_pedido = null) {

			return $this->db->query('select count(*) as quantidade from pedidos where id_pedido = '.$id_pedido);

		}

		public function listar_pedidos() {

			try {

				return $this->db->query('select id_pedido,DATE_FORMAT(data_pedido, \'%d/%m/%Y %H:%i:%s\') data, status_pedido, 						nome_cliente, cep_cliente,
										(select ((sum(preco * quantidade)) + taxa_entrega) 
										from pedidos_x_produtos where fk_pedido = id_pedido ) as total
										from pedidos 
										inner join clientes on id_cliente = fk_cliente
										where status_pedido != 2;')->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar os pedidos: '.$e;
				
			}


		}

		public function pegar_itens($pedido = null){

			try {

				return $this->db->query("select concat(nome_produto,' R$ ',preco,' Qtd. ',quantidade,'<br>') as item
										from pedidos_x_produtos 
										inner join produtos on fk_produto = id_produto
										where fk_pedido = ".$pedido)->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar os pedidos: '.$e;
				
			}


		}

		public function gerarPedido($pedido = null) {

			$this->db->insert('pedidos',$pedido);
			return $this->db->insert_id(); //Retorna o id do novo cliente.

		}

		public function add($pedido = null, $produto = null, $quantidade = null) {

			$qtd = $this->db->query('select nome_produto,quantidade_produto,preco_produto from produtos where id_produto = '.$produto);

			//Confere o saldo
			if($qtd->row()->quantidade_produto >= $quantidade){

				$jaExiste = $this->db->query('select count(*) total from pedidos_x_produtos 
					where fk_pedido = '.$pedido.' and fk_produto = '.$produto);

				//Verifica se já existe este produto no pedido
				if($jaExiste->row()->total == 0){

					//Adiciona insumo ao pedido
					$this->db->query("insert into pedidos_x_produtos (fk_pedido,fk_produto,quantidade,preco) values ({$pedido},{$produto},{$quantidade},{$qtd->row()->preco_produto});");	

					//Atualiza o saldo
					$this->db->query("update produtos set quantidade_produto = (quantidade_produto - {$quantidade}) where id_produto = {$produto};");	

					return array('status' => true);

				} else {

					return array('status' => false, 'aviso' => 'Erro, este produto já foi adicionado anteriormente.');

				}
				
				

			} else {

				return array('status' => false, 'aviso' => 'Erro, Quantidade em estoque para '.$qtd->row()->nome_produto.' é '.$qtd->row()->quantidade_produto.' e foi pedido '.$quantidade);

			}

			
		}

		public function remover($pedido = null, $produto = null) {

			$qtd = $this->db->query('select quantidade from pedidos_x_produtos where fk_produto = '.$produto.' and fk_pedido = '.$pedido.';');

			//Volta o saldo para o estoque
			$this->db->query("update produtos set quantidade_produto = (quantidade_produto + {$qtd->row()->quantidade}) where id_produto = {$produto};");

			return $this->db->query("delete from pedidos_x_produtos where fk_pedido = {$pedido} and fk_produto = {$produto};");

		}

		public function confirmarPedido($dados = null, $id_pedido = null){

			$this->db->where('id_pedido', $id_pedido);
			return $this->db->update('pedidos',$dados);

		}

		public function finalizaPedido($dados = null, $id_pedido = null){

			if (isset($dados['fk_usuario_entregador'])) {
				$finalizar_pedido = "update pedidos set taxa_entrega = {$dados['taxa_entrega']},
									fk_usuario_entregador = {$dados['fk_usuario_entregador']},
									status_pedido = 5,
									data_atendimento = current_timestamp where id_pedido = ".$id_pedido;
			} else {
				$finalizar_pedido = "update pedidos set taxa_entrega = {$dados['taxa_entrega']},
									status_pedido = 5,
									data_atendimento = current_timestamp where id_pedido = ".$id_pedido;
			}

			//Atualiza dados do pedido
			if ($this->db->query($finalizar_pedido)) {

				$promotor = 0; //Precisa já existir a partir da segunda iteração.

				$comissoes = $this->db->query('select 
					(select comissao from hist_comissao where nivel = 1 ORDER BY data_comissao DESC limit 1) as comissao1,
					(select comissao from hist_comissao where nivel = 2 ORDER BY data_comissao DESC limit 1) as comissao2,
					(select comissao from hist_comissao where nivel = 3 ORDER BY data_comissao DESC limit 1) as comissao3,
					(select comissao from hist_comissao where nivel = 4 ORDER BY data_comissao DESC limit 1) as comissao4,
					(select comissao from hist_comissao where nivel = 5 ORDER BY data_comissao DESC limit 1) as comissao5');

				$comicao[1] = $comissoes->row()->comissao1;
				$comicao[2] = $comissoes->row()->comissao2;
				$comicao[3] = $comissoes->row()->comissao3;
				$comicao[4] = $comissoes->row()->comissao4;
				$comicao[5] = $comissoes->row()->comissao5;

				//Como são 5 níveis busco o promotor até seu quinto nível.
				for ($i=0; $i < 5; $i++) { 

					if($i == 0) { //Caso seja a primeira passagem tem que verificar o FK_Promotor do cliente
						$promotor = $this->db->query('select fk_promotor_cliente as id_promotor
												from pedidos
												inner join clientes on id_cliente = fk_cliente
												where id_pedido = '.$id_pedido)->row()->id_promotor;
					} else { //Descobre quem é o promotor do promotor.
						$promotor = $this->db->query('select fk_promotor as id_promotor
													from promotores
													where id_promotor = '.$promotor)->row()->id_promotor;
					} //end else

					if($promotor != null) { //Cria uma lista de saldo para os promotores, se houverem.

						$nivel = $i+1;

						//Cria um novo pedido na tabela, com esses valores é calculado o saldo;
						$this->db->query('insert into tbl_comissao (fk_pedido,fk_promotor,nivel,comissao_atual)
							values 
								('.$id_pedido.','.$promotor.','.$nivel.','.$comicao[$nivel].')');
						
					} else { // Caso não exista um promotor acima deste.
						$i = 5;
					}

				} //end for


				
			} else { //Valida o update no pedido
				return false; // Caso falhe.
			}

		}

		public function novos_pedidos($where = null) {

			try {

				$sql = 'SELECT p.*, c.*, nome_promotor FROM pedidos p 
						inner join clientes c on id_cliente = fk_cliente
						left join promotores on id_promotor = fk_promotor_cliente
						where id_pedido = '.$where[0].';';

				$dados = array 
				(
					'pedido' => $this->db->query($sql),
				 	'entregador' => $this->db->get_where('entregador',array('status_entregador' => 1))->result(),
				 	'listaProdutos' => $this->db->get('produtos')->result(),
				 	'produtos' => $this->db->query('select p.*, quantidade from produtos p inner join pedidos_x_produtos on fk_produto = id_produto where fk_pedido = '.$where[0])->result()

				);

				return $dados;
				
			} catch (Exception $e) {

				echo 'Falha ao listar os estados: '.$e;
				
			}

		}

		//Pega os produtos do pedido e adiciona seu valor ao estoque novamente.
		public function cancelando($id_pedido = null) {

			$produtos = $this->db->get_where('pedidos_x_produtos',array('fk_pedido' => $id_pedido))->result();

			foreach ($produtos as $produto) {
				$this->db->query('update produtos set quantidade_produto = (quantidade_produto + '.$produto->quantidade.') where id_produto = '.$produto->fk_produto);
			}

			return $this->db->query('update pedidos set status_pedido = 4, data_atendimento = current_timestamp where id_pedido = '.$id_pedido);

		}


		public function editar_pedidos($where = null) {

			try {

				$sql = 'SELECT p.*, c.*, nome_promotor FROM pedidos p 
						inner join clientes c on id_cliente = fk_cliente
						left join promotores on id_promotor = fk_promotor_cliente
						where id_pedido = '.$where[0].';';

				$dados = array 
				(
					'pedido' => $this->db->query($sql),
				 	'entregador' => $this->db->get_where('entregador',array('status_entregador' => 1))->result(),
				 	'listaProdutos' => $this->db->get('produtos')->result(),
				 	'produtos' => $this->db->query('select p.*, quantidade from produtos p inner join pedidos_x_produtos on fk_produto = id_produto where fk_pedido = '.$where[0])->result()

				);

				return $dados;
				
			} catch (Exception $e) {

				echo 'Falha ao listar os estados: '.$e;
				
			}

		}

		public function filtro_pedidos() {

			try {

				$sql = 'SELECT id_cliente,
										nome_cliente,
										cpf_cliente,
										bairro_cliente,
										nome_promotor,
										cep_cliente FROM clientes
										left join promotores on id_promotor = fk_promotor_cliente;';

				return $this->db->query($sql)->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar os estados: '.$e;
				
			}

		}

		public function editar_clientes($where = null) { //Array

			try {

				return array ('cliente' =>$this->db->get_where('clientes',array ('id_cliente' => $where[0])),
							  'estados' => $this->db->get('estados')->result()
							);
				
			} catch (Exception $e) {

				echo 'Falha ao carregar grupo: '.$e;
				
			}

		}

		public function cliente_novo($cliente = null){

			try {

				$this->db->insert('clientes',$cliente);
				return $this->db->insert_id(); //Retorna o id do novo cliente.

			} catch (Exception $e) {

				echo 'Falha ao criar cliente: '.$e;

			}

		}

		public function update($cliente = null) {

			try {
				
				$this->db->where('id_cliente', $cliente['id_cliente']);
				return $this->db->update('clientes',$cliente);

			} catch (Exception $e) {

				echo 'Falha ao editar '.$e;
				return false;
				
			}	

		}

		public function listar_filtro($status = null,$cpf = null,$de = null,$ate = null) {

			$where = 'where ';
			if(!empty($status)) {
				$where .= 'status_pedido = '.$status;
			}

			if(!empty($cpf)) {
				if($where != 'where ') {$where .= ' and ';}
				$where .= "cpf_cliente = '".$cpf."'";
			}

			if(!empty($de)) {
				if($where != 'where ') {$where .= ' and ';}
				$where .= "date_format(data_pedido, '%d/%m/%Y') >= '".$de."'";
			}	

			if(!empty($ate)) {
				if($where != 'where ') {$where .= ' and ';}
				$where .= "date_format(data_pedido, '%d/%m/%Y') <= '".$ate."'";
			}

			//Caso cliquem sem realizar filtro algum.
			if($where == 'where ') {$where = ';';}

			return $this->db->query("select id_pedido,DATE_FORMAT(data_pedido, '%d/%m/%Y %h:%i:%s') data, status_pedido, 						nome_cliente, cep_cliente,
										(select ((sum(preco * quantidade)) + taxa_entrega) 
										from pedidos_x_produtos where fk_pedido = id_pedido ) as total
										from pedidos 
										inner join clientes on id_cliente = fk_cliente ".$where)->result();

		}

	}

?>