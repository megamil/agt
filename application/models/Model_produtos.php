<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_produtos extends CI_Model {

		public function listar_produtos() {

			try {

				return $this->db->get('produtos')->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar os grupos: '.$e;
				
			}


		}

		//Em caso de erro no upload
		public function del($id = null) {
			$this->db->where("id_produto",$id);
			return $this->db->delete("produtos");
		}

		public function update($dados = null) {

			try {

				$this->db->where('id_produto', $dados['id_produto']);
				return $this->db->update('produtos',$dados);

			} catch (Exception $e) {

				echo 'Falha ao editar '.$e;
				return false;
				
			}	

		}

		public function listar_filtro($nome,$preco){


			$sql = 'SELECT * FROM produtos';

			if($preco != '' && $nome != '') {

				$sql .= " where preco_produto = ".$preco." and nome_produto like '%".$nome."%';";

			} else if($preco != '') {
			
				$sql .= " where preco_produto = ".$preco.";";
			
			} else if($nome != '') {

				$sql .= " where nome_produto like '%".$nome."%';";

			}

			try {

				return $this->db->query($sql)->result();

			} catch (Exception $e) {

				echo 'Falha: '.$e;
				
			}


		}

		public function novo_produto($dados = null) {

			try {

				$this->db->insert('produtos',$dados);
				return $id = $this->db->insert_id(); //Retorna o id do novo produto.

			} catch (Exception $e) {

				echo 'Falha ao criar produto: '.$e;

			}

		}

		public function editar_produtos($where = null) { //Array

			try {

				return $this->db->get_where('produtos',array ('id_produto' => $where[0]));
				
			} catch (Exception $e) {

				echo 'Falha ao carregar grupo: '.$e;
				
			}

		}

	}

?>