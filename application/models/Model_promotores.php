<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_promotores extends CI_Model {

		public function listar_promotores() {

			try {

				if($this->session->userdata('promotor')){ //Caso o usuário logado seja um promotor
					$sql = 'SELECT id_promotor,
									nome_promotor,
									cpf_promotor,
									bairro_promotor,
									(select nome_promotor from promotores p1 where p1.id_promotor = p2.fk_promotor) nome_promotor_fk,
									cep_promotor 
									FROM promotores p2
									where p2.fk_promotor = '.$this->session->userdata('id_usuario').';';
				} else {
					$sql = 'SELECT id_promotor,
									nome_promotor,
									cpf_promotor,
									bairro_promotor,
									(select nome_promotor from promotores p1 where p1.id_promotor = p2.fk_promotor) nome_promotor_fk,
									cep_promotor 
									FROM promotores p2;';
				}

				return $this->db->query($sql)->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar os promotores: '.$e;
				
			}


		}

		public function novos_promotores() {

			try {

				return array (
							'estados' => $this->db->get('estados')->result(),
							'bancos' => $this->db->get('bancos')->result()
							);
				
			} catch (Exception $e) {

				echo 'Falha ao listar os estados: '.$e;
				
			}

		}

		public function editar_promotores($where = null) { //Array

			try {

				return array ('promotor' =>$this->db->get_where('promotores',array ('id_promotor' => $where[0])),
							  'estados' => $this->db->get('estados')->result(),
							  'bancos' => $this->db->get('bancos')->result()
							);
				
			} catch (Exception $e) {

				echo 'Falha ao carregar grupo: '.$e;
				
			}

		}

		public function promotor_novo($promotor = null){

			try {

				$this->db->insert('promotores',$promotor);
				return $this->db->insert_id(); //Retorna o id do novo promotor.

			} catch (Exception $e) {

				echo 'Falha ao criar promotor: '.$e;

			}

		}

		public function update($promotor = null) {

			try {
				
				$this->db->where('id_promotor', $promotor['id_promotor']);
				return $this->db->update('promotores',$promotor);

			} catch (Exception $e) {

				echo 'Falha ao editar '.$e;
				return false;
				
			}	

		}

		public function listar_filtro($nome,$cpf){


			$sql = 'SELECT * FROM promotores';

			if($cpf != '' && $nome != '') {

				$sql .= " where cpf_promotor = '".$cpf."' and nome_promotor like '%".$nome."%';";

			} else if($cpf != '') {
			
				$sql .= " where cpf_promotor = '".$cpf."';";
			
			} else if($nome != '') {

				$sql .= " where nome_promotor like '%".$nome."%';";

			}

			try {

				return $this->db->query($sql)->result();

			} catch (Exception $e) {

				echo 'Falha: '.$e;
				
			}


		}

		/*ALTERA O VALOR DA COMISSÃO INDIRETA.*/
		public function atualizar_comissao($comissao = null, $nivel = null) {

			//Resgata o último valor inserido.
			$comissao_atual = $this->db->query('select comissao from hist_comissao where nivel = '.$nivel.' ORDER BY data_comissao DESC limit 1');

			if($comissao_atual->num_rows() > 0) {//Caso não seja o primeiro registo
				$comissao_atual = $comissao_atual->row()->comissao;
			} else {
				$comissao_atual = 0;
			}

			if($comissao_atual != $comissao){//Atualiza registro.
				return $this->db->query('insert into hist_comissao(comissao,fk_usuario,nivel) values ('.$comissao.','.$this->session->userdata('id_usuario').','.$nivel.');');
			} else {//Somente finaliza
				return true; 
			}

		}

	}

?>