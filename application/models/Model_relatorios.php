<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_relatorios extends CI_Model {

		//Lista todos relatórios, pegando pelo nome da tela e excluíndo a tela 24, que é a propria lista e o 30 que é uma sub-view dos pagamentos.
		public function master_relatorios(){

			return $this->db->query("select * from aplicacoes inner join grupo_aplicacoes on fk_aplicacao = id_aplicacao where aplicacao like '%relatorios' and id_aplicacao not in (24,30) and fk_grupo = 1;")->result();

		}

		public function vendas_relatorios(){
			return $this->db->query("SELECT
									fk_pedido, DATE_FORMAT(data_pedido, '%d/%m/%Y %H:%i:%s') as data_pedido, nome_promotor, nome_cliente, nivel, format(comissao_atual,2,'de_DE') as comissao_atual
									from tbl_comissao c
									inner join pedidos on id_pedido = fk_pedido
									inner join clientes on id_cliente = fk_cliente
									inner join promotores p on p.id_promotor = c.fk_promotor;")->result();
		}

		public function pedidospg_relatorios(){
			return $this->db->query("SELECT 
										id_pagamento as id,
										nome_status,
										id_status,
										nome_promotor as nome,
										DATE_FORMAT(data_pagamento, '%d/%m/%Y %H:%i:%s') as data_pg,
										format(valor,2,'de_DE') as total
										from pagamentos p 
										inner join status on id_status = status_pagamento
										inner join promotores on id_promotor = p.fk_promotor order by id_status asc")->result();
		}


		######################################################################################################
		############################################## PROMOTOR ##############################################
		######################################################################################################

		public function promotor_relatorios(){

			return $this->db->query("SELECT * from aplicacoes inner join grupo_aplicacoes on fk_aplicacao = id_aplicacao where aplicacao like '%_p_relatorios' and fk_grupo = 2;")->result();

		}

		public function ganhos_p_relatorios() {

			return $this->db->query("SELECT
									fk_pedido, DATE_FORMAT(data_pedido, '%d/%m/%Y %H:%i:%s') as data_pedido, nome_cliente, nivel, format(comissao_atual,2,'de_DE') as comissao_atual
									from tbl_comissao 
									inner join pedidos on id_pedido = fk_pedido
									inner join clientes on id_cliente = fk_cliente
									where fk_promotor = {$this->session->userdata('id_usuario')};")->result();

		}

		public function trocascomissao_relatorios() {

			return $this->db->query("SELECT 
									id_hist_comissao, 
									format(comissao,2,'de_DE') as comissao,
									nivel, 
									nome, 
									DATE_FORMAT(data_comissao, '%d/%m/%Y %H:%i:%s') as data_comissao
									FROM hist_comissao
									inner join usuarios on fk_usuario = id_usuario;")->result();
			
		}

		######################################################################################################
		############################################ RELATÓRIOS ##############################################
		######################################################################################################

		public function filtro_pedidospg($status = null, $promotor = null){

			$where = "";

			if($status != ''){
				$where = ' where status_pagamento = '.$status;
			}

			if($promotor != ''){
				if($where == "") {
					$where = " where nome_promotor like '%{$promotor}%'";
				} else {
					$where .= " and nome_promotor like '%{$promotor}%'";
				}
			}

			return $this->db->query("select 
									id_pagamento as id,
									nome_status,
									id_status,
									nome_promotor as nome,
									DATE_FORMAT(data_pagamento, '%d/%m/%Y %H:%i:%s') as data_pg,
									format(valor,2,'de_DE') as total
									from pagamentos p 
									inner join status on id_status = status_pagamento
									inner join promotores on id_promotor = p.fk_promotor {$where} order by id_status asc")->result();


		}

		public function filtro_vendas($nivel = null, $promotor = null,$cliente = null,$pedido = null,$de = null,$ate = null){

			$where = "";

			if($nivel != ''){
				$where = ' where nivel = '.$nivel;
			}

			if($promotor != ''){
				if($where == "") { $where = " where "; } else { $where .= " and "; }

				$where .= " nome_promotor like '%".$promotor."%'";

			}

			if($cliente != ''){
				if($where == "") { $where = " where "; } else { $where .= " and "; }

				$where .= " nome_cliente like '%".$cliente."%'";
				
			}

			if($pedido != ''){
				if($where == "") { $where = " where "; } else { $where .= " and "; }

				$where .= " fk_pedido = ".$pedido."";
				
			}

			if($de != ''){
				if($where == "") { $where = " where "; } else { $where .= " and "; }

				$where .= " date_format(data_pedido, '%d/%m/%Y') >= '".$de."'";
				
			}

			if($ate != ''){
				if($where == "") { $where = " where "; } else { $where .= " and "; }

				$where .= " date_format(data_pedido, '%d/%m/%Y') <= '".$ate."'";
				
			}

			return $this->db->query("select
									fk_pedido, DATE_FORMAT(data_pedido, '%d/%m/%Y %H:%i:%s') as data_pedido, nome_promotor, nome_cliente, nivel, format(comissao_atual,2,'de_DE') as comissao_atual
									from tbl_comissao c
									inner join pedidos on id_pedido = fk_pedido
									inner join clientes on id_cliente = fk_cliente
									inner join promotores p on p.id_promotor = c.fk_promotor {$where};")->result();


		}


	}

?>