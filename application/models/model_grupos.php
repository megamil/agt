<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_grupos extends CI_Model {

		public function listar_grupos() {

			try {

				return $this->db->get('grupos')->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar os grupos: '.$e;
				
			}


		}

		public function criar_grupos($dados = null) {

			try {

				$this->db->insert('grupos',$dados);

				return $this->db->insert_id(); //Retorna o id do novo usuário.
				
			} catch (Exception $e) {

				echo 'Falha ao criar um grupo: '.$e;
				
			}

		}

		public function update_grupos($dados = null) {

			try {
				
				$this->db->where('id_grupo', $dados['id_grupo']);
				return $this->db->update('grupos',$dados);

			} catch (Exception $e) {

				echo 'Falha ao atualizar grupo: '.$e;
				
			}



		}	

		public function editar_grupos($where = null) { //Array

			try {

				$pack = array (

				'grupo' => $this->db->get_where('grupos',array ('id_grupo' => $where[0])),
				'aplicacoes' => $this->db->query('select A.*, GA.fk_grupo > 0 pertence
					from aplicacoes A LEFT OUTER join grupo_aplicacoes GA on GA.fk_grupo = '.$where[0].'
					and GA.fk_aplicacao = A.id_aplicacao
					order by A.id_aplicacao')->result()

				);

				return $pack;
				
			} catch (Exception $e) {

				echo 'Falha ao carregar grupo: '.$e;
				
			}

		}

		public function rem_add_aplicacao($dados = null, $condicao = null) {

			try {

				if($condicao == 'remover') {

					$this->db->where($dados);
					return $this->db->delete('grupo_aplicacoes'); 

				} else {

					return $this->db->insert('grupo_aplicacoes',$dados);

				}
				
			} catch (Exception $e) {

				echo 'Falha ao Adicionar/Remover aplicação: '.$e;
				
			}

		}

	}

?>