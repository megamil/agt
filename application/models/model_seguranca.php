<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_seguranca extends CI_Model {

		public function validar($login = null) { //Valida o login no inicio.

			try {

				if($this->session->userdata('promotor')){ //Case seja um promotor, será fixo do grupo 2
					$ativo = $this->db->query("select nome_promotor as nome, id_promotor as id_usuario, 2 as id_grupo from promotores where login_promotor = '{$login['usuario']}' and senha_promotor = '{$login['senha']}' and status_promotor = 1");

				} else {
					$ativo = $this->db->query("select nome, id_usuario, id_grupo from usuarios 
											inner join grupo_usuarios on id_usuario = fk_usuario
											inner join grupos on id_grupo = fk_grupo
											where ativo = 1 and usuario = '{$login['usuario']}' and senha = '{$login['senha']}'");
				}


				/*Se existir um usuário com essa senha*/
				if($ativo->num_rows() == 1) { return $ativo; } else { return false; }
				
			} catch (Exception $e) {

				echo 'Falha ao validar login: '.$e;
				
			}

		}

		public function inicioMaster() {
			return $this->db->get('view_inicio_master');
		}

		public function inicioPromotor() {
			return $this->db->query("select 

							(select count(*) from clientes where fk_promotor_cliente = {$this->session->userdata('id_usuario')}) as clientes,

							(select count(*) from promotores where fk_promotor = {$this->session->userdata('id_usuario')}) as promotores,

							(select 
							format((sum(comissao_atual) -
							IF(
									(select sum(valor) as saques 
									from pagamentos where fk_promotor = {$this->session->userdata('id_usuario')} 
									and (status_pagamento = 3 
									or status_pagamento = 5)) is null
									,'0'
									,(select sum(valor) as saques 
									from pagamentos where fk_promotor = {$this->session->userdata('id_usuario')} 
									and (status_pagamento = 3 
									or status_pagamento = 5)))
							        
								),2,'de_DE') as total

							from tbl_comissao where fk_promotor = {$this->session->userdata('id_usuario')}) as total");
		}


		public function acesso($tela = null) { //Valida se o usuário tem permissão de acesso a tela.

			try {

				return $this->db->query('select count(GA.fk_grupo) as permissao from grupo_aplicacoes GA 
											inner join aplicacoes A on A.id_aplicacao = GA.fk_aplicacao 
											where A.aplicacao = \''.$tela.'\'
											and GA.fk_grupo = '.$this->session->userdata('id_grupo'))->row()->permissao;
				
			} catch (Exception $e) {

				echo 'Falha ao validar acesso: '.$e;
				
			}

		}

		public function titulo($tela = null) { //Carrega o título da aplicação.

			try {
			
				return $this->db->query('select titulo_aplicacao from aplicacoes where aplicacao = \''.$tela.'\';')->row();

			} catch (Exception $e) {
				
				echo 'Falha ao consultar título: '.$e;

			}	

		}

		public function senha_Email($senha = null,$usuario = null) {

			$email = $this->db->query('select email from usuarios where usuario = \''.$usuario.'\' and ativo = 1;');

			try {

				if($email->num_rows() > 0) {

				$this->db->query('update usuarios set senha = \''.$senha.'\' where usuario = \''.$usuario.'\';');

				return $email->row()->email;

				} 
				
			} catch (Exception $e) {

				return "";
				
			}

		}

		public function senha_Email_Promotor($senha = null,$usuario = null) {

			$email = $this->db->query('select email_promotor as email from promotores where login_promotor = \''.$usuario.'\' and status_promotor = 1;');

			try {

				if($email->num_rows() > 0) {

				$this->db->query('update promotores set senha_promotor = \''.$senha.'\' where login_promotor = \''.$usuario.'\';');

				return $email->row()->email;

				} 
				
			} catch (Exception $e) {

				return "";
				
			}

		}

	}

?>