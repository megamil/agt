<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_usuarios extends CI_Model {

		public function listar_usuarios() {

			try {

				return $this->db->query('select *, nome_grupo from usuarios inner join grupo_usuarios on id_usuario = fk_usuario inner join grupos on id_grupo = fk_grupo')->result();

			} catch (Exception $e) {

				echo 'Falha ao listar usuários: '.$e;

			}

		}

		public function criar_usuarios($dados = null,$fk_grupo = null) {

			try {

				$this->db->insert('usuarios',$dados);
				$id = $this->db->insert_id();

				//Adiciona no grupo
				$this->db->query("insert into grupo_usuarios (fk_usuario,fk_grupo) values ({$id},{$fk_grupo})");

				return $id; //Retorna o id do novo usuário.

			} catch (Exception $e) {

				echo 'Falha ao criar usuário: '.$e;

			}

		}

		public function update_usuarios($dados = null) {

			try {			

				$this->db->where('id_usuario', $dados['id_usuario']);
				return $this->db->update('usuarios',$dados);

			} catch (Exception $e) {

				echo 'Falha ao editar usuário: '.$e;
				
			}

			

		}	

		public function editar_usuarios($where = null) { //Array

			try {

				$pack = array (

				'usuario' => $this->db->get_where('usuarios',array ('id_usuario' => $where[0])),
				'grupo' => $this->db->get_where('grupo_usuarios',array ('fk_usuario' => $where[0]))

				);

				return $pack;
				
			} catch (Exception $e) {

				echo 'Falha ao carregar usuário: '.$e;
				
			}

		}

		public function proprios_usuarios() {

			try {

				return $this->db->query("select *, 
					format((select comissao from hist_comissao where nivel = 1 ORDER BY data_comissao DESC limit 1),2,'de_DE') as comissao1,
					format((select comissao from hist_comissao where nivel = 2 ORDER BY data_comissao DESC limit 1),2,'de_DE') as comissao2,
					format((select comissao from hist_comissao where nivel = 3 ORDER BY data_comissao DESC limit 1),2,'de_DE') as comissao3,
					format((select comissao from hist_comissao where nivel = 4 ORDER BY data_comissao DESC limit 1),2,'de_DE') as comissao4,
					format((select comissao from hist_comissao where nivel = 5 ORDER BY data_comissao DESC limit 1),2,'de_DE') as comissao5
					from usuarios where id_usuario = {$this->session->userdata('id_usuario')}");
				
			} catch (Exception $e) {

				echo 'Falha ao carregar usuário: '.$e;
				
			}

		}

		public function rem_add_grupo($dados = null, $condicao = null) {

			try {

				if($condicao == 'remover') {

					$this->db->where($dados);
					return $this->db->delete('grupo_usuarios'); 

				} else {

					return $this->db->insert('grupo_usuarios',$dados);

				}
				
			} catch (Exception $e) {

				echo 'Falha ao Adicionar/Remover do grupo: '.$e;
				
			}

		}

	}

?>