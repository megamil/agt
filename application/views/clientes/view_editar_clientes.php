<div class="mdl-grid tituloTela">
	<div class="mdl-cell mdl-cell--12-col">
		<img src="<?php echo base_url(); ?>style/imagens/novo_cliente.png">
		<strong>Editar Cliente</strong>
	</div>
</div>

<?php if(!$this->session->userdata('promotor')) { //Somente se não for promotor pode gerar um pedido?>

<div class="mdl-grid" style="margin-top: -80px; margin-bottom: 10px;">

	<div class="mdl-cell mdl-cell--9-col">
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="<?php echo base_url(); ?>controller_pedidos/novoPedido/<?php echo $dados['cliente']->row()->id_cliente; ?>">
			<small>Cadastrar</small><br>
			NOVO PEDIDO <img src="<?php echo base_url(); ?>style/imagens/pedidos.png" width="28px">
		</a>
	</div>

</div>

<?php } ?>

<?php echo form_open('controller_clientes/editar_cliente'); ?>

<div class="mdl-grid">

	<input type="hidden" name="id_cliente" value="<?php echo $dados['cliente']->row()->id_cliente; ?>">
	<input type="hidden" name="cpfAtual" value="<?php echo $dados['cliente']->row()->cpf_cliente; ?>">
	<input type="hidden" name="telefoneAtual" value="<?php echo $dados['cliente']->row()->tel_cliente; ?>">
	<input type="hidden" name="celularAtual" value="<?php echo $dados['cliente']->row()->cel_cliente; ?>">

	 <div class="mdl-cell mdl-cell--6-col">
	 	<label class="label" for="nome_cliente">Nome</label>
	    <input type="text" class="mdl-textfield__input obrigatorio" name="nome_cliente" aviso="Nome" id="nome_cliente" size="50" maxlength="30" value="<?php echo $dados['cliente']->row()->nome_cliente; ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="cpf_cliente">CPF</label>
	    <input type="text" class="mdl-textfield__input mascara_cpf " name="cpf_cliente" aviso="CPF" id="cpf_cliente" size="50" maxlength="11" value="<?php echo $dados['cliente']->row()->cpf_cliente; ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="rg_cliente">RG</label>
	    <input type="text" class="mdl-textfield__input mascara_rg " name="rg_cliente" aviso="RG" id="rg_cliente" size="50" maxlength="11" value="<?php echo $dados['cliente']->row()->rg_cliente; ?>"/>   
	 </div>

 </div>

 <div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="tel_cliente">Tel</label>
	    <input type="text" class="mdl-textfield__input mascara_tel " name="tel_cliente" aviso="Telefone" id="tel_cliente" size="50" maxlength="11" value="<?php echo $dados['cliente']->row()->tel_cliente; ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="cel_cliente">Cel</label>
	    <input type="text" class="mdl-textfield__input mascara_cel " name="cel_cliente" aviso="Celular" id="cel_cliente" size="50" maxlength="11" value="<?php echo $dados['cliente']->row()->cel_cliente; ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--6-col" align="center" style="margin-top: 25px;">
	 <?php 

	 	if($dados['cliente']->row()->nome_promotor != '') {
	 		echo 'ADICIONADO PELO PROMOTOR: '.strtoupper($dados['cliente']->row()->nome_promotor);
	 	} else {	
	 		echo 'ADICIONADO POR UM USUÁRIO MASTER';
	 	}

	  ?>
	 </div>

 </div>

   <div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="tel2_cliente">Tel 2</label>
	    <input type="text" class="mdl-textfield__input mascara_cel " name="tel2_cliente" aviso="Telefone" id="tel2_cliente" size="50" maxlength="11" value="<?php echo $dados['cliente']->row()->tel2_cliente; ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="tel3_cliente">Cel 2</label>
	    <input type="text" class="mdl-textfield__input mascara_cel " name="tel3_cliente" aviso="Celular" id="tel3_cliente" size="50" maxlength="11" value="<?php echo $dados['cliente']->row()->tel3_cliente; ?>"/>   
	 </div>

	 <?php 

	 if ($dados['cliente']->row()->data_nascimento_cliente != '') {
			$data = explode('-',$dados['cliente']->row()->data_nascimento_cliente);
			$data_nascimento_cliente = $data[2].'/'.$data[1].'/'.$data[0];
		} else {
			$data_nascimento_cliente = '';
		}


	  ?>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="data_nascimento_cliente">ANIVERSÁRIO</label>
	    <input type="text" class="mdl-textfield__input mascara_data" name="data_nascimento_cliente" aviso="Data Nascimento" id="data_nascimento_cliente" size="50" maxlength="11" value="<?php echo $data_nascimento_cliente; ?>"/>   
	 </div>

 </div>

 <div class="mdl-grid">

	<div class="mdl-cell mdl-cell--5-col">
	 	<label class="label" for="rua_cliente">Lougradouro</label>
	    <input type="text" class="mdl-textfield__input  " name="rua_cliente" aviso="Rua" id="rua_cliente" size="50" maxlength="40" value="<?php echo $dados['cliente']->row()->rua_cliente; ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--1-col">
	 	<label class="label" for="numero_cliente">Nº</label>
	    <input type="text" class="mdl-textfield__input " name="numero_cliente" aviso="Número" id="numero_cliente" maxlength="20" size="50" value="<?php echo $dados['cliente']->row()->numero_cliente; ?>"/>   
	 </div>

	<div class="mdl-cell mdl-cell--6-col">
	 	<label class="label" for="complemento_cliente">Complemento</label>
	    <input type="text" class="mdl-textfield__input " name="complemento_cliente" aviso="Complemento" id="complemento_cliente" size="50" maxlength="100" value="<?php echo $dados['cliente']->row()->complemento_cliente; ?>"/>   
	 </div>

</div>	

<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="bairro_cliente">Bairro</label>
	    <input type="text" class="mdl-textfield__input " name="bairro_cliente" aviso="Bairro" id="bairro_cliente" maxlength="30" size="50" value="<?php echo $dados['cliente']->row()->bairro_cliente; ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="cidade_cliente">Cidade</label>
	    <input type="text" class="mdl-textfield__input " name="cidade_cliente" aviso="Cidade" id="cidade_cliente" maxlength="30" size="50" value="<?php echo $dados['cliente']->row()->cidade_cliente; ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">

		<label for="estado_cliente" class="label">UF</label>
		<select class="mdl-cell mdl-cell--12-col" name="estado_cliente" id="estado_cliente" aviso="Estado">
			<?php 
				foreach ($dados['estados'] as $estado) {
					if($dados['cliente']->row()->estado_cliente == $estado->id_estado) {
						echo '<option value="'.$estado->id_estado.'" selected>'.$estado->nome.'</option>';
					} else {
						echo '<option value="'.$estado->id_estado.'">'.$estado->nome.'</option>';
					}
				}
			 ?>
		</select>
	</div>

	<div class="mdl-cell mdl-cell--2-col">
	 	<label class="label" for="cep_cliente">CEP</label>
	    <input type="text" class="mdl-textfield__input mascara_cep " name="cep_cliente" aviso="CEP" id="cep_cliente" size="50" value="<?php echo $dados['cliente']->row()->cep_cliente; ?>"/>   
	 </div>

</div> 

 <div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--10-col">
	 	<label class="label" for="usuario">Observações</label>
	    <textarea class="mdl-textfield__input " aviso="Observação" name="obs_cliente" maxlength="50"><?php echo $dados['cliente']->row()->obs_cliente; ?></textarea>
	 </div>

	 <div class="mdl-cell mdl-cell--2-col">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar" style="margin-top: 15px;"><i class="material-icons">done</i>Editar</button>	
	</div>

</div>

<?php echo form_close(); ?>