<div class="tituloTelaComFiltro">

	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--2-col">
			<strong>Clientes</strong>
		</div> 

		<div class="mdl-cell mdl-cell--7-col"></div>
		<div class="mdl-cell mdl-cell--3-col">
			<a href="<?php echo base_url(); ?>main/redirecionar/clientes-view_novos_clientes">
				<small>Cadastrar</small><br>
				NOVO CLIENTE <img src="<?php echo base_url(); ?>style/imagens/novo.png" width="28px">
			</a>
		</div>
	</div>

	<div class="mdl-grid filtro">

		<div class="mdl-cell mdl-cell--3-col">
			Filtro <hr>

			Nome <input type="text" class="mdl-textfield__input" aviso="Nome" name="nome" id="nome" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--2-col">
			<br><hr>
			Telefone <input type="text" class="mdl-textfield__input mascara_cel" aviso="Telefone" name="tel" id="tel" size="50" maxlength="40" value="11)" />

		</div>

		<div class="mdl-cell mdl-cell--4-col">
			<br><hr>
			LOGRADOURO <input type="text" class="mdl-textfield__input" aviso="Rua" name="rua" id="rua" size="50" maxlength=""/>

		</div>

		<div class="mdl-cell mdl-cell--2-col">
			<br><hr>
			CPF <input type="text" class="mdl-textfield__input mascara_cpf" aviso="CPF" name="cpf" id="cpf" size="50" maxlength="40"/>

		</div>

		<div class="mdl-cell mdl-cell--1-col">
			<br><hr>
			<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtro"><i class="material-icons">search</i>Buscar</button>	
		</div>

	</div>

</div>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
	 	<div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" hidden id="loadSpinner"></div>
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th class="mdl-data-table__cell--non-numeric">Editar</th>
		      <th class="mdl-data-table__cell--non-numeric">Histórico</th>
		      <th class="mdl-data-table__cell--non-numeric">Nome</th>
		      <th>CPF</th>
		      <th>Adicionado Por:</th>
		      <th class="mdl-data-table__cell--non-numeric">Bairro</th>
		      <th>CEP</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados as $cliente) {
			    	echo '<tr>';

					echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/clientes-view_editar_clientes/'.$cliente->id_cliente, 'Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Editar.', 'alt' => 'Editar.')).'</td>';

					echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/pedidos-view_listar_pedidos_clientes/'.$cliente->id_cliente, 'Compras', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect alerta', 'title' => 'Compras.', 'alt' => 'Compras.')).'</td>';

					echo '<td width="40%"  class="mdl-data-table__cell--non-numeric">'.$cliente->nome_cliente.'</td>';
					echo '<td width="10%">'.$cliente->cpf_cliente.'</td>';

					if($cliente->nome_promotor == ''){
						echo '<td width="20%">Um usuário Master</td>';
					} else {
						echo '<td width="20%">'.$cliente->nome_promotor.'</td>';
					}
					
					echo '<td width="20%" class="mdl-data-table__cell--non-numeric">'.$cliente->bairro_cliente.'</td>';
					echo '<td width="10%">'.$cliente->cep_cliente.'</td>';

					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#loadSpinner').hide();

		$('#filtro').click(function(){

			$('#loadSpinner').show();
			var tel = '';

			if ($('#tel').val() != '(11)') {
				tel = $('#tel').val();
			}

			$('#load').load('<?php echo base_url(); ?>controller_clientes/lista_filtro',{
				nome:$('#nome').val(),
				cpf:$('#cpf').val(),
				rua:$('#rua').val(),
				tel:tel},
				function(){

					$('#loadSpinner').hide();


			});

		});

	});
</script>
