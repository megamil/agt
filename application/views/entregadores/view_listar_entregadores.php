<div class="tituloTelaComFiltro">

	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--2-col">
			<strong>Entregadores</strong>
		</div> 

		<div class="mdl-cell mdl-cell--7-col"></div>
		<div class="mdl-cell mdl-cell--3-col">
			<a href="<?php echo base_url(); ?>main/redirecionar/entregadores-view_novos_entregadores">
				<small>Cadastrar</small><br>
				NOVO ENTREGADOR <img src="<?php echo base_url(); ?>style/imagens/novo.png" width="28px">
			</a>
		</div>
	</div>

	<div class="mdl-grid filtro">

		<div class="mdl-cell mdl-cell--5-col">
			Filtro <hr>

			Nome <input type="text" class="mdl-textfield__input" aviso="Nome" name="nome" id="nome" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--5-col">
			<br><hr>
			Status <select class="mdl-cell mdl-cell--12-col" name="status" id="status" aviso="Status">
				<option value="">Todos</option>
				<option value="1">Ativo</option>
				<option value="2">Inativo</option>
			</select>

		</div>

		<div class="mdl-cell mdl-cell--1-col">
			<br><hr>
			<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtro"><i class="material-icons">search</i>Buscar</button>	
		</div>

	</div>

</div>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
	 	<div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" hidden id="loadSpinner"></div>
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th class="mdl-data-table__cell--non-numeric">Editar</th>
		      <th class="mdl-data-table__cell--non-numeric">Nome</th>
		      <th class="mdl-data-table__cell--non-numeric">Status</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados as $entregadores) {
			    	echo '<tr>';

					echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/entregadores-view_editar_entregadores/'.$entregadores->id_entregador, 'Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Editar.', 'alt' => 'Editar.')).'</td>';

					echo '<td width="50%" class="mdl-data-table__cell--non-numeric">'.$entregadores->nome_entregador.'</td>';
					if($entregadores->status_entregador == 1){
						echo '<td width="10%" class="mdl-data-table__cell--non-numeric">Ativo</td>';	
					} else {
						echo '<td width="10%" class="mdl-data-table__cell--non-numeric">Inativo</td>';	
					}
					
					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#loadSpinner').hide();

		$('#filtro').click(function(){

			$('#loadSpinner').show();

			$('#load').load('<?php echo base_url(); ?>controller_entregadores/lista_filtro',{
				nome:$('#nome').val(),
				status:$('#status').val()},
				function(){

					$('#loadSpinner').hide();


			});

		});

	});
</script>
