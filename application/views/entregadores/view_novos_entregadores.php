<div class="mdl-grid tituloTela">
	<div class="mdl-cell mdl-cell--12-col">
		<img src="<?php echo base_url(); ?>style/imagens/novo.png">
		<strong>Novo Entregador</strong>
	</div>
</div>

<?php echo form_open('controller_entregadores/novo_entregador'); ?>

<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--6-col">
	 	<label class="label" for="nome_entregador">Nome</label>
	    <input type="text" class="mdl-textfield__input obrigatorio" name="nome_entregador" aviso="Nome" id="nome_entregador" size="50" maxlength="30" value="<?php echo $this->session->flashdata('nome_entregador'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--2-col">
	 	<label for="status_entregador" class="label">Status</label>
		<select class="mdl-cell mdl-cell--12-col" name="status_entregador" id="status_entregador" aviso="Estado">
			<?php 
					if($this->session->flashdata('status_entregador') == 1) {
						echo '<option value="1" checked>Ativo</option>';
						echo '<option value="2">Inativo</option>';
					} else {
						echo '<option value="1">Ativo</option>';
						echo '<option value="2" checked>Inativo</option>';
					}
			 ?>
		</select>
	 </div>

	 <div class="mdl-cell mdl-cell--2-col">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar" style="margin-top: 15px;"><i class="material-icons">done</i>Cadastrar</button>	
	</div>

</div>

<?php echo form_close(); ?>