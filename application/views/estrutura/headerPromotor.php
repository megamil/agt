<!DOCTYPE html>
<html>

	<head>

		<meta charset="UTF-8">	
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $tela; ?></title>

		<!--ICON-->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>style/imagens/icon_user.png" type="image/x-icon">

		<!--CSS-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/mdl/mdl.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/mdl/material.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/DataTables/datatables.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/estilo.css">

		<!--JS-->
		<script type="text/javascript" src="<?php echo base_url(); ?>style/mdl/material.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/maskedinput.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/script.js"></script>

		<!-- DataTables -->
		<script type="text/javascript" src="<?php echo base_url(); ?>style/DataTables/DataTables/js/jquery.dataTables.js"></script>	
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/datatable.js"></script>
		
	</head>

	<body>

	<noscript>
		<div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
		HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.
			<iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

		</div>
	</noscript> 

			<div class="mdl-layout__content">

			<?php if($aviso['tipo'] != ''){ /*Confirma a existencia de aviso para esta tela.*/ 

			switch ($aviso['tipo']) { /*Define o icone de acordo com o tipo do erro*/
				case 'erro':
					$icon = 'error';
					break;

				case 'aviso':
					$icon = 'warning';
					break;
							
				default:
					$icon = 'check';
					break;
			}

			echo '<div class="'.$aviso['tipo'].'-card-wide mdl-card mdl-shadow--3dp card_aviso">
				  <div class="mdl-card__title">
				    <h2 class="mdl-card__title-text"><i class="material-icons">'.$icon.'</i>'.$aviso['titulo'].'</h2>
				  </div>
				  <div class="mdl-card__supporting-text">
				    '.$aviso['mensagem'].'
				  </div>
				  <div class="mdl-card__menu">
				    <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" id="fechar_aviso">
				      <i class="material-icons">clear</i>
				    </button>
				  </div>
				</div>';

			} ?>

				<!-- No header, and the drawer stays open on larger screens (fixed drawer). -->
				<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer">
				  <div class="mdl-layout__drawer" style="background-color: white !important;" align="center">
				    <span class="mdl-layout-title">
				    	<img src="<?php echo base_url(); ?>style/imagens/logo-colorido.jpg" width="100%" style="margin-left: -20px;">
				    </span>

				    <span style="font-size: 12px; margin-top: 50px; margin-bottom: 50px;" class="textoEscuro"><b class="textoEscuro">
				    	<?php echo $this->session->userdata('usuario');?></b>, Navegue pelas opções.
				    </span>

				    <?php 

				    	$selecionadoHome = '';
				    	$selecionadoClientes = '';
				    	$selecionadoPromotor = '';
				    	$selecionadoRelatorios = '';
				    	$selecionadoPagamentos = '';
				    	$selecionadoConfig = '';


				    	$url = explode('-', $this->uri->segment(3));


				    	switch ($url[0]) {

				    		case 'clientes':
				    			$selecionadoClientes = 'selecionado';
				    			break;

				    		case 'relatorios':
				    			$selecionadoRelatorios = 'selecionado';
				    			break;

							case 'promotores':
								if($this->uri->segment(3).'/'.$this->uri->segment(4) 
									== 
									'promotores-view_editar_promotores/'.$this->session->userdata('id_usuario'))
									{//Exceção, essa tela também é de configuração.
									$selecionadoConfig = 'selecionado';
								} else {
									$selecionadoPromotor = 'selecionado';
								}
				    			
				    			break;

				    		case 'saques':
				    			$selecionadoPagamentos = 'selecionado';
				    			break;

				    		case 'seguranca':
				    			$selecionadoConfig = 'selecionado';
				    			break;
				    		
				    		default:
				    			$selecionadoHome = 'selecionado';
				    			break;
				    	}


				    ?>

				    <a href="<?php echo base_url(); ?>"><div class="menuLateral <?php echo $selecionadoHome; ?>">HOME</div></a>
				    
				    <a href="<?php echo base_url(); ?>main/redirecionar/clientes-view_listar_clientes">
				    <div class="menuLateral <?php echo $selecionadoClientes; ?>">CLIENTES</div></a>

				    <a href="<?php echo base_url(); ?>main/redirecionar/promotores-view_listar_promotores"><div class="menuLateral <?php echo $selecionadoPromotor; ?>">PROMOTORES</div></a>

				    <a href="<?php echo base_url(); ?>main/redirecionar/saques-view_saque_pagamentos"><div class="menuLateral <?php echo $selecionadoPagamentos; ?>">SAQUE</div></a>
				    
				    <a href="<?php echo base_url(); ?>main/redirecionar/relatorios-view_promotor_relatorios"><div class="menuLateral <?php echo $selecionadoRelatorios; ?>">RELATÓRIOS</div></a>

				    <a href="<?php echo base_url(); ?>main/redirecionar/promotores-view_editar_promotores/<?php echo $this->session->userdata('id_usuario'); ?>">
					    <div class="menuLateral <?php echo $selecionadoConfig; ?>">
					    	<img src="<?php echo base_url(); ?>style/imagens/config.png" width="15%">
					    </div>
				    </a>

				    
				    <span id="rodapeMenu">
				    	<a href="<?php echo base_url(); ?>main/Sair" style="color: white;">
					    	<img src="<?php echo base_url(); ?>style/imagens/pass-white.png" width="15px">
					    	Sair
					    </a>
				    </span>

				  </div>
				  <main class="mdl-layout__content">
				    <div class="page-content">
				    <div id="aviso_de_erro" align="center"></div>