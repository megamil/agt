<style type="text/css">
.mdl-layout__content {
  background-image: url(<?php echo base_url() ?>style/imagens/background-main.jpg) !important;
  background-repeat: no-repeat !important;
  background-size: 100% 100% !important;
}

form {
	background: none !important;
	color: white !important;
}

</style>

<div class="mdl-grid" style="border-bottom-style: solid; border-bottom-width: 2px; border-bottom-color: white;">
	<div class="mdl-cell mdl-cell--12-col dadosTitulo" style="margin-top: 80px; color: white;" align="right">
		Clientes Cadastrados: &nbsp; <b><?php echo $dados->row()->clientes; ?></b> &nbsp; Promotores Cadastrados: &nbsp; <b><?php echo $dados->row()->promotores; ?></b> &nbsp; Pedidos <small>(30 Dias):</small> &nbsp; <b><?php echo $dados->row()->ultimospedidos; ?></b> &nbsp;	Pedidos em aberto: &nbsp; <b><?php echo $dados->row()->pedidosabertos; ?></b>
	</div>
</div>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--1-col"></div>

	<div class="mdl-cell mdl-cell--11-col" style="margin-top: 30px; margin-left: -17px; color: white; font-size: 25px;">
		Seja bem-vindo, <b><?php echo strtoupper($this->session->userdata('usuario')); ?></b>
	</div>

</div>

<?php echo form_open('controller_pedidos/buscaRapida'); ?>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--8-col">

		<div class="mdl-grid">
			
			<div class="mdl-cell mdl-cell--4-col" align="center">
				<a href="<?php echo base_url(); ?>main/redirecionar/pedidos-view_filtro_pedidos"><div class="divFundoBranco">
					<img class="img" src="<?php echo base_url(); ?>style/imagens/pedidos.png">
				</div></a>
				NOVO PEDIDO
			</div>

			<div class="mdl-cell mdl-cell--4-col" align="center">
				<a href="<?php echo base_url(); ?>main/redirecionar/clientes-view_listar_clientes"><div class="divFundoBranco">
					<img class="img" src="<?php echo base_url(); ?>style/imagens/novo_cliente.png">
				</div></a>
				CLIENTES
			</div>

			<div class="mdl-cell mdl-cell--4-col" align="center">
				<a href="<?php echo base_url(); ?>main/redirecionar/produtos-view_listar_produtos"><div class="divFundoBranco">
					<img class="img" src="<?php echo base_url(); ?>style/imagens/produtos.png">
				</div></a>
				PRODUTOS
			</div>

		</div>

		<div class="mdl-grid">


			<div class="mdl-cell mdl-cell--4-col" align="center">
				<a href="<?php echo base_url(); ?>main/redirecionar/promotores-view_listar_promotores"><div class="divFundoBranco">
					<img class="img" src="<?php echo base_url(); ?>style/imagens/novo.png">
				</div></a>
				PROMOTORES
			</div>

			<div class="mdl-cell mdl-cell--4-col" align="center">
				<a href="<?php echo base_url(); ?>main/redirecionar/entregadores-view_listar_entregadores"><div class="divFundoBranco">
					<img class="img" src="<?php echo base_url(); ?>style/imagens/novo.png">
				</div></a>
				ENTREGADORES
			</div>

			<div class="mdl-cell mdl-cell--4-col" align="center">
				<a href="<?php echo base_url(); ?>main/redirecionar/relatorios-view_master_relatorios"><div class="divFundoBranco">
					<img class="img" src="<?php echo base_url(); ?>style/imagens/relatorios.png">
				</div></a>
				RELATÓRIOS
			</div>

		</div>

		<div class="mdl-grid">

			<div class="mdl-cell mdl-cell--4-col" align="center">
				<a href="<?php echo base_url(); ?>main/redirecionar/seguranca-view_listar_usuarios"><div class="divFundoBranco">
					<img class="img" src="<?php echo base_url(); ?>style/imagens/novo.png">
				</div></a>
				USUARIOS
			</div>

			<div class="mdl-cell mdl-cell--4-col" align="center">
				<a href="<?php echo base_url(); ?>main/redirecionar/seguranca-view_proprios_usuarios">
					<div class="divFundoBranco">
						<img class="img" src="<?php echo base_url(); ?>style/imagens/config-verde.png">
					</div>
				</a>
				CONFIGURAÇÕES
			</div>

		</div>
		
	</div>

	<div class="mdl-cell mdl-cell--4-col" style="padding-top: 100px;">
		Procurar Pedido:<br>
		<input class="mdl-textfield__input validar_numeros busca_rapida" type="text" name="id_pedido" id="id_pedido" style="background-color: white;" />
	</div>

</div>
<?php echo form_close(); ?>