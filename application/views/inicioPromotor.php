<style type="text/css">
.mdl-layout__content {
  background-image: url(<?php echo base_url() ?>style/imagens/background-main.jpg) !important;
  background-repeat: no-repeat !important;
  background-size: 100% 100% !important;
  color: white !important;
}

</style>
<div class="mdl-grid" style="border-bottom-style: solid; border-bottom-width: 2px; border-bottom-color: white;">
	<div class="mdl-cell mdl-cell--12-col dadosTitulo" style="margin-top: 80px; color: white;" align="right">
		Clientes Cadastrados: &nbsp; <b><?php echo $dados->row()->clientes ?></b> &nbsp; Promotores Cadastrados: &nbsp; <b><?php echo $dados->row()->promotores ?></b> &nbsp; Saldo Disponível: <b>R$ <?php echo $dados->row()->total; ?></b>
	</div>
</div>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--12-col" style="margin-top: 30px; color: white; font-size: 25px;">
		Seja bem-vindo, <b><?php echo strtoupper($this->session->userdata('usuario')); ?></b>
	</div>

</div>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--3-col" align="center">
		<a href="<?php echo base_url(); ?>main/redirecionar/clientes-view_listar_clientes"><div class="divFundoBranco">
			<img class="img" src="<?php echo base_url(); ?>style/imagens/novo_cliente.png">
		</div></a>
		CLIENTES
	</div>

	<div class="mdl-cell mdl-cell--3-col" align="center">
		<a href="<?php echo base_url(); ?>main/redirecionar/promotores-view_listar_promotores"><div class="divFundoBranco">
			<img class="img" src="<?php echo base_url(); ?>style/imagens/novo.png">
		</div></a>
		PROMOTORES
	</div>

	<div class="mdl-cell mdl-cell--3-col" align="center">
		<a href="<?php echo base_url(); ?>main/redirecionar/relatorios-view_promotor_relatorios"><div class="divFundoBranco">
			<img class="img" src="<?php echo base_url(); ?>style/imagens/relatorios.png">
		</div></a>
		RELATÓRIOS
	</div>

	<div class="mdl-cell mdl-cell--3-col" align="center">
		<a href="<?php echo base_url(); ?>main/redirecionar/promotores-view_editar_promotores/<?php echo $this->session->userdata('id_usuario'); ?>">
			<div class="divFundoBranco">
				<img class="img" src="<?php echo base_url(); ?>style/imagens/config-verde.png">
			</div>
		</a>
		CONFIGURAÇÕES
	</div>

	<div class="mdl-cell mdl-cell--3-col" align="center">
		<a href="<?php echo base_url(); ?>main/redirecionar/saques-view_saque_pagamentos">
			<div class="divFundoBranco">
				<img class="img" src="<?php echo base_url(); ?>style/imagens/money.png">
			</div>
		</a>
		SAQUE
	</div>

</div>