<div class="mdl-grid tituloTela">
	<div class="mdl-cell mdl-cell--12-col">
		<img src="<?php echo base_url(); ?>style/imagens/pedidos.png">
		<?php 

		$cliente = $dados['pedido']->row();

		$disabled = '';
		//O pedido está aguardando?
		if($cliente->status_pedido == 3) { ?>

			<strong>Editando Pedido: <?php echo $cliente->id_pedido; ?></strong>
		<?php 
			//Pedido cancelado
			} else if($cliente->status_pedido == 4) { 
			//Já irá desabilitar alguns campos
			$disabled = 'disabled';
		?>

			<strong>Pedido: <?php echo $dados['pedido']->row()->id_pedido; ?> Cancelado</strong>

		<?php } else if($cliente->status_pedido >= 5) {  
			//Já irá desabilitar alguns campos
			$disabled = 'disabled';?>

			<strong>Pedido: <?php echo $dados['pedido']->row()->id_pedido; ?> Finalizado</strong>

		<?php } ?>

	</div>
</div>

<?php echo form_open('controller_pedidos/confirmar_pedido'); ?>

<div class="mdl-grid">

	<input type="hidden" name="id_pedido" id="id_pedido" value="<?php echo $dados['pedido']->row()->id_pedido; ?>">

	<div class="mdl-cell mdl-cell--12-col dadosTitulo" align="left">

	<?php 

		echo '<strong>'.$cliente->nome_cliente.'</strong>';
		echo '<br>'.$cliente->rua_cliente.', '.$cliente->numero_cliente;
		echo '<br>'.$cliente->bairro_cliente.' - '.$cliente->cidade_cliente;
		echo '<br>CPF: '.$cliente->cpf_cliente;

		echo '<br><i class="material-icons">&#xE0CD;</i>Tel.: '.$cliente->tel_cliente.',<i class="material-icons">&#xE0CD;</i> Tel.(2): '.$cliente->cel_cliente;

		echo '<br><i class="material-icons">&#xE0CD;</i>Cel.: '.$cliente->tel2_cliente.',<i class="material-icons">&#xE0CD;</i> Cel.(2): '.$cliente->tel3_cliente;

		echo '<br><small><a href="'.base_url().'main/redirecionar/clientes-view_editar_clientes/'.$cliente->id_cliente.'" target="_blank"><i class="material-icons">&#xE89E;</i></a></small>'; 

		echo '<br>Promotor: '.$cliente->nome_promotor;

	?>

		<?php if($disabled != 'disabled') { ?>
	<div class="mdl-cell mdl-cell--2-col">

		<button type="button" id="show-action" style="margin-top: 15px;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
    		<i class="material-icons">&#xE854;</i>Produto
		</button>	

	</div>
	<?php } ?>

	</div>

</div>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--12-col">

		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" width="100%" id="lista_produtos">
			<thead>
			<tr>
			  <th class="mdl-data-table__cell--non-numeric">Imagem</th>
			  <th>Quantidade</th>
			  <th class="mdl-data-table__cell--non-numeric">Produto</th>
			  <th>Valor</th>
			  <?php if($disabled != 'disabled') { ?>
			  	<th class="mdl-data-table__cell--non-numeric"></th>
			  <?php } ?>
			  
			</tr>
			</thead>
			<tbody>
			    <?php 

			    foreach ($dados['produtos'] as $produto) {
			    	echo '<tr>';

					echo '<td width="20%" class="mdl-data-table__cell--non-numeric"><a href="'.base_url().'produtos/'.$produto->url_produto.'" target="_blank" title="Click para abrir"><img src="'.base_url().'produtos/'.$produto->url_produto.'" width="120px"></a></td>';

					echo '<td width="10%">'.$produto->quantidade.'</td>';

					echo '<td width="50%"  class="mdl-data-table__cell--non-numeric">'.$produto->nome_produto.'</td>';
					echo '<td width="10%">R$ '.str_replace('.', ',',$produto->preco_produto).'</td>';

					if($disabled != 'disabled') {

						echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">
							<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect remover" title="Remover" style="background-color: red !important;" id_pedido="'.$dados['pedido']->row()->id_pedido.'" id_produto="'.$produto->id_produto.'" type="button"> <i class="material-icons">&#xE5C9;</i>
							</button>
						</td>';

					}

					echo '</tr>';
				} ?>
			</tbody>
		</table>

	</div>

</div>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--4-col">
	 	<label for="fk_entregador" class="label">Entregador</label>
		<select class="mdl-cell mdl-cell--12-col" name="fk_entregador" id="fk_entregador" aviso="Entregador" <?php echo $disabled ?>>
			<option value="0">Selecione...</option>
			<?php 
				foreach ($dados['entregador'] as $entregador) {

					if($entregador->id_entregador == $dados['pedido']->row()->fk_usuario_entregador) {
						echo '<option value="'.$entregador->id_entregador.'" selected>'.$entregador->nome_entregador.'</option>';
					} else {
						echo '<option value="'.$entregador->id_entregador.'">'.$entregador->nome_entregador.'</option>';
					}

				}
			 ?>
		</select>
	 </div>

	 <div class="mdl-cell mdl-cell--2-col">
	 	<label class="label" for="taxa_entrega">Taxa de Entrega</label>
	    <input type="number" class="mdl-textfield__input validar_deciamais obrigatorio" name="taxa_entrega" aviso="Taxa" id="taxa_entrega" size="50" maxlength="30" value="<?php if($dados['pedido']->row()->taxa_entrega != 0){ echo $dados['pedido']->row()->taxa_entrega; } else { echo '0.00'; } ?>" <?php echo $disabled; ?>/>   
	 </div>

	<?php echo '<input type="hidden" id="qtd_produtos" value="'.count($dados['produtos']).'">'; ?>

</div>

<div class="mdl-grid">

	<!-- Pedido não cancelado. -->
	<?php if($cliente->status_pedido == 3) { ?>

	<div class="mdl-cell mdl-cell--2-col">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect falha" id="cancelar" type="button" style="margin-top: 15px;"><i class="material-icons">&#xE5C9;</i>Cancelar Pedido</button>	
	</div>

	 <div class="mdl-cell mdl-cell--2-col">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent sucesso" id="editar" style="margin-top: 15px;" title="Somente Atualiza"><i class="material-icons">&#xE254;</i>Editar pedido</button>	
	</div>

	<div class="mdl-cell mdl-cell--6-col">
	</div>

	<div class="mdl-cell mdl-cell--2-col">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent sucesso" id="finalizar" style="margin-top: 15px;" type="button" title="Finaliza o pedido"><i class="material-icons">done</i>Finalizar pedido</button>	
	</div>

	<?php } else { ?>

		<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent sucesso" type="button" href="<?php echo base_url(); ?>main/redirecionar/pedidos-view_listar_pedidos">Voltar</a>

	<?php } ?>

</div>

<?php echo form_close(); ?>

<script type="text/javascript">

			<?php 
				$option = '<option>Selecione...</option>';
				foreach ($dados['listaProdutos'] as $produto) {
					$option .= '<option value="'.$produto->id_produto.'">'.$produto->nome_produto.' - R$'.str_replace('.', ',', $produto->preco_produto).'</option>';
				}
			 ?>

		   $('#show-action').click(function () {
		        showDialog({
		            title: '<span class="textoEscuro" align="center">Adicionar produtos:</span>',
		            /*</p> para ajustar um bug do modal, que abre um <p> antes do text*/
		             text: '<div class="mdl-grid" width="100%"><div class="mdl-cell mdl-cell--5-col"><select name="produtosLista" id="produtosLista"><?php echo $option; ?></select></div><div class="mdl-cell mdl-cell--2-col"></div><div class="mdl-cell mdl-cell--5-col"><input name="quantidadeProduto" id="quantidadeProduto" placeholder="Quantidade" type="number" class="validar_deciamais" value="1"></div></div>',
		            negative: {
		                title: 'Cancelar'
		            },
		            positive: {
		                title: 'Adicionar',
		                onClick: function (e) {

		                	if($('#quantidadeProduto').val() != ''){

		                		$.ajax({
				                dataType: 'json',
				                url: '<?php echo base_url(); ?>controller_pedidos/addProduto',
				                type: 'post',
				                async: false,
				                data: { 
				                pedido: $('#id_pedido').val(), 
				                quantidade: $('#quantidadeProduto').val(), 
				                taxa_entrega: $('#taxa_entrega').val(), 
								fk_entregador: $('#fk_entregador').val(), 
				                produto: $('#produtosLista').val()},

				                success: function(data) {
				                     
				                    location.reload();

				                },
				                error:function(){

				                    alert('Falha ao inserir');

				                }
				    
				            });
		                
		                } else {

		                	alert('Digite uma quantidade');

		                }

		                    
		                }

		            }
		        });
	    });

	   $('#cancelar').click(function () {
	        showDialog({
	            title: '<span class="textoEscuro" align="center">Cancelar Pedidos?</span>',
	            text: '<div class="mdl-grid" width="100%"><div class="mdl-cell mdl-cell--12-col textoEscuro">Deseja realmente cancelar pedido? a reserva será apagada e os produtos serão retornados ao estoque.</div></div>',
	            negative: {
	                title: 'Sim',
	                onClick: function (e) {

	                	 $.ajax({
				                dataType: 'json',
				                url: '<?php echo base_url(); ?>controller_pedidos/cancelarPedido',
				                type: 'post',
				                async: false,
				                data: { 
				                pedido: $('#id_pedido').val()
				            	},

				                success: function(data) {
				                     
				                    location.reload();

				                },
				                error:function(){

				                    alert('Falha ao cancelar');

				                }
				    
				            });

	                }
	            },
	            positive: {
	                title: 'Não'
	            }
	        });
   		});

		 $('.remover').click(function(){
		 	$.ajax({
                dataType: 'json',
                url: '<?php echo base_url(); ?>controller_pedidos/removerProduto',
                type: 'post',
                async: false,
                data: { 
                pedido: $(this).attr('id_pedido'), 
                taxa_entrega: $('#taxa_entrega').val(), 
				fk_entregador: $('#fk_entregador').val(), 
                produto: $(this).attr('id_produto')},

                success: function(data) {
                     
                    location.reload();

                },
                error:function(){

                    alert('Falha ao inserir');

                }
    
            });
		 });

		 $('#finalizar').click(function(){

		 	if($("#qtd_produtos").val() == 0){

		 		alert('Adicione pelo menos um produto ao pedido');

		 	} else {

		 		$.ajax({
	                dataType: 'json',
	                url: '<?php echo base_url(); ?>controller_pedidos/finalizarPedido',
	                type: 'post',
	                async: false,
	                data: { 
	                pedido: $('#id_pedido').val(), 
	                taxa_entrega: $('#taxa_entrega').val(), 
					fk_entregador: $('#fk_entregador').val()
					},

	                success: function(data) {
	                     
	                    location.reload();

	                },
	                error:function(){

	                    alert('Falha ao finalizar');

	                }
	    
	            });

		 	}


		 });

</script>



