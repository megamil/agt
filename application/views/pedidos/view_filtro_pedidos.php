

<div class="tituloTelaComFiltro" style="margin-top: 50px">

	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--5-col">
			<strong>novo pedido/ localizar cliente</strong>
		</div> 

		<div class="mdl-cell mdl-cell--4-col"></div>
		<div class="mdl-cell mdl-cell--3-col">
			<a href="<?php echo base_url(); ?>main/redirecionar/clientes-view_novos_clientes">
				<small>Cadastrar</small><br>
				NOVO CLIENTE <img src="<?php echo base_url(); ?>style/imagens/novo.png" width="28px">
			</a>
		</div>
	</div>

	<?php echo form_open('controller_pedidos/buscar_cliente'); ?>

	<div class="mdl-grid filtro" style="margin-top: 100px">
		
		<div class="mdl-cell mdl-cell--3-col">
			Filtro <hr>

			CPF <input type="text" class="mdl-textfield__input mascara_cpf" aviso="Preço" name="cpf" id="cpf" size="50" maxlength="40"/>

		</div>

		<div class="mdl-cell mdl-cell--3-col">
			<br><hr>
			Telefone <input type="text" class="mdl-textfield__input mascara_cel" aviso="Telefone" name="tel" id="tel" size="50" maxlength="40" value="11)" />

		</div>

		<div class="mdl-cell mdl-cell--4-col">
			<br><hr>
			LOGRADOURO <input type="text" class="mdl-textfield__input" aviso="Rua" name="rua" id="rua" size="50"/>

		</div>

		<div class="mdl-cell mdl-cell--2-col">
			<br><hr>
			<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtro"><i class="material-icons">search</i>Buscar</button>	
		</div>

	</div>
	<?php echo form_close(); ?>

</div>

<?php if (isset($_GET['avisoCliente']))  { ?>

<div id="avisoCliente" align="center" style="margin-top: 50px;">

	<!-- <a href="<?php //echo base_url(); ?>main/redirecionar/clientes-view_novos_clientes">
		<strong>Cliente não localizado, clique aqui para adicioná-lo antes de criar o pedido.</strong>
	</a> -->

	<?php header("Location: ".base_url()."main/redirecionar/clientes-view_novos_clientes/pedido"); ?>
	
</div>

<?php } ?>