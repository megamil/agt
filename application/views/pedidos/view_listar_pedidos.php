<div class="tituloTelaComFiltro">

	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--2-col">
			<strong>Pedidos</strong>
		</div> 

		<div class="mdl-cell mdl-cell--7-col"></div>
		<div class="mdl-cell mdl-cell--3-col">
			<a href="<?php echo base_url(); ?>main/redirecionar/pedidos-view_filtro_pedidos">
				<small>Cadastrar</small><br>
				NOVO PEDIDO <img src="<?php echo base_url(); ?>style/imagens/novo.png" width="28px">
			</a>
		</div>
	</div>

	<div class="mdl-grid filtro">

		 <div class="mdl-cell mdl-cell--2-col">
			 Filtro <hr>

			Status <select class="mdl-cell mdl-cell--12-col" name="status_pedido" id="status_pedido" aviso="Status">
				<option value="">Todos</option>
				<option value="3">Aguardando</option>
				<option value="5">Finalizado</option>
				<option value="4">Cancelado</option>
				<option value="2">inativos</option>
			</select>
			
		</div>

		<div class="mdl-cell mdl-cell--2-col">			
			<br><hr>
			De <input type="text" class="mdl-textfield__input mascara_data" aviso="de" name="de" id="de" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--2-col">			
			<br><hr>
			Até <input type="text" class="mdl-textfield__input mascara_data" aviso="até" name="ate" id="ate" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--4-col">
			<br><hr>
			CPF Cliente<input type="text" class="mdl-textfield__input mascara_cpf" aviso="CPF" name="cpf" id="cpf" size="50" maxlength="40"/>

		</div>

		<div class="mdl-cell mdl-cell--1-col">
			<br><hr>
			<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtro"><i class="material-icons">search</i>Buscar</button>	
		</div>

	</div>

</div>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
	 	<div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" hidden id="loadSpinner"></div>
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th>Atender</th>
		      <th class="mdl-data-table__cell--non-numeric">Status</th>
		      <th class="mdl-data-table__cell--non-numeric">Cliente</th>
		      <th>Data</th>
		      <th>CEP</th>
		      <th>Total R$:</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados as $pedido) {
			    	echo '<tr>';

			    	if($pedido->status_pedido == 3){
			    		echo '<td width="10%">'.anchor('main/redirecionar/pedidos-view_editar_pedidos/'.$pedido->id_pedido, 'Atender', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Atender.', 'alt' => 'Atender.')).'</td>';
			    	} else {
			    		echo '<td width="10%">'.anchor('main/redirecionar/pedidos-view_editar_pedidos/'.$pedido->id_pedido, 'Ver', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Ver.', 'alt' => 'Ver.')).'</td>';
			    	}
					

					switch ($pedido->status_pedido) {
			    		case '5': //Concluído
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #00B848;" class="corStatus" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    		case '6': //Concluído
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #00B848;" class="corStatus" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    		case '7': //Concluído
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #00B848;" class="corStatus" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    		case '8': //Concluído
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #00B848;" class="corStatus" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;

						case '4': //Cancelado
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #E01329;" class="corStatus" status="'.$pedido->status_pedido.'" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    		
			    		case '3': //Aguardando
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #F4CC22;" class="corStatus" status="'.$pedido->status_pedido.'" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;

			    		default:
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #E7E6E5;" class="corStatus" status="'.$pedido->status_pedido.'" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    	}

					echo '<td width="50%"  class="mdl-data-table__cell--non-numeric">'.$pedido->nome_cliente.'</td>';
					echo '<td width="10%">'.$pedido->data.'</td>';
					echo '<td width="10%">'.$pedido->cep_cliente.'</td>';
					echo '<td width="10%">R$ '.$pedido->total.'</td>';

					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--2-col" align="center"><div style="background-color: #00B848; color:black;">Aprovado</div></div>
	<div class="mdl-cell mdl-cell--2-col" align="center"><div style="background-color: #E01329; color:black;">Reprovado</div></div>
	<div class="mdl-cell mdl-cell--3-col" align="center"><div style="background-color: #F4CC22; color:black;">Aguardando Análise</div></div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#loadSpinner').hide();

		$('#filtro').click(function(){

			$('#loadSpinner').show();

			$('#load').load('<?php echo base_url(); ?>controller_pedidos/lista_filtro',{
				status:$('#status_pedido').val(),
				de:$('#de').val(),
				ate:$('#ate').val(),
				cpf:$('#cpf').val()},
				function(){

					$('#loadSpinner').hide();


			});

		});

	});
</script>
