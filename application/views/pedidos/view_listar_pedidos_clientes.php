<div class="tituloTelaComFiltro">

	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--12-col">
			<strong>Histórico dos Pedidos - Cliente: <?=$dados['cliente'] ?></strong>
		</div> 

	</div>

</div>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
	 	<div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" hidden id="loadSpinner"></div>
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th>Atender</th>
		      <th class="mdl-data-table__cell--non-numeric">Status</th>
		      <th>Data</th>
		      <th>Total R$:</th>
		      <th>Itens</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados['pedidos'] as $pedido) {
			    	echo '<tr>';

			    	if($pedido->status_pedido == 3){
			    		echo '<td width="10%">'.anchor('main/redirecionar/pedidos-view_editar_pedidos/'.$pedido->id_pedido, 'Atender', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Atender.', 'alt' => 'Atender.')).'</td>';
			    	} else {
			    		echo '<td width="10%">'.anchor('main/redirecionar/pedidos-view_editar_pedidos/'.$pedido->id_pedido, 'Detalhes', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Detalhes.', 'alt' => 'Detalhes.')).'</td>';
			    	}
					

					switch ($pedido->status_pedido) {
			    		case '5': //Concluído
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #00B848;" class="corStatus" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    		case '6': //Concluído
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #00B848;" class="corStatus" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    		case '7': //Concluído
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #00B848;" class="corStatus" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    		case '8': //Concluído
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #00B848;" class="corStatus" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;

						case '4': //Cancelado
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #E01329;" class="corStatus" status="'.$pedido->status_pedido.'" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    		
			    		case '3': //Aguardando
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #F4CC22;" class="corStatus" status="'.$pedido->status_pedido.'" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;

			    		default:
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color: #E7E6E5;" class="corStatus" status="'.$pedido->status_pedido.'" cod="'.$pedido->id_pedido.'"><input type="hidden" id="promotor_status'.$pedido->id_pedido.'" value="'.$pedido->status_pedido.'"></td>';
			    			break;
			    	}

					echo '<td width="10%">'.$pedido->data.'</td>';
					echo '<td width="10%">R$ '.$pedido->total.'</td>';

					//Usado para testes, mudar lógica para não chamar o Model pela View
					$teste = $this->model_pedidos->pegar_itens($pedido->id_pedido);

					echo '<td width="50%">';
					foreach ($teste as $itens) {
						echo $itens->item;
					}
					 echo '</td>';

					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--2-col" align="center"><div style="background-color: #00B848; color:black;">Aprovado</div></div>
	<div class="mdl-cell mdl-cell--2-col" align="center"><div style="background-color: #E01329; color:black;">Reprovado</div></div>
	<div class="mdl-cell mdl-cell--3-col" align="center"><div style="background-color: #F4CC22; color:black;">Aguardando Análise</div></div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#loadSpinner').hide();

		$('#filtro').click(function(){

			$('#loadSpinner').show();

			$('#load').load('<?php echo base_url(); ?>controller_pedidos/lista_filtro',{
				status:$('#status_pedido').val(),
				de:$('#de').val(),
				ate:$('#ate').val(),
				cpf:$('#cpf').val()},
				function(){

					$('#loadSpinner').hide();


			});

		});

	});
</script>
