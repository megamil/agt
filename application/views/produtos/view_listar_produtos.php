<div class="tituloTelaComFiltro">

	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--2-col">
			<strong>Produtos</strong>
		</div> 

		<div class="mdl-cell mdl-cell--7-col"></div>
		<div class="mdl-cell mdl-cell--3-col">
			<a href="<?php echo base_url(); ?>main/redirecionar/produtos-view_novos_produtos">
				<small>Cadastrar</small><br>
				NOVO PRODUTO <img src="<?php echo base_url(); ?>style/imagens/novo.png" width="28px">
			</a>
		</div>
	</div>

	<div class="mdl-grid filtro">

		<div class="mdl-cell mdl-cell--5-col">
			Filtro <hr>

			Nome <input type="text" class="mdl-textfield__input" aviso="Nome" name="nome" id="nome" size="50" maxlength="40"/>

		</div>

		<div class="mdl-cell mdl-cell--5-col">
			<br><hr>
			Preço <input type="text" class="mdl-textfield__input validar_decimais" aviso="Preço" name="preco" id="preco" size="50" maxlength="40"/>

		</div>

		<div class="mdl-cell mdl-cell--1-col">
			<br><hr>
			<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtro"><i class="material-icons">search</i>Buscar</button>	
		</div>

	</div>

</div>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
	 	<div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" hidden id="loadSpinner"></div>
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th class="mdl-data-table__cell--non-numeric">Editar</th>
		      <th class="mdl-data-table__cell--non-numeric">Imagem</th>
		      <th class="mdl-data-table__cell--non-numeric">Nome</th>
		      <th>Preço</th>
		      <th>Estoque</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados as $produto) {
			    	echo '<tr>';

					echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/produtos-view_editar_produtos/'.$produto->id_produto, 'Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Editar.', 'alt' => 'Editar.')).'</td>';

					echo '<td width="20%" class="mdl-data-table__cell--non-numeric"><a href="'.base_url().'produtos/'.$produto->url_produto.'" target="_blank" title="Click para abrir"><img src="'.base_url().'produtos/'.$produto->url_produto.'" width="120px"></a></td>';

					echo '<td width="50%"  class="mdl-data-table__cell--non-numeric">'.$produto->nome_produto.'</td>';
					echo '<td width="10%">R$ '.str_replace('.', ',',$produto->preco_produto).'</td>';
					echo '<td width="10%">'.$produto->quantidade_produto.'</td>';

					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#loadSpinner').hide();

		$('#filtro').click(function(){

			$('#loadSpinner').show();

			$('#load').load('<?php echo base_url(); ?>controller_produtos/lista_filtro',{
				nome:$('#nome').val(),
				preco:$('#preco').val()},
				function(){

					$('#loadSpinner').hide();


			});

		});

	});
</script>
