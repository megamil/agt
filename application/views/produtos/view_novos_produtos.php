<div class="mdl-grid tituloTela">
	<div class="mdl-cell mdl-cell--12-col">
		<img src="<?php echo base_url(); ?>style/imagens/produtos.png">
		<strong>Produtos</strong>
	</div>
</div>

<?php echo form_open_multipart('controller_produtos/criar_produto'); ?>

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--3-col">
 	<label class="label" for="usuario">Nome</label>
    <input type="text" class="mdl-textfield__input obrigatorio" name="nome" aviso="Nome" id="nome" size="50" maxlength="15" value="<?php echo $this->session->flashdata('nome_produto'); ?>"/>
    
 </div>

 <div class="mdl-cell mdl-cell--2-col">
 <label class="label" for="email">Preço</label>
    <input type="text" class="mdl-textfield__input validar_decimais obrigatorio" aviso="Preço" name="preco" id="preco"  size="50" maxlength="40" value="<?php echo $this->session->flashdata('preco_produto'); ?>"/>
    
 </div>

 <div class="mdl-cell mdl-cell--2-col">
 	<label class="label" for="telefone">Quantidade (Estoque)</label>
    <input type="tel" class="mdl-textfield__input validar_numeros obrigatorio" aviso="Quantidade" name="quantidade" id="quantidade"  size="50" maxlength="11" value="<?php echo $this->session->flashdata('quantidade_produto'); ?>"/>
 </div>

 <div class="mdl-cell mdl-cell--2-col">
 	<label class="label" for="nome">Código</label>
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Código" name="codigo" id="codigo"  size="50" maxlength="15" value="<?php echo $this->session->flashdata('codigo_produto'); ?>"/>
    
 </div>

 <div class="mdl-cell mdl-cell--2-col">
 	<button type="button" class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="gerar" style="margin-top: 15px;"><i class="material-icons">done</i>Gerar Código</button>	
 </div>

</div> <!-- Fecha mdl-grid Linha 1 -->

<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="nome">Imagem</label>
    	<input type="file" class="mdl-textfield__input" aviso="Imagem" name="imagem" id="imagem"  size="50" maxlength="150"/>
	    
	 </div>

	 <div class="mdl-cell mdl-cell--5-col">
	 	<label class="label" for="nome">Descrição</label>
	 	<textarea class="mdl-textfield__input" name="descricao" aviso="Descrição" maxlength="50"><?php echo $this->session->flashdata('descricao_produto'); ?></textarea>
	    
	 </div>

	<div class="mdl-cell mdl-cell--2-col">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar" style="margin-top: 15px;"><i class="material-icons">done</i>Cadastrar</button>	
	</div>

</div> <!-- Fecha mdl-grid Linha 2 -->

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){

		$('#gerar').click(function(){

			var codigo = '';

			/*
			*    matriz contendo em cada linha indices (inicial e final) da tabela ASCII para retornar alguns caracteres.
			*    [48, 57] = numeros;
			*    [64, 90] = "@" mais letras maiusculas;
			*    [97, 122] = letras minusculas;
			*/

			for (var j = 5; j >= 0; j--) {
				var ascii = [[48, 57],[64,90],[97,122]];
				var i = Math.floor(Math.random()*ascii.length);
				codigo = codigo+String.fromCharCode(Math.floor(Math.random()*(ascii[i][1]-ascii[i][0]))+ascii[i][0]);
			}

			$('#codigo').val(codigo);

		});

	});
</script>
