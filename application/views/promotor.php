<!DOCTYPE html>
<html>

	<head>

		<meta charset="UTF-8">	
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login Promotor</title>

		<!--CSS-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/mdl/material.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/promotor.css">

		<!--JS-->
		<script type="text/javascript" src="<?php echo base_url(); ?>style/mdl/material.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/index.js"></script>

		<!--ICON-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>style/mdl/mdl.css">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>style/imagens/1.png" type="image/x-icon">
		
	</head>

	<body>

	<noscript>
		<div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
		HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.
			<iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

		</div>
	</noscript> 

	<script type="text/javascript">
		history.replaceState({pagina: "index"}, "index", "<?php echo base_url() ?>");
	</script>

	<?php if($aviso['tipo'] != ''){ /*Confirma a existencia de aviso para esta tela.*/ 

		switch ($aviso['tipo']) { /*Define o icone de acordo com o tipo do erro*/
			case 'erro':
				$icon = 'error';
				break;

			case 'aviso':
				$icon = 'warning';
				break;
						
			default:
				$icon = 'check';
				break;
		}

		echo '<div class="'.$aviso['tipo'].'-card-wide mdl-card mdl-shadow--3dp card_aviso">
			  <div class="mdl-card__title">
			    <h2 class="mdl-card__title-text"><i class="material-icons">'.$icon.'</i>'.$aviso['titulo'].'</h2>
			  </div>
			  <div class="mdl-card__supporting-text">
			    '.$aviso['mensagem'].'
			  </div>
			  <div class="mdl-card__menu">
			    <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" id="fechar_aviso">
			      <i class="material-icons">clear</i>
			    </button>
			  </div>
			</div>';

		} ?>

	<?php echo form_open("main/login"); ?>

		<div id="caixaLogin" align="center">

			<div class="mdl-spinner mdl-js-spinner" style="margin-top: 47%;" id="loading" align="center" hidden></div>

			<div id="useresenha">

				<p style="color: white; font-size: 230%;">Área Restrita</p>

				<input class="mdl-textfield__input" type="text" name="user" id="user" placeholder="Usuário"/>
				<input class="mdl-textfield__input" type="password" name="pass" id="pass" placeholder="Senha" /> 

				<div align="center">
					<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--success" id="button">ENTRAR</button>	

					<a href="#" type="text" id="esqueceuSenhaPromotor" style="color: white"><i>Esqueci minha senha</i></a>
				</div>

			</div>

		</div>

	<?php echo form_close(); ?>

	</body>

</html>