<div class="tituloTelaComFiltro">

	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--2-col">
			<strong>Promotores</strong>
		</div> 

		<div class="mdl-cell mdl-cell--7-col"></div>
		<div class="mdl-cell mdl-cell--3-col">
			<a href="<?php echo base_url(); ?>main/redirecionar/promotores-view_novos_promotores">
				<small>Cadastrar</small><br>
				NOVO PROMOTOR <img src="<?php echo base_url(); ?>style/imagens/novo.png" width="28px">
			</a>
		</div>
	</div>

	<div class="mdl-grid filtro">

		<div class="mdl-cell mdl-cell--5-col">
			Filtro <hr>

			Nome <input type="text" class="mdl-textfield__input" aviso="Nome" name="nome" id="nome" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--5-col">
			<br><hr>
			CPF <input type="text" class="mdl-textfield__input mascara_cpf" aviso="Preço" name="cpf" id="cpf" size="50" maxlength="40"/>

		</div>

		<div class="mdl-cell mdl-cell--1-col">
			<br><hr>
			<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtro"><i class="material-icons">search</i>Buscar</button>	
		</div>

	</div>

</div>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
	 	<div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" hidden id="loadSpinner"></div>
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th class="mdl-data-table__cell--non-numeric">Editar</th>
		      <th class="mdl-data-table__cell--non-numeric">Nome</th>
		      <th>CPF</th>
		      <th>Adicionado Por:</th>
		      <th class="mdl-data-table__cell--non-numeric">Bairro</th>
		      <th>CEP</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados as $promotor) {
			    	echo '<tr>';

					echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/promotores-view_editar_promotores/'.$promotor->id_promotor, 'Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Editar.', 'alt' => 'Editar.')).'</td>';

					echo '<td width="40%"  class="mdl-data-table__cell--non-numeric">'.$promotor->nome_promotor.'</td>';
					echo '<td width="10%">'.$promotor->cpf_promotor.'</td>';

					if($promotor->nome_promotor_fk == ''){
						echo '<td width="20%">Usuário Administrador</td>';
					} else {
						echo '<td width="20%">'.$promotor->nome_promotor_fk.'</td>';
					}

					echo '<td width="20%" class="mdl-data-table__cell--non-numeric">'.$promotor->bairro_promotor.'</td>';
					echo '<td width="10%">'.$promotor->cep_promotor.'</td>';

					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#loadSpinner').hide();

		$('#filtro').click(function(){

			$('#loadSpinner').show();

			$('#load').load('<?php echo base_url(); ?>controller_promotores/lista_filtro',{
				nome:$('#nome').val(),
				cpf:$('#cpf').val()},
				function(){

					$('#loadSpinner').hide();


			});

		});

	});
</script>
