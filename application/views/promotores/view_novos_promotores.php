<div class="mdl-grid tituloTela">
	<div class="mdl-cell mdl-cell--12-col">
		<img src="<?php echo base_url(); ?>style/imagens/novo.png">
		<strong>Novo Promotor</strong>
	</div>
</div>

<?php echo form_open('controller_promotores/novo_promotor'); ?>

<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--5-col">
	 	<label class="label" for="nome_promotor">Nome</label>
	    <input type="text" class="mdl-textfield__input obrigatorio" name="nome_promotor" aviso="Nome" id="nome_promotor" size="50" maxlength="30" value="<?php echo $this->session->userdata('nome_promotor'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--2-col">
	 	<label class="label" for="cpf_promotor">CPF</label>
	    <input type="text" class="mdl-textfield__input mascara_cpf " name="cpf_promotor" aviso="CPF" id="cpf_promotor" size="50" maxlength="11" value="<?php echo $this->session->userdata('cpf_promotor'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--2-col">
	 	<label class="label" for="rg_promotor">RG</label>
	    <input type="text" class="mdl-textfield__input mascara_rg " name="rg_promotor" aviso="RG" id="rg_promotor" size="50" maxlength="11" value="<?php echo $this->session->userdata('rg_promotor'); ?>"/>   
	 </div>

	<div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="email_promotor">E-mail</label>
	    <input type="text" class="mdl-textfield__input validar_email " name="email_promotor" aviso="E-mail" id="email_promotor" size="50" maxlength="100" value="<?php echo $this->session->userdata('email_promotor'); ?>"/>   
	 </div>

 </div>

 <div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="tel_promotor">Tel</label>
	    <input type="text" class="mdl-textfield__input mascara_tel " name="tel_promotor" aviso="Telefone" id="tel_promotor" size="50" maxlength="11" value="<?php echo $this->session->userdata('tel_promotor'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="cel_promotor">Cel</label>
	    <input type="text" class="mdl-textfield__input mascara_cel " name="cel_promotor" aviso="Celular" id="cel_promotor" size="50" maxlength="11" value="<?php echo $this->session->userdata('cel_promotor'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="login_promotor">Usuário</label>
	    <input type="text" class="mdl-textfield__input " name="login_promotor" aviso="Usuário" id="login_promotor" size="50" maxlength="50" value="<?php echo $this->session->userdata('login_promotor'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="senha_promotor">Senha</label>
	    <input type="password" class="mdl-textfield__input " name="senha_promotor" aviso="Senha" id="senha_promotor" size="50" maxlength="50" value="<?php echo $this->session->userdata('senha_promotor'); ?>"/>   
	 </div>

 </div>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--2-col">
	 	<label class="label" for="cep_promotor">CEP</label>
	    <input type="text" class="mdl-textfield__input mascara_cep " name="cep_promotor" aviso="CEP" id="cep_promotor" size="50" value="<?php echo $this->session->userdata('cep_promotor'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="cidade_promotor">Cidade</label>
	    <input type="text" class="mdl-textfield__input " name="cidade_promotor" aviso="Cidade" id="cidade_promotor" maxlength="30" size="50" value="<?php echo $this->session->userdata('cidade_promotor'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--1-col">
	 	<label class="label" for="numero_promotor">Número</label>
	    <input type="text" class="mdl-textfield__input " name="numero_promotor" aviso="Número" id="numero_promotor" maxlength="20" size="50" value="<?php echo $this->session->userdata('numero_promotor'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="bairro_promotor">Bairro</label>
	    <input type="text" class="mdl-textfield__input " name="bairro_promotor" aviso="Bairro" id="bairro_promotor" maxlength="30" size="50" value="<?php echo $this->session->userdata('bairro_promotor'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">

		<label for="estado_promotor" class="label">Estado</label>
		<select class="mdl-cell mdl-cell--12-col" name="estado_promotor" id="estado_promotor" aviso="Estado">
			<?php 
				foreach ($dados['estados'] as $estado) {
					if($this->session->userdata('estado_promotor') == $estado->id_estado) {
						echo '<option value="'.$estado->id_estado.'" selected>'.$estado->nome.'</option>';
					} else {
						echo '<option value="'.$estado->id_estado.'">'.$estado->nome.'</option>';
					}
				}
			 ?>
		</select>
	</div>

</div>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--6-col">
	 	<label class="label" for="rua_promotor">Rua</label>
	    <input type="text" class="mdl-textfield__input  " name="rua_promotor" aviso="Rua" id="rua_promotor" size="50" maxlength="40" value="<?php echo $this->session->userdata('rua_promotor'); ?>"/>   
	 </div>

	<div class="mdl-cell mdl-cell--6-col">
	 	<label class="label" for="complemento_promotor">Complemento</label>
	    <input type="text" class="mdl-textfield__input " name="complemento_promotor" aviso="Complemento" id="complemento_promotor" size="50" maxlength="100" value="<?php echo $this->session->userdata('complemento_promotor'); ?>"/>   
	 </div>

</div>	 

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--3-col">

		<label for="fk_banco" class="label">Banco</label>
		<select class="mdl-cell mdl-cell--12-col" name="fk_banco" id="fk_banco" aviso="Banco">
			<?php 
				foreach ($dados['bancos'] as $banco) {
					if($this->session->userdata('fk_banco') == $banco->id_banco) {
						echo '<option value="'.$banco->id_banco.'" selected>'.$banco->banco.'</option>';
					} else {
						echo '<option value="'.$banco->id_banco.'">'.$banco->banco.'</option>';
					}
				}
			 ?>
		</select>
	</div>

	<div class="mdl-cell mdl-cell--2-col">
	 	<label class="label" for="agencia_promotor">Agencia</label>
	    <input type="text" class="mdl-textfield__input mascara_agencia " name="agencia_promotor" aviso="agencia" id="agencia_promotor" size="50" maxlength="10" value="<?php echo $this->session->userdata('agencia_promotor'); ?>"/>   
	 </div>

	 <div class="mdl-cell mdl-cell--2-col">
	 	<label class="label" for="conta_promotor">Conta</label>
	    <input type="text" class="mdl-textfield__input mascara_conta " name="conta_promotor" aviso="conta" id="conta_promotor" size="50" maxlength="15" value="<?php echo $this->session->userdata('conta_promotor'); ?>"/>   
	 </div>

</div>

 <div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--10-col">
	 	<label class="label" for="usuario">Observações</label>
	    <textarea class="mdl-textfield__input " aviso="Observação" name="obs_promotor" maxlength="50"><?php echo $this->session->userdata('obs_promotor'); ?></textarea>
	 </div>

	 <div class="mdl-cell mdl-cell--2-col">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar" style="margin-top: 15px;"><i class="material-icons">done</i>Cadastrar</button>	
	</div>

</div>

<?php echo form_close(); ?>