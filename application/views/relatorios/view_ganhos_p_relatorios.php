<div class="mdl-grid tituloTela">
	<div class="mdl-cell mdl-cell--12-col">
		<img src="<?php echo base_url(); ?>style/imagens/relatorios.png">
		<strong>Relatório Comissões Por pedidos em andamento</strong>
	</div>
</div>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th class="mdl-data-table__cell--non-numeric">Cliente</th>
		      <th class="mdl-data-table__cell--non-numeric">Pedido</th>
		      <th class="mdl-data-table__cell--non-numeric">Data do pedido</th>
		      <th class="mdl-data-table__cell--non-numeric">Comissão</th>
		      <th class="mdl-data-table__cell--non-numeric">Nível</th>
		      <th class="mdl-data-table__cell--non-numeric">Detalhes do pedido</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados as $comporpromotor) {
			    	echo '<tr>';

					echo '<td width="60%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->nome_cliente.'</td>';

					echo '<td width="60%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->fk_pedido.'</td>';

					echo '<td width="60%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->data_pedido.'</td>';

					echo '<td width="60%" class="mdl-data-table__cell--non-numeric">R$ '.$comporpromotor->comissao_atual.'</td>';

					echo '<td width="60%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->nivel.'</td>';

					echo '<td width="10%">'.anchor('main/redirecionar/pedidos-view_editar_pedidos/'.$comporpromotor->fk_pedido, 'Detalhes', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Detalhes.', 'alt' => 'Detalhes.')).'</td>';
					
					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>