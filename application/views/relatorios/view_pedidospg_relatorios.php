<?php echo form_open('controller_relatorios/excel_pedidospg'); ?>

<div class="tituloTelaComFiltro">

	<div class="mdl-grid tituloTela">
		<div class="mdl-cell mdl-cell--12-col">
			<img src="<?php echo base_url(); ?>style/imagens/relatorios.png">
			<strong>Relatório solicitações de pagamento</strong>
		</div>
	</div>

	<div class="mdl-grid filtro">

		 <div class="mdl-cell mdl-cell--2-col">
			 Filtro <hr>

			Status <select class="mdl-cell mdl-cell--12-col" name="status_pagamento" id="status_pagamento" aviso="Status">
				<option value="">Todos</option>
				<option value="3">Aguardando</option>
				<option value="5">Finalizado</option>
				<option value="4">Cancelado</option>
			</select>
			
		</div>

		<div class="mdl-cell mdl-cell--4-col">			
			<br><hr>
			Promotor <input type="text" class="mdl-textfield__input" aviso="Promotor" name="promotor" id="promotor" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--3-col">
			<br><hr>
		</div>

		<div class="mdl-cell mdl-cell--2-col">
			<br><hr>
			<button type="submit" class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="exportar"><i class="material-icons">file_download</i>Exportar Excel</button>	

		</div>

		<div class="mdl-cell mdl-cell--1-col">
			<br><hr>
			<button type="button" class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtro"><i class="material-icons">search</i>Buscar</button>	
		</div>

	</div>

</div>
<? echo form_close(); ?>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
	 	<div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" hidden id="loadSpinner"></div>
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th class="mdl-data-table__cell--non-numeric">Status</th>
		      <th class="mdl-data-table__cell--non-numeric">Pagamento</th>
		      <th class="mdl-data-table__cell--non-numeric">Promotor</th>
		      <th class="mdl-data-table__cell--non-numeric">Pedido em:</th>
		      <th class="mdl-data-table__cell--non-numeric">Valor Solicitado</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados as $pg) {
			    	echo '<tr>';

			    	switch ($pg->id_status) {
			    		case 3:
			    		echo '<td width="10%" align="left">'.anchor('Controller_pagamentos/confirmar_pagamento/'.$pg->id, 'Pagar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Pagar.', 'alt' => 'Pagar.'));

			    		echo ' '.anchor('Controller_pagamentos/cancelar_pagamento/'.$pg->id, 'Cancelar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect erro', 'title' => 'Cancelar.', 'alt' => 'Cancelar.', 'style' => 'background-color: #C1272D')).'</td>';
			    			break;

			    		case 4:
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color:#C1272D">'.$pg->nome_status.'</td>';
			    			break;
			    		
			    		default:
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color:#118049">'.$pg->nome_status.'</td>';
			    			break;
			    	}
					
						

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">'.$pg->id.'</td>';

					echo '<td width="40%" class="mdl-data-table__cell--non-numeric">'.$pg->nome.'</td>';

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">'.$pg->data_pg.'</td>';

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">R$ '.$pg->total.'</td>';
					
					echo '</tr>';
				} ?>
		  </tbody>
		</table>
	</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#loadSpinner').hide();

		$('#filtro').click(function(){

			$('#loadSpinner').show();

			$('#load').load('<?php echo base_url(); ?>controller_relatorios/ajax_pedidospg',{
				status_pagamento:$('#status_pagamento').val(),
				promotor:$('#promotor').val()},
				function(){

					$('#loadSpinner').hide();


			});

		});

		$('#exportar').click(function(){

			

		});

	});
</script>