<div class="mdl-grid tituloTela">
	<div class="mdl-cell mdl-cell--12-col">
		<img src="<?php echo base_url(); ?>style/imagens/relatorios.png">
		<strong>Relatórios Promotor</strong>
	</div>
</div>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th class="mdl-data-table__cell--non-numeric">Ver</th>
		      <th class="mdl-data-table__cell--non-numeric">Título</th>
		      <th class="mdl-data-table__cell--non-numeric">Descrição</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados as $relatorios) {
			    	echo '<tr>';

					echo '<td width="10%"  class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/relatorios-'.$relatorios->aplicacao, 'Ver', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Ver.', 'alt' => 'Ver.')).'</td>';

					echo '<td width="30%" class="mdl-data-table__cell--non-numeric">'.$relatorios->titulo_aplicacao.'</td>';

					echo '<td width="60%" class="mdl-data-table__cell--non-numeric">'.$relatorios->descricao_aplicacao.'</td>';
					
					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>