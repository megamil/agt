<div class="mdl-grid tituloTela">
	<div class="mdl-cell mdl-cell--12-col">
		<img src="<?php echo base_url(); ?>style/imagens/relatorios.png">
		<strong>Relatórios troca de comissão</strong>
	</div>
</div>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th class="mdl-data-table__cell--non-numeric">Nível Comissão</th>
		      <th class="mdl-data-table__cell--non-numeric">Novo Valor R$:</th>
		      <th class="mdl-data-table__cell--non-numeric">Alterado por:</th>
		      <th class="mdl-data-table__cell--non-numeric">Em:</th>
		    </tr>
		  </thead>
		  <tbody>
			    <?php foreach ($dados as $relatorios) {
			    	echo '<tr>';

			    	echo '<td width=10%" class="mdl-data-table__cell--non-numeric">'.$relatorios->nivel.'</td>';

					echo '<td width=10%" class="mdl-data-table__cell--non-numeric">'.$relatorios->comissao.'</td>';

					echo '<td width=10%" class="mdl-data-table__cell--non-numeric">'.$relatorios->nome.'</td>';

					echo '<td width=10%" class="mdl-data-table__cell--non-numeric">'.$relatorios->data_comissao.'</td>';
					
					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>