<?php echo form_open('controller_relatorios/excel_vendas'); ?>

<div class="tituloTelaComFiltro">

	<div class="mdl-grid tituloTela">
		<div class="mdl-cell mdl-cell--12-col">
			<img src="<?php echo base_url(); ?>style/imagens/relatorios.png">
			<strong>Relatório Comissões Por pedidos em andamento</strong>
		</div>
	</div>

	<div class="mdl-grid filtro">

		 <div class="mdl-cell mdl-cell--1-col">
			 Filtro <hr>

			Nível <select class="mdl-cell mdl-cell--12-col" name="nivel" id="nivel" aviso="Nível">
				<option value="">Todos</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
			</select>
			
		</div>

		<div class="mdl-cell mdl-cell--2-col">			
			<br><hr>
			Promotor <input type="text" class="mdl-textfield__input" name="promotor" id="promotor" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--2-col">			
			<br><hr>
			Cliente <input type="text" class="mdl-textfield__input" name="cliente" id="cliente" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--1-col">			
			<br><hr>
			Pedido <input type="text" class="mdl-textfield__input validar_numeros" name="pedido" id="pedido" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--1-col">			
			<br><hr>
			De <input type="text" class="mdl-textfield__input mascara_data" name="de" id="de" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--1-col">			
			<br><hr>
			Até <input type="text" class="mdl-textfield__input mascara_data" name="ate" id="ate" size="50" maxlength="30"/>

		</div>

		<div class="mdl-cell mdl-cell--2-col">
			<br><hr>
			<button type="submit" class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="exportar"><i class="material-icons">file_download</i>Exportar Excel</button>	

		</div>

		<div class="mdl-cell mdl-cell--1-col">
			<br><hr>
			<button type="button" class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtro"><i class="material-icons">search</i>Buscar</button>	
		</div>

	</div>

</div>
<? echo form_close(); ?>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
	 	<div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" hidden id="loadSpinner"></div>
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th class="mdl-data-table__cell--non-numeric">Cliente</th>
		      <th class="mdl-data-table__cell--non-numeric">Promotor</th>
		      <th class="mdl-data-table__cell--non-numeric">Pedido</th>
		      <th class="mdl-data-table__cell--non-numeric">Data do pedido</th>
		      <th class="mdl-data-table__cell--non-numeric">Comissão</th>
		      <th class="mdl-data-table__cell--non-numeric">Nível</th>
		      <th class="mdl-data-table__cell--non-numeric">Detalhes do pedido</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados as $comporpromotor) {
			    	echo '<tr>';

					echo '<td width="20%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->nome_cliente.'</td>';

					echo '<td width="20%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->nome_promotor.'</td>';

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->fk_pedido.'</td>';

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->data_pedido.'</td>';

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">R$ '.$comporpromotor->comissao_atual.'</td>';

					echo '<td width="60%" class="mdl-data-table__cell--non-numeric">'.$comporpromotor->nivel.'</td>';

					echo '<td width="10%">'.anchor('main/redirecionar/pedidos-view_editar_pedidos/'.$comporpromotor->fk_pedido, 'Detalhes', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Detalhes.', 'alt' => 'Detalhes.')).'</td>';
					
					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>


<script type="text/javascript">
	$(document).ready(function(){

		$('#loadSpinner').hide();

		$('#filtro').click(function(){

			$('#loadSpinner').show();

			$('#load').load('<?php echo base_url(); ?>controller_relatorios/ajax_vendas',{
				nivel:$('#nivel').val(),
				promotor:$('#promotor').val(),
				cliente:$('#cliente').val(),
				pedido:$('#pedido').val(),
				de:$('#de').val(),
				ate:$('#ate').val()},
				function(){

					$('#loadSpinner').hide();


			});

		});

		$('#exportar').click(function(){

			

		});

	});
</script>