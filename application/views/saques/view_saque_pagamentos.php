<script type="text/javascript">

	$(document).ready(function(){

		<?php if($dados['valores']->row()->total > 0){ ?>

		var total = '<?php echo $dados['valores']->row()->total; ?>';

		$('#chamarModal').click(function(){

			var digitado = $('#receber').val();

			if(digitado <= total){

				$( "#dialog-confirm" ).prop('title','Deseja solicitar o pagamento?');

			    $( "#dialog-confirm" ).dialog({
			      resizable: false,
			      height: "auto",
			      width: 400,
			      modal: true,
			      buttons: {
			        "Solicitar": function() {
			          window.location.assign("<?php echo base_url() ?>controller_pagamentos/abrir_pedido/"+digitado.replace(',','.'));
			        },
			        "Cancelar": function() {
			          $( this ).dialog( "close" );
			        }
			      }
			    }).prev(".ui-dialog-titlebar").css("background","#118049");

			} else {

				alert('Valor Maior que o Saldo.');

			}

			
		});

	<?php } else { ?>

		$('#chamarModal').click(function(){
			$(function() {

				$( "#dialog-confirm" ).prop('title','Saldo insuficiente');

			    $( "#dialog-confirm" ).dialog({
			      resizable: false,
			      height: "auto",
			      width: 400,
			      modal: true,
			      buttons: {
			        "Cancelar": function() {
			          $( this ).dialog( "OK" );
			        }
			      }
			    }).prev(".ui-dialog-titlebar").css("background","#118049");
			});
		});


	<?php } ?>

	});

</script>

<div class="tituloTelaComFiltro">

	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--2-col">
			<strong>pagamentos</strong>
		</div> 

		<div class="mdl-cell mdl-cell--5-col"></div>
		<div class="mdl-cell mdl-cell--2-col">
			<label class="label" for="receber">Solicitar R$:</label>
			<input type="text" class="mdl-textfield__input validar_decimais" name="receber" id="receber" value="<?php echo $dados['valores']->row()->total; ?>">
		</div>
		<div class="mdl-cell mdl-cell--3-col">

			<span id="dialog-confirm" hidden></span>

			<a href="#" id="chamarModal">
				<small>Solicitar</small><br>
				NOVO PAGAMENTO <img src="<?php echo base_url(); ?>style/imagens/novo.png" width="28px">
			</a>
		</div>
	</div>

	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--6-col">
			<small align="left" style="color: #118049;">
				Saldo R$: <?php echo $dados['valores']->row()->total; ?>
			</small>
		</div>
		<div class="mdl-cell mdl-cell--6-col"></div>
	</div>

</div>

<div class="mdl-grid" align="center">

	 <div class="mdl-cell mdl-cell--12-col">
	 	<div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" hidden id="loadSpinner"></div>
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" width="100%">
		  <thead>
		    <tr>
		      <th class="mdl-data-table__cell--non-numeric">Status</th>
		      <th class="mdl-data-table__cell--non-numeric">Pagamento</th>
		      <th class="mdl-data-table__cell--non-numeric">Pedido em:</th>
		      <th class="mdl-data-table__cell--non-numeric">Valor Solicitado</th>
		    </tr>
		  </thead>
		  <tbody id="load">
			    <?php foreach ($dados['pagamentos'] as $pg) {
			    	echo '<tr>';
					
					switch ($pg->id_status) {
			    		case 3:
			    		echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color:#FFFF00">'.$pg->nome_status.'</td>';
			    			break;

			    		case 4:
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color:#C1272D">'.$pg->nome_status.'</td>';
			    			break;
			    		
			    		default:
			    			echo '<td width="10%" class="mdl-data-table__cell--non-numeric" style="background-color:#118049">'.$pg->nome_status.'</td>';
			    			break;
			    	}

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">'.$pg->id.'</td>';

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">'.$pg->data_pg.'</td>';

					echo '<td width="10%" class="mdl-data-table__cell--non-numeric">R$ '.$pg->total.'</td>';
					
					echo '</tr>';
				} ?>
		  </tbody>
		</table>

	</div>

</div>
