<script type="text/javascript">
	history.replaceState({pagina: "listar_grupos"}, "Novo Grupo", "<?php echo base_url() ?>main/redirecionar/seguranca-view_listar_grupos");
</script>

<?php echo form_open('controller_seguranca/editar_grupo'); 
echo form_fieldset('Editar Grupo '.$dados['grupo']->row()->nome_grupo);
echo '<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="apagar"><i class="material-icons">clear</i>Limpar campos</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="voltar" class=""><i class="material-icons">reply</i>Voltar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="recarregar" url="'.$_SERVER ['REQUEST_URI'].'"><i class="material-icons">cached</i>Recarregar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		'.anchor('main/redirecionar/seguranca-view_novos_grupos', '<i class="material-icons">add</i>', array('class' => 'mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored', 'title' => 'Editar grupo', 'alt' => 'Editar grupo')).'
	</div>

</div>';
?>

<hr>
<input type="hidden" name="id_grupo" value="<?php echo $dados['grupo']->row()->id_grupo; ?>" size="50" />
<input type="hidden" name="nome_anterior" value="<?php echo $dados['grupo']->row()->nome_grupo; ?>" size="50" />

<div align="center">

	<div class="mdl-grid">

		<?php if($dados['grupo']->row()->id_grupo == 1){$disabled = 'disabled';} else {$disabled = '';} ?>

		 <div class="mdl-cell mdl-cell--4-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Nome do grupo" name="nome_grupo" id="nome_grupo" value="<?php echo $dados['grupo']->row()->nome_grupo; ?>" size="50" <?php echo $disabled; ?> maxlength="15"/>
		    <label class="mdl-textfield__label" for="nome_grupo">Nome do Grupo</label>
		 </div>

		 <div class="mdl-cell mdl-cell--4-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Descrição do grupo" name="descricao_grupo" id="descricao_grupo" value="<?php echo $dados['grupo']->row()->descricao_grupo; ?>" size="50" <?php echo $disabled; ?> />
		    <label class="mdl-textfield__label" for="descricao_grupo">Descrição do Grupo</label>
		 </div>

		

		<div class="mdl-cell mdl-cell--4">
			<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar" <?php echo $disabled; ?>><i class="material-icons">done</i>Editar Grupo</button>	
		</div>

	</div>

</div>

<?php echo form_fieldset('Defina Aplicação(ões) ao grupo.') ?>

	<hr>

		<div align="center">
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
		  <thead>
		    <tr>
		      <th>ID</th>
		      <th>Aplicação</th>
		      <th>Descrição</th>
		      <th>Pertence</th>
		      <th>Adicionar ou Remover</th>
		    </tr>
		  </thead>
		  <tbody>
		    <?php foreach ($dados['aplicacoes'] as $grupo) {
		    	echo '<tr>';
				echo '<td>'.$grupo->id_aplicacao.'</td>';
				echo '<td>'.$grupo->titulo_aplicacao.'</td>';
				echo '<td>'.$grupo->descricao_aplicacao.'</td>';
				if($disabled != 'disabled') {
					
					if($grupo->pertence == 1){
						echo '<td class="sucesso">Pertence ao grupo</td>';
						echo '<td>'.anchor('controller_seguranca/add_rem_aplicacao/remover/'.$dados['grupo']->row()->id_grupo.'/'.$grupo->id_aplicacao, 'Remover do grupo', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect alerta', 'title' => 'Remover do Grupo.', 'alt' => 'Remover do Grupo.')).'</td>';
					} else {
						echo '<td class="alerta">Não pertence ao grupo</td>';
						echo '<td>'.anchor('controller_seguranca/add_rem_aplicacao/adicionar/'.$dados['grupo']->row()->id_grupo.'/'.$grupo->id_aplicacao, 'Adicionar ao grupo', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Adicionar ao Grupo.', 'alt' => 'Adicionar ao Grupo.')).'</td>';
					}

				} else {
					echo '<td class="sucesso">Pertence ao grupo</td>';
					echo '<td class="desabilitado">Não se pode remover.</td>';
				}
				echo '</tr>';
			} ?>
		  </tbody>
		</table>
		</div>

	<?php echo form_fieldset_close();

echo form_fieldset_close();
echo form_close(); ?>