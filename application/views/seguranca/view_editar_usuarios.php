<div class="mdl-grid tituloTela">
	<div class="mdl-cell mdl-cell--12-col">
		<img src="<?php echo base_url(); ?>style/imagens/novo.png">
		<strong>Editar Usuário</strong>
	</div>
</div>

<?php echo form_open('controller_seguranca/editar_usuario'); 
?>
<div class="mdl-grid">

<input type="hidden" name="id_usuario" value="<?php echo $dados['usuario']->row()->id_usuario; ?>">

<input type="hidden" name="usuario_anterior" value="<?php echo $dados['usuario']->row()->usuario; ?>">
<input type="hidden" name="email_anterior" value="<?php echo $dados['usuario']->row()->email; ?>">

 <div class="mdl-cell mdl-cell--3-col">
    <label class="label" for="usuario">Usuário</label>
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Usuário" name="usuario" id="usuario" value="<?php echo $dados['usuario']->row()->usuario; ?>" size="50" maxlength="20"/>
    
 </div>

 <div class="mdl-cell mdl-cell--3-col">
 	<label class="label" for="email">E-mail</label>
    <input type="text" class="mdl-textfield__input obrigatorio validar_email" aviso="E-mail" name="email" id="email" value="<?php echo $dados['usuario']->row()->email; ?>" size="50" maxlength="40"/>
 </div>
 <div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="telefone">Telefone</label>
    <input type="tel" class="mdl-textfield__input obrigatorio mascara_cel" aviso="Telefone" name="telefone" id="telefone" value="<?php echo $dados['usuario']->row()->telefone; ?>" size="50" maxlength="11"/>
 </div>

 <div class="mdl-cell mdl-cell--3-col">
    <label class="label" for="nome">Nome</label>
    <input type="text" class="mdl-textfield__input obrigatorio" name="nome" aviso="Nome" id="nome" value="<?php echo $dados['usuario']->row()->nome; ?>" size="50" maxlength="40" />
 </div>

</div> <!-- Fecha mdl-grid Linha 1 -->

<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col">
	    <label class="label" for="senha">Nova Senha</label>
	    <input type="password" class="mdl-textfield__input" aviso="Senha" name="senha" id="senha" value="" size="50" maxlength="32"/>
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	    <label class="label" for="confirmacao">Confirme a nova Senha</label>
	    <input type="password" class="mdl-textfield__input " aviso="confirmação de senha" name="confirmacao" id="confirmacaoSenha" value="" size="50" maxlength="32"/>
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
		<label for="fk_grupo" class="label">Grupo</label>
		<select class="mdl-cell mdl-cell--12-col" name="fk_grupo" id="fk_grupo" aviso="GRUPO">
			<?php 

				if($dados['grupo']->row()->fk_grupo == 3){
					echo '<option value="1">Administrador</option>
						  <option value="3" selected>Delivery</option>';
				} else {
					echo '<option value="1" selected>Administrador</option>
						  <option value="3">Delivery</option>';
				}

			 ?>
		</select>
	</div>

	<?php if($dados['usuario']->row()->ativo != 'off') { $checked = 'checked';} else {$checked = '';} ?>

		<label class="mdl-cell mdl-cell--1-col mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-1" style="margin-top: 20px;">
		  <span class="mdl-switch__label">Ativo</span>	
		  <input type="checkbox" name="ativo" id="switch-1" class="mdl-switch__input" <?php echo $checked ?> />
		</label>

	<div class="mdl-cell mdl-cell--2">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar"><i class="material-icons">done</i>Editar Usuário</button>	
	</div>

</div> <!-- Fecha mdl-grid Linha 2 -->

<?php echo form_fieldset_close();
echo form_close(); ?>