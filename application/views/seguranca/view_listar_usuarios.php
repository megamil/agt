<div class="mdl-grid" style="margin-top: 30px;">
	<div class="mdl-cell mdl-cell--2-col">
		<strong>Usuários</strong>
	</div> 

	<div class="mdl-cell mdl-cell--7-col"></div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="<?php echo base_url(); ?>main/redirecionar/seguranca-view_novos_usuarios">
			<small>Cadastrar</small><br>
			NOVO USUÁRIO <img src="<?php echo base_url(); ?>style/imagens/novo.png" width="28px">
		</a>
	</div>
</div>

<hr>

<div class="mdl-grid" align="center">

	<div class="mdl-cell mdl-cell--12-col">
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
		  <thead>
		    <tr>
		      <th width="5%">ID</th>
		      <th class="mdl-data-table__cell--non-numeric">Usuários</th>
		      <th class="mdl-data-table__cell--non-numeric">Grupo</th>
		      <th>Telefone</th>
		      <th class="mdl-data-table__cell--non-numeric">Nome</th>
		      <th class="mdl-data-table__cell--non-numeric">Status</th>
		      <th class="mdl-data-table__cell--non-numeric">Editar</th>
		    </tr>
		  </thead>
		  <tbody>
		    <?php foreach ($dados as $usuario) {
		    	echo '<tr>';
				echo '<td width="5%">'.$usuario->id_usuario.'</td>';
				echo '<td class="mdl-data-table__cell--non-numeric">'.$usuario->usuario.'</td>';
				echo '<td class="mdl-data-table__cell--non-numeric">'.$usuario->nome_grupo.'</td>';
				echo '<td><input value="'.$usuario->telefone.'" class="label_mascara_input mascara_telefone"/></td>';
				echo '<td class="mdl-data-table__cell--non-numeric">'.$usuario->nome.'</td>';
				if($usuario->ativo == 1) {echo '<td class="mdl-data-table__cell--non-numeric">Ativo</td>';}else{echo '<td class="mdl-data-table__cell--non-numeric">Inativo</td>';}
				echo '<td class="mdl-data-table__cell--non-numeric">'.anchor('main/redirecionar/seguranca-view_editar_usuarios/'.$usuario->id_usuario, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar Usuário.', 'alt' => 'Editar Usuário.', 'style' => 'margin-top: -7px;')).'</td>';
				echo '</tr>';
			} ?>
		  </tbody>
		</table>
	</div>
</div>

