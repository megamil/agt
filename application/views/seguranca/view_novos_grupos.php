<script type="text/javascript">
	history.replaceState({pagina: "listar_grupos"}, "Novo Grupo", "<?php echo base_url() ?>main/redirecionar/seguranca-view_listar_grupos");
</script>

<?php echo form_open('controller_seguranca/criar_grupo'); 
echo form_fieldset('Criar Grupo');
echo '<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="apagar"><i class="material-icons">clear</i>Limpar campos</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="voltar" class=""><i class="material-icons">reply</i>Voltar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="recarregar" url="'.$_SERVER ['REQUEST_URI'].'"><i class="material-icons">cached</i>Recarregar</a>
	</div>
</div>';
?>

<hr>

<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--4-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Nome do grupo" name="nome_grupo" id="nome_grupo" value="<?php echo $this->session->flashdata('nome_grupo'); ?>" size="50" maxlength="15"/>
	    <label class="mdl-textfield__label" for="nome_grupo">Nome do Grupo</label>
	 </div>

	 <div class="mdl-cell mdl-cell--4-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Descrição do grupo" name="descricao_grupo" id="descricao_grupo" value="<?php echo $this->session->flashdata('descricao_grupo'); ?>" size="50" />
	    <label class="mdl-textfield__label" for="descricao_grupo">Descrição do Grupo</label>
	 </div>

	<div class="mdl-cell mdl-cell--4">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar"><i class="material-icons">done</i>Criar Grupo</button>	
	</div>

</div>

 <?php echo form_fieldset_close();
echo form_close(); ?>