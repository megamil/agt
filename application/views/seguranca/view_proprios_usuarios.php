<div class="mdl-grid tituloTela">
	<div class="mdl-cell mdl-cell--12-col">
	<img src="<?php echo base_url(); ?>style/imagens/config-verde.png" >
	<strong>Configurações Pessoais</strong>
	</div>
</div>

<?php echo form_open('controller_seguranca/editar_proprio_usuario'); ?>
<input type="hidden" name="id_usuario" value="<?php echo $dados->row()->id_usuario; ?>" size="50" />
<input type="hidden" name="usuario_anterior" value="<?php echo $dados->row()->usuario; ?>" size="50" />
<input type="hidden" name="email_anterior" value="<?php echo $dados->row()->email; ?>" size="50" />

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--3-col">
 	<label class="label" for="usuario">Usuário</label>
    <input type="text" class="mdl-textfield__input obrigatorio" name="usuario" aviso="Usuário" id="usuario" value="<?php echo $dados->row()->usuario; ?>" size="50" maxlength="15"/>
    
 </div>

 <div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="email">E-mail</label>
    <input type="text" class="mdl-textfield__input validar_email obrigatorio" aviso="E-mail" name="email" id="email" value="<?php echo $dados->row()->email; ?>" size="50" maxlength="40"/>
    
 </div>

 <div class="mdl-cell mdl-cell--3-col">
 	<label class="label" for="telefone">Telefone</label>
    <input type="tel" class="mdl-textfield__input mascara_cel obrigatorio" aviso="Telefone" name="telefone" id="telefone" value="<?php echo $dados->row()->telefone; ?>" size="50" maxlength="11" />
 </div>

 <div class="mdl-cell mdl-cell--3-col">
 	<label class="label" for="nome">Nome</label>
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Nome" name="nome" id="nome" value="<?php echo $dados->row()->nome; ?>" size="50" maxlength="40"/>
    
 </div>

</div> <!-- Fecha mdl-grid Linha 1 -->

<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="senha">Nova Senha</label>
	    <input type="password" class="mdl-textfield__input" aviso="Senha" name="senha" id="senha" value="" size="50" maxlength="32"/>
	    
	 </div>

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="confirmacao">Confirme a nova Senha</label>
	    <input type="password" class="mdl-textfield__input" aviso="Confirmação de senha" name="confirmacao" id="confirmacaoSenha" value="" size="50" maxlength="32"/>
	    
	 </div>

		<label class="mdl-cell mdl-cell--1-col mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-1" style="margin-top: 20px;" hidden>
		  <span class="mdl-switch__label">Ativo</span>	
		  <input type="checkbox" name="ativo" id="switch-1" class="mdl-switch__input" checked disabled/>
		</label>

</div> <!-- Fecha mdl-grid Linha 2 -->


<?php if($this->session->userdata('id_grupo') == 1) { ?>

<div class="mdl-grid tituloTela" style="margin-top: 100px !important;">
	<strong>Outras configurações</strong>
</div>

<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--2-col">
		<label class="label" for="comissao1">Comissão Nível 1.</label>
	    <input type="text" class="mdl-textfield__input validar_decimais obrigatorio" aviso="Comissão" name="comissao1" id="comissao1" value="<?php echo str_replace('.', ',',$dados->row()->comissao1); ?>" />
	</div>

	<div class="mdl-cell mdl-cell--2-col">
		<label class="label" for="comissao2">Comissão Nível 2.</label>
	    <input type="text" class="mdl-textfield__input validar_decimais obrigatorio" aviso="Comissão" name="comissao2" id="comissao2" value="<?php echo str_replace('.', ',',$dados->row()->comissao2); ?>" />
	</div>

	<div class="mdl-cell mdl-cell--2-col">
		<label class="label" for="comissao3">Comissão Nível 3.</label>
	    <input type="text" class="mdl-textfield__input validar_decimais obrigatorio" aviso="Comissão" name="comissao3" id="comissao3" value="<?php echo str_replace('.', ',',$dados->row()->comissao3); ?>" />
	</div>

	<div class="mdl-cell mdl-cell--2-col">
		<label class="label" for="comissao4">Comissão Nível 4.</label>
	    <input type="text" class="mdl-textfield__input validar_decimais obrigatorio" aviso="Comissão" name="comissao4" id="comissao4" value="<?php echo str_replace('.', ',',$dados->row()->comissao4); ?>" />
	</div>

	<div class="mdl-cell mdl-cell--2-col">
		<label class="label" for="comissao5">Comissão Nível 5.</label>
	    <input type="text" class="mdl-textfield__input validar_decimais obrigatorio" aviso="Comissão" name="comissao5" id="comissao5" value="<?php echo str_replace('.', ',',$dados->row()->comissao5); ?>" />
	</div>

</div>

<?php } ?>

<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--9-col"></div>

	<div class="mdl-cell mdl-cell--3-col">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar" style="margin-top: 15px;"><i class="material-icons">done</i>Confirmar Edição</button>	
	</div>
</div>

<?php echo form_close(); ?>