$(document).ready( function () {

	if(!$('table').hasClass('semDataTable')){

	    $('table').DataTable({

		    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
			buttons: [
		        {
		            extend: 'collection', text: 'Opções',
		            buttons: [

			            {extend: 'colvis', text: 'Colunas'},
			            {extend: 'excel', text: 'Exportar para Excel'},
			            {extend: 'print', text: 'Imprimir'},

		            ]
		        }
		    ],"oLanguage": {
				"sLengthMenu": "Mostrar _MENU_ registros por página",
				"sZeroRecords": "Nenhum registro encontrado",
				"sInfo": "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
				"sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
				"sInfoFiltered": "(filtrado de _MAX_ registros)",
				"sSearch": "Pesquisar: ",
				"oPaginate": {
					"sFirst": "Início ",
					"sPrevious": " Anterior ",
					"sNext": " Próximo ",
					"sLast": " Último "
				}
			},language: {
	        decimal: ",",
	   		}});
	 }

});