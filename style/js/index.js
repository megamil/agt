$(document).ready(function(){

	/*Código usado no popup de avisos*/

	//Código para fechar o aviso.
	$('#fechar_aviso').click(function(){

		$(".card_aviso").animate({opacity: 0.10, right: "+=1000"}, 300, function(){});

		setTimeout(function() {$('.aviso').remove();}, 1200);

	});

	var urli = "/agt/";

	$(document).on('click','#esqueceuSenha', function(){

		if($("#user").val() != ''){

			$("#useresenha").hide();
			$("#loading").addClass('is-active').removeAttr('hidden');

			$.ajax({
				  url: urli+'controller_seguranca/esqueci_Senha',
				  type: 'GET',
				  data: 'user='+$("#user").val(),
				  success: function(data) {

				  	alert(data);
					$("#useresenha").show(),
					$("#loading").removeClass('is-active').attr('hidden','Hidden')

				  },
				  error: function(e) {
					
				  		alert(e)

				  }
				});

		} else {

			alert('Preencha seu usuário e deixe a senha em branco.');

		}

	});

	$(document).on('click','#esqueceuSenhaPromotor', function(){

		if($("#user").val() != ''){

			$("#useresenha").hide();
			$("#loading").addClass('is-active').removeAttr('hidden');

			$.ajax({
				  url: urli+'controller_seguranca/esqueci_Senha_Promotor',
				  type: 'GET',
				  data: 'user='+$("#user").val(),
				  success: function(data) {

				  	alert(data);
					$("#useresenha").show(),
					$("#loading").removeClass('is-active').attr('hidden','Hidden')

				  },
				  error: function(e) {
					
				  		alert(e)

				  }
				});

		} else {

			alert('Preencha seu usuário e deixe a senha em branco.');

		}

	});


});