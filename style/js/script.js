function showLoading() {
    // remove existing loaders
    $('.loading-container').remove();
    $('<div id="orrsLoader" class="loading-container"><div><div class="mdl-spinner mdl-js-spinner is-active"></div></div></div>').appendTo("body");

    componentHandler.upgradeElements($('.mdl-spinner').get());
    setTimeout(function () {
        $('#orrsLoader').css({opacity: 1});
    }, 1);
}

function hideLoading() {
    $('#orrsLoader').css({opacity: 0});
    setTimeout(function () {
        $('#orrsLoader').remove();
    }, 400);
}

function showDialog(options) {
    options = $.extend({
        id: 'orrsDiag',
        title: null,
        text: null,
        negative: false,
        positive: false,
        cancelable: true,
        contentStyle: null,
        onLoaded: false
    }, options);

    // remove existing dialogs
    $('.dialog-container').remove();
    $(document).unbind("keyup.dialog");

    $('<div id="' + options.id + '" class="dialog-container"><div class="mdl-card mdl-shadow--16dp"></div></div>').appendTo("body");
    var dialog = $('#orrsDiag');
    var content = dialog.find('.mdl-card');
    if (options.contentStyle != null) content.css(options.contentStyle);
    if (options.title != null) {
        $('<h5>' + options.title + '</h5>').appendTo(content);
    }
    if (options.text != null) {
        //$('<p>' + options.text + '</p>').appendTo(content);
        $(options.text).appendTo(content);
    }
    if (options.negative || options.positive) {
        var buttonBar = $('<div class="mdl-card__actions dialog-button-bar"></div>');
        if (options.negative) {
            options.negative = $.extend({
                id: 'negative',
                title: 'Cancel',
                onClick: function () {
                    return false;
                }
            }, options.negative);
            var negButton = $('<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect falha" id="' + options.negative.id + '">' + options.negative.title + '</button>');
            negButton.click(function (e) {
                e.preventDefault();
                if (!options.negative.onClick(e))
                    hideDialog(dialog)
            });
            negButton.appendTo(buttonBar);
        }
        if (options.positive) {
            options.positive = $.extend({
                id: 'positive',
                title: 'OK',
                onClick: function () {
                    return false;
                }
            }, options.positive);
            var posButton = $('<button class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect sucesso" id="' + options.positive.id + '">' + options.positive.title + '</button>');
            posButton.click(function (e) {
                e.preventDefault();
                if (!options.positive.onClick(e))
                    hideDialog(dialog)
            });
            posButton.appendTo(buttonBar);
        }
        buttonBar.appendTo(content);
    }
    componentHandler.upgradeDom();
    if (options.cancelable) {
        dialog.click(function () {
            hideDialog(dialog);
        });
        $(document).bind("keyup.dialog", function (e) {
            if (e.which == 27)
                hideDialog(dialog);
        });
        content.click(function (e) {
            e.stopPropagation();
        });
    }
    setTimeout(function () {
        dialog.css({opacity: 1});
        if (options.onLoaded)
            options.onLoaded();
    }, 1);
}

function hideDialog(dialog) {
    $(document).unbind("keyup.dialog");
    dialog.css({opacity: 0});
    setTimeout(function () {
        dialog.remove();
    }, 400);
}

$(document).ready(function(){

	/*Botões no menu lateral.*/

	//Oculta os menus
	$( ".toggle_menu_lateral" ).hide();

	//Abre o menu
	$( ".toggle_lateral" ).click(function() {
	  $( this ).next('.toggle_menu_lateral').toggle( "fast", function() {});
	});

	/**/

	/*Botões*/

	$('#voltar').click(function() {
    	history.back()
	});

	$('#recarregar').click(function() {

		var recarregar = confirm('Dados inseridos ou alterados serão perdidos.');

		if(recarregar) {	
			history.replaceState({pagina: 1}, "Recarregado", $(this).attr('url'));
		} else {
			event.preventDefault();
		}
    	
	});

	$("#apagar").click(function(){
		//Zera os campos imputs
		$('input').each(function(){
			if($(this).attr('readonly')!="readonly"){
				$(this).css("background-color","white");
				$(this).val("");
			}		
		});
		//Zera os selects
		$('.mdl-select__input').each(function(){
			$(this).css("background-color","white");
			$(this).prop('selectedIndex',0);
		});
		event.preventDefault();
	});

	/*Botões*/

	//caso o usuário tente digitar mais do que o campo comporta
	$(".mdl-textfield__input").keyup(function(){
		if($(this).val().length == $(this).attr('maxlength')){
			$(this).finish();
			$(this).effect("shake");
		} 

	});

	/*Código usado no popup de avisos*/

	//Código para fechar o aviso.
	$('#fechar_aviso').click(function(){

		$(".card_aviso").animate({opacity: 0.10, left: "150"}, 100, function(){});

		setTimeout(function() {$('.card_aviso').remove();}, 400);

	});

	/*Código usado no popup de avisos*/


	/*Validações de campos*/

	//Validando E-mail
	$(".validar_email").focusout(function(){

	  $("#erro_email").remove();

      if($(this).val() != "") { 

         var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

         if(!filtro.test($(this).val())) {
		
         	$("#aviso_de_erro").fadeIn("slow", function() { $(this).append('<div id="erro_email"><i class="material-icons">error</i> E-mail inválido!</div>'); });
         	$(this).css('background-color', 'LightSalmon');
        	return false;

         } else {

         	$(this).css('background-color', 'LightGreen');
         	$("#erro_email").remove(); 

         }

      } else {

      	$(this).css('background-color', '');
      	$("#erro_email").remove(); 

      }

   });

	// Aceita somente números
	$(".validar_numeros").keyup(function() {
		var $this = $( this ); //armazeno o ponteiro em uma variavel
		var valor = $this.val().replace(/[^1234567890]+/g,'');
		$this.val( valor );
	});

	// Aceita somente números com . ou ,
	$(".validar_decimais").keyup(function() {
		var $this = $( this ); //armazeno o ponteiro em uma variavel
		var valor = $this.val().replace(/[^1234567890.,]+/g,'');
		$this.val( valor );
	});

	// Validador de CPF
	$(".validar_cpf").focusout(function(){

		$("#erro_cpf").remove(); 

		if($(this).val() != '') {

		valor = jQuery.trim($(this).val());

        exp = /\.|\-|\//g;
	    cpf = valor.toString().replace(exp, "" );

        while(cpf.length < 11) cpf = "0"+ cpf;
        var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
        var a = [];
        var b = new Number;
        var c = 11;
        for (i=0; i<11; i++){
            a[i] = cpf.charAt(i);
            if (i < 9) b += (a[i] * --c);
        }
        if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
        b = 0;
        c = 11;
        for (y=0; y<10; y++) b += (a[y] * c--);
        if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
	        if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) {

	         	$("#aviso_de_erro").fadeIn("slow", function() { $(this).append('<div id="erro_cpf"><i class="material-icons">error</i> CPF inválido!</div>'); });
	         	$(this).css('background-color', 'LightSalmon');

	     	} else { 

	     		$("#erro_cpf").remove();
	     		$(this).css('background-color', 'LightGreen');

	     	}	

        } else {

        	$("#erro_cpf").remove(); 
        	$(this).css('background-color', '');

        }
        
		  
	});
	
	// Validador de CNPJ
	$(".validar_cnpj").focusout(function(){

		$("#erro_cnpj").remove(); 

		if($(this).val() != '') {

			cnpj = $(this).val();
	        valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
	        dig1= new Number;
	        dig2= new Number;

	        exp = /\.|\-|\//g;
	        cnpj = cnpj.toString().replace(exp, "" ); 
	        digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));

	        for(i = 0; i<valida.length; i++){
	                dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);  
	                dig2 += cnpj.charAt(i)*valida[i];       
	        }
	        dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
	        dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));

	        if(((dig1*10)+dig2) != digito)  {

	        	$("#aviso_de_erro").fadeIn("slow", function() { $(this).append('<div id="erro_cnpj"><i class="material-icons">error</i> CNPJ inválido!</div>'); });
	         	$(this).css('background-color', 'LightSalmon');

	        } else {

				$("#erro_cnpj").remove(); 
         		$(this).css('background-color', 'LightGreen');

	        }

	    } else {

			$("#erro_cnpj").remove(); 
			$(this).css('background-color', '');

	    }
                


	});

	//Deixa campos inputs, que tem mascara, com aparencia de label.
	$('.label_mascara_input').attr('disabled','Disabled')
	.css('border','none')
	.css('background','none');

	//Máscaras

	$('.mascara_cel').mask("(?99)99999-9999");
	$('.mascara_tel').mask("(?99)9999-9999");
	$('.mascara_cnpj').mask("99.999.999/9999-99");
	$('.mascara_cpf').mask("999.999.999-99");
	$('.mascara_rg').mask("?99.999.999-*"); 
	$('.mascara_cep').mask("?99999-999");
	$('.mascara_placa').mask("?aaa-9999");
	$('.mascara_data').mask("?99/99/9999");

	/*Calendário com datepicker*/
	$('.mascara_data').datepicker({
		dateFormat: "dd/mm/yy",
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
	    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
	    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
	    nextText: 'Próximo',
	    prevText: 'Anterior'
	});
	//Máscaras


	/*Validações de campos*/
	//Deixar mais claro os campos obrigatórios
	$('.obrigatorio').attr('placeholder','*');
	$('.obrigatorio').attr('title','Este campo é obrigatório');

	//Comparar senhas
	$("#confirmacaoSenha, #senha").change(function(){

		$("#erro_senha").remove(); 

		if($("#confirmacaoSenha").val() != $("#senha").val() && $("#confirmacaoSenha").val() != '' && $("#senha").val() != ''){
			$("#aviso_de_erro").fadeIn("slow", function() { $(this).append('<div id="erro_senha"><i class="material-icons">error</i>Senhas não conferem!</div>'); });
			$("#confirmacaoSenha").css('border-bottom-color', 'LightSalmon');
	        $("#confirmacaoSenha").css('border-bottom-width', '2px');
	        $("#senha").css('border-bottom-color', 'LightSalmon');
	        $("#senha").css('border-bottom-width', '2px');
		} else if ($("#confirmacaoSenha").val() != '' && $("#senha").val() != '') {
			$("#confirmacaoSenha").css('border-bottom-width', '2px');
			$("#confirmacaoSenha").css('border-bottom-color', 'LightGreen');
			$("#senha").css('border-bottom-width', '2px');
			$("#senha").css('border-bottom-color', 'LightGreen');
		}

	});

	//Verifica um campo input assim que perde o foco
	$('.obrigatorio').focusout(function(){

		var aviso = $(this).attr('aviso');


		$("#erro").remove(); 

		if(!$(this).hasClass("validar_cnpj") && !$(this).hasClass("validar_cpf") && !$(this).hasClass("validar_email")){
			$(this).css('background-color', '');	
		}	
        
		//Antes de validar retira as mascaras dos campos
		$('.mascara_cel').mask("?99999999999");
		$('.mascara_tel').mask("?9999999999");
		$('.mascara_cnpj').mask("99999999999999");
		$('.mascara_cpf').mask("99999999999"); 
		$('.mascara_rg').mask("?99999999*");
		$('.mascara_cep').mask("?99999999");
		$('.mascara_placa').mask("?aaa9999");
		$('.mascara_data').mask("?99999999");
		//Máscaras

		if($(this).val() == '' || $(this).val() == "Selecione...") {

			/*Quando possui a mascara_data e o usuário preenche a data com o calendário, 
			ele primeiro verifica se está vazio para depois colocar um valor, sendo assim para evitar esse erro deixo essa validação
			só para o momento de envio do formulário */

			if(!$(this).hasClass("mascara_data")) {

				$("#aviso_de_erro").fadeIn("slow", function() { $(this).append('<div id="erro"><i class="material-icons">error</i> Campo '+aviso+' deve ser preenchido!</div>'); });
		        $(this).css('background-color', 'LightSalmon');

				//Retorna as mascaras.
				$('.mascara_cel').mask("(?99)99999-9999");
				$('.mascara_tel').mask("(?99)9999-9999");
				$('.mascara_cnpj').mask("99.999.999/9999-99");
				$('.mascara_cpf').mask("999.999.999-99"); 
				$('.mascara_rg').mask("?99.999.999-*");
				$('.mascara_cep').mask("?99999-999");
				$('.mascara_placa').mask("?aaa-9999");
				$('.mascara_data').mask("?99/99/9999");
				$('.mascara_cns').mask("999.9999.9999.9999");
				//Máscaras

			}

		} else {
			
			if(!$(this).hasClass("validar_cnpj") && !$(this).hasClass("validar_cpf") && !$(this).hasClass("validar_email")){
				$(this).css('background-color', 'LightGreen');	
			}			

			$("#erro").remove(); 

			//Retorna as mascaras.

			$('.mascara_cel').mask("(?99)99999-9999");
			$('.mascara_tel').mask("(?99)9999-9999");
			$('.mascara_cnpj').mask("99.999.999/9999-99");
			$('.mascara_cpf').mask("999.999.999-99");
			$('.mascara_rg').mask("?99.999.999-*"); 
			$('.mascara_cep').mask("?99999-999");
			$('.mascara_placa').mask("?aaa-9999");
			$('.mascara_data').mask("?99/99/9999");

			//Máscaras

		}

	});
	
		//Valida o envio dos dados
		$('#validar_Enviar').click(function(e){
			
			e.preventDefault();
			var erros = 0;
			$("#erro").remove(); 
			
			//Antes de validar retira as mascaras dos campos
			$('.mascara_cel').mask("?99999999999");
			$('.mascara_tel').mask("?9999999999");
			$('.mascara_cnpj').mask("99999999999999");
			$('.mascara_cpf').mask("99999999999"); 
			$('.mascara_rg').mask("?99999999*");
			$('.mascara_cep').mask("?99999999");
			$('.mascara_data').mask("?99999999");
			$('.mascara_placa').mask("?aaa9999");
			//Máscaras

			mensagem = '';
			
			//Percorre todos inputs com essa classe
			$(".obrigatorio").each(function(){

				var aviso = $(this).attr('aviso');

				if($(this).val() == "" || $(this).val() == "Selecione...") {
					erros++;
					if(mensagem == '') {mensagem = 'O(s) Campo(s): ' + aviso;} //Garante que não irá começar com virgula.
					else {mensagem = mensagem + ', ' + aviso;}
					
			        $(this).css('background-color', 'LightSalmon');
				}
				
			}); //Fecha o percorre input

			if($("#erro_cnpj").length || $("#erro_cpf").length || $("#erro_email").length) { //Garante que não existem erros na tela

				$("#aviso_de_erro").fadeIn("slow", function() { $(this).append('<div id="erro"><i class="material-icons">error</i> Existe(m) campo(s) com erro(s) de preenchimento!</div>'); });
				
				//Retorna as mascaras.
				$('.mascara_cel').mask("(?99)99999-9999");
				$('.mascara_tel').mask("(?99)9999-9999");
				$('.mascara_cnpj').mask("99.999.999/9999-99");
				$('.mascara_cpf').mask("999.999.999-99");
				$('.mascara_rg').mask("?99.999.999-*"); 
				$('.mascara_cep').mask("?99999-999");
				$('.mascara_data').mask("?99/99/9999");
				$('.mascara_placa').mask("?aaa-9999");
				//Máscaras

				return false;
					

			} else if(erros == 0) {

				$('.mascara_data').mask("?99/99/9999"); // Precisa passar com as barras.
				var preco = ''+$('.preco').val(); //Garante que mesmo na inexistencia do campo com classe "preço" o formulário será enviado sem erros.

				var precoComPonto = preco.replace(",", ".");
				
				$('.preco').val(precoComPonto);
				$("#erro").fadeOut("slow", function() { $(this).remove(); });
				$("form").submit();

			} else {
					//Retorna as mascaras.
					$('.mascara_cel').mask("(?99)99999-9999");
					$('.mascara_tel').mask("(?99)9999-9999");
					$('.mascara_cnpj').mask("99.999.999/9999-99");
					$('.mascara_cpf').mask("999.999.999-99");
					$('.mascara_rg').mask("?99.999.999-*"); 
					$('.mascara_cep').mask("?99999-999");
					$('.mascara_placa').mask("?aaa-9999");
					$('.mascara_data').mask("?99/99/9999");
					//Máscaras
				$("#aviso_de_erro").fadeIn("slow", function() { $(this).append('<div id="erro"><i class="material-icons">error</i> '+mensagem+'. Deve(m) ser preenchido(s)!</div>'); });
				return false;
			}

	}); //Fecha o validar enviar


});

//***Usado para ter efeito select

 function MaterialSelect(element) {
  'use strict';

  this.element_ = element;
  this.maxRows = this.Constant_.NO_MAX_ROWS;
  // Initialize instance.
  this.init();
}

MaterialSelect.prototype.Constant_ = {
  NO_MAX_ROWS: -1,
  MAX_ROWS_ATTRIBUTE: 'maxrows'
};

MaterialSelect.prototype.CssClasses_ = {
  LABEL: 'mdl-textfield__label',
  INPUT: 'mdl-select__input',
  IS_DIRTY: 'is-dirty',
  IS_FOCUSED: 'is-focused',
  IS_DISABLED: 'is-disabled',
  IS_INVALID: 'is-invalid',
  IS_UPGRADED: 'is-upgraded'
};

MaterialSelect.prototype.onKeyDown_ = function(event) {
  'use strict';

  var currentRowCount = event.target.value.split('\n').length;
  if (event.keyCode === 13) {
    if (currentRowCount >= this.maxRows) {
      event.preventDefault();
    }
  }
};

MaterialSelect.prototype.onFocus_ = function(event) {
  'use strict';

  this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
};

MaterialSelect.prototype.onBlur_ = function(event) {
  'use strict';

  this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
};

MaterialSelect.prototype.updateClasses_ = function() {
  'use strict';
  this.checkDisabled();
  this.checkValidity();
  this.checkDirty();
};

MaterialSelect.prototype.checkDisabled = function() {
  'use strict';
  if (this.input_.disabled) {
    this.element_.classList.add(this.CssClasses_.IS_DISABLED);
  } else {
    this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
  }
};

MaterialSelect.prototype.checkValidity = function() {
  'use strict';
  if (this.input_.validity.valid) {
    this.element_.classList.remove(this.CssClasses_.IS_INVALID);
  } else {
    this.element_.classList.add(this.CssClasses_.IS_INVALID);
  }
};

MaterialSelect.prototype.checkDirty = function() {
  'use strict';
  if (this.input_.value && this.input_.value.length > 0) {
    this.element_.classList.add(this.CssClasses_.IS_DIRTY);
  } else {
    this.element_.classList.remove(this.CssClasses_.IS_DIRTY);
  }
};

MaterialSelect.prototype.disable = function() {
  'use strict';

  this.input_.disabled = true;
  this.updateClasses_();
};

MaterialSelect.prototype.enable = function() {
  'use strict';

  this.input_.disabled = false;
  this.updateClasses_();
};

MaterialSelect.prototype.change = function(value) {
  'use strict';

  if (value) {
    this.input_.value = value;
  }
  this.updateClasses_();
};

MaterialSelect.prototype.init = function() {
  'use strict';

  if (this.element_) {
    this.label_ = this.element_.querySelector('.' + this.CssClasses_.LABEL);
    this.input_ = this.element_.querySelector('.' + this.CssClasses_.INPUT);

    if (this.input_) {
      if (this.input_.hasAttribute(this.Constant_.MAX_ROWS_ATTRIBUTE)) {
        this.maxRows = parseInt(this.input_.getAttribute(
            this.Constant_.MAX_ROWS_ATTRIBUTE), 10);
        if (isNaN(this.maxRows)) {
          this.maxRows = this.Constant_.NO_MAX_ROWS;
        }
      }

      this.boundUpdateClassesHandler = this.updateClasses_.bind(this);
      this.boundFocusHandler = this.onFocus_.bind(this);
      this.boundBlurHandler = this.onBlur_.bind(this);
      this.input_.addEventListener('input', this.boundUpdateClassesHandler);
      this.input_.addEventListener('focus', this.boundFocusHandler);
      this.input_.addEventListener('blur', this.boundBlurHandler);

      if (this.maxRows !== this.Constant_.NO_MAX_ROWS) {
        // TODO: This should handle pasting multi line text.
        // Currently doesn't.
        this.boundKeyDownHandler = this.onKeyDown_.bind(this);
        this.input_.addEventListener('keydown', this.boundKeyDownHandler);
      }

      this.updateClasses_();
      this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
    }
  }
};

MaterialSelect.prototype.mdlDowngrade_ = function() {
  'use strict';
  this.input_.removeEventListener('input', this.boundUpdateClassesHandler);
  this.input_.removeEventListener('focus', this.boundFocusHandler);
  this.input_.removeEventListener('blur', this.boundBlurHandler);
  if (this.boundKeyDownHandler) {
    this.input_.removeEventListener('keydown', this.boundKeyDownHandler);
  }
};

// The component registers itself. It can assume componentHandler is available
// in the global scope.
componentHandler.register({
  constructor: MaterialSelect,
  classAsString: 'MaterialSelect',
  cssClass: 'mdl-js-select',
  widget: true
});